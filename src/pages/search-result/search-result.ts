import {Component} from '@angular/core';
import {
  IonicPage,
  NavParams,
  ToastController
} from 'ionic-angular';
import {SearchProvider} from '../../providers/search/search';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the SearchResult page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-search-result',
  templateUrl: 'search-result.html',
})
export class SearchResult {

  tokenAndUserID: any = '';

  doctors: Array<any> = [];
  search_data: {
    selected_state: {
      id: number,
      code: string,
      name: string
    },
    selected_specialty: string,
    query: string,
    phone_number: string,
    online_now: boolean
  };

  page: number = 1;

  constructor(private navParams: NavParams,
              private searchService: SearchProvider,
              private toastCtrl: ToastController,
              private authService: AuthProvider) {
    // this.doctors = navParams.get('doctors');
    // this.search_data = navParams.get('search_data');
    // if (!this.search_data.selected_state) {
    //   this.search_data.selected_state = {
    //     id: 0,
    //     code: '',
    //     name: ''
    //   };
    // }
  }

  ionViewCanEnter() {
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if (typeof tokenAndUserID == 'object') {
        this.tokenAndUserID = tokenAndUserID;

        this.doctors = this.navParams.get('doctors');
        this.search_data = this.navParams.get('search_data');
        console.log(this.search_data);
        if (!this.search_data.selected_state) {
          this.search_data.selected_state = {
            id: 0,
            code: '',
            name: ''
          };
        }

        resolve();
      }
    });
  }

  private searchResult(token) {
    return new Promise(resolve => {
      this.searchService.search(token, {
        query: this.search_data.query,
        phone_number: '',
        state: this.search_data.selected_state.id,
        specialty: this.search_data.selected_specialty,
        online_now: this.search_data.online_now
      }).subscribe(response => {
        const res: any = response;
        if (!res.bool) {
          let toast = this.toastCtrl.create({
            message: res.message,
            duration: 3000,
            position: 'top',
            cssClass: 'error'
          });
          toast.present();
          resolve();
          return;
        }
        this.doctors = res.search_result;
        resolve();
      });
    });
  }

  async doRefresh(refresher) {
    await this.searchResult(this.tokenAndUserID.token);
    refresher.complete();
  }

}
