import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {SearchResult} from './search-result';

// all custom components
import {ComponentsModule} from '../../components/components.module';

@NgModule({
  declarations: [
    SearchResult,
  ],
  imports: [
    IonicPageModule.forChild(SearchResult),
    ComponentsModule
  ],
  exports: [
    SearchResult
  ]
})
export class SearchResultModule {
}
