import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController, ToastController, MenuController} from 'ionic-angular';

// Providers
import {AuthProvider} from '../../providers/auth/auth';
import {HealthConditionDataProvider} from '../../providers/health-condition-data/health-condition-data';
import {PatientProvider} from '../../providers/patient/patient';
import {HealthProvider} from '../../providers/health/health';

/**
 * Generated class for the HealthConditionPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-health-condition',
  templateUrl: 'health-condition.html',
})
export class HealthConditionPage {

  tokenAndUserID: any;

  healthConditionData: Array<any>;

  isOther: boolean;
  isHealthCondition: boolean = false;
  otherField: string = '';

  healthConditionDataForAdd: Array<any> = [];

  canLeave: boolean = false;

  profileCompletionLinks: any;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private authService: AuthProvider,
              private loadingCtrl: LoadingController,
              private healthConditionService: HealthConditionDataProvider,
              private toastCtrl: ToastController,
              private patientService: PatientProvider,
              private healthService: HealthProvider,
              private menu: MenuController) {
    this.profileCompletionLinks = this.navParams.get('profileCompletionLinks');
  }

  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }

  ionViewCanLeave(): boolean {
    return this.canLeave;
  }

  ionViewCanEnter() {
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if (typeof tokenAndUserID == 'object') {
        this.tokenAndUserID = tokenAndUserID;
        if (this.tokenAndUserID.data.profileCompletionData.type != 'Health') {
          this.goNext();
          return resolve();
        }
        let loading = this.loadingCtrl.create({
          spinner: 'crescent'
        });
        loading.present();
        this.healthConditionService.getAll(this.tokenAndUserID.token).subscribe(response => {
          const res: any = response;
          if (!res.bool) {
            loading.dismiss();
            let toast = this.toastCtrl.create({
              message: res.message,
              duration: 3000,
              position: 'top',
              cssClass: 'error'
            });
            toast.present();
            resolve();
            return;
          }
          loading.dismiss();
          this.healthConditionData = res.message;
          resolve();
        });
      }
    });
  }

  private insertDataInFinalArray() {
    return new Promise((resolve) => {
      for (let i = 0; i < this.healthConditionData.length; i++) {
        if (this.healthConditionData[i].hasOwnProperty('isChecked')) {
          if (this.healthConditionData[i].isChecked) {
            this.healthConditionDataForAdd.push(this.healthConditionData[i].name.trim());
          }
        }
        if (i == (this.healthConditionData.length - 1)) {
          resolve();
          // for (let x = 0; x < this.otherFields.length; x++) {
          //   if (this.otherFields[x].value.trim() !== '') {
          //     this.healthConditionDataForAdd.push(this.otherFields[x].value.trim());
          //   }
          //   if (x - (this.otherFields.length - 1)) {
          //     resolve();
          //   }
          // }
        }
      }
    });
  }

  updateIsHealthCondition() {
    return new Promise(resolve => {
      this.patientService.updateIsHealth(this.tokenAndUserID.token, !this.isHealthCondition).subscribe(() => {
        resolve();
      });
    });
  }

  addMoreOtherFields() {
    this.healthConditionData.push({
      name: this.otherField.trim(),
      isChecked: true
    });
    this.otherField = '';
    this.isOther = false;
  }

  // removeOtherField(index) {
  //   this.otherFields.splice(index, 1);
  // }

  async addBunchHealthCondition() {
    await this.insertDataInFinalArray();
    if (this.healthConditionDataForAdd.length <= 0) {
      let toast = this.toastCtrl.create({
        message: 'You have to select at least one condition',
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
      return;
    }
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.healthService.insertBunch(this.tokenAndUserID.token, {
      conditions: this.healthConditionDataForAdd
    }).subscribe(async response => {
      const res: any = response;
      await this.updateIsHealthCondition();
      this.tokenAndUserID = await this.authService.authenticate();
      loading.dismiss();
      let cssClass = 'error';
      if (res.bool) {
        cssClass = 'success';
      }
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: cssClass
      });
      toast.present();
      this.goNext();
    });
  }

  private goNext() {
    this.canLeave = true;
    this.navCtrl.push(this.profileCompletionLinks[this.tokenAndUserID.data.profileCompletionData.type], {
      profileCompletionLinks: this.profileCompletionLinks
    });
  }

}
