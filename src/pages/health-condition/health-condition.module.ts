import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HealthConditionPage } from './health-condition';

@NgModule({
  declarations: [
    HealthConditionPage,
  ],
  imports: [
    IonicPageModule.forChild(HealthConditionPage),
  ],
  exports: [
    HealthConditionPage
  ]
})
export class HealthConditionPageModule {}
