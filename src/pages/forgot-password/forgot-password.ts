import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController, LoadingController} from 'ionic-angular';

// Providers
import {UserProvider} from '../../providers/user/user';

/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {

  email: string = '';

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private toastCtrl: ToastController,
              private loadingCtrl: LoadingController,
              private userService: UserProvider) {
  }

  private showToast(text: string, cssClasses: string = 'error') {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top',
      cssClass: cssClasses
    });
    toast.present();
  }

  openLoginPage() {
    this.navCtrl.setRoot('Login');
  }

  resetPassword() {
    if (this.email == '') {
      this.showToast('Email is required.');
      return;
    }
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.userService.resetPassword(this.email).subscribe(response => {
      const res: any = response;
      loading.dismiss();
      if(!res.bool) {
        this.showToast(res.message);
        return;
      }
      this.showToast(res.message, 'success');
      this.openLoginPage();
    });
  }

}
