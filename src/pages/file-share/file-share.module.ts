import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {FileSharePage} from './file-share';
import {ComponentsModule} from '../../components/components.module';

@NgModule({
  declarations: [
    FileSharePage,
  ],
  imports: [
    IonicPageModule.forChild(FileSharePage),
    ComponentsModule
  ],
  exports: [
    FileSharePage
  ]
})
export class FileSharePageModule {
}
