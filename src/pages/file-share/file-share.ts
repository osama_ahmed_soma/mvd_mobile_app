import {Component} from '@angular/core';
import {IonicPage, NavParams, LoadingController, ToastController} from 'ionic-angular';
import {PatientProvider} from '../../providers/patient/patient';
import {SharedFilesProvider} from '../../providers/shared-files/shared-files';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the FileSharePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-file-share',
  templateUrl: 'file-share.html',
})
export class FileSharePage {

  tokenAndUserID: any = '';

  fileID: number;
  sharedFiles: Array<any> = [];
  doctors: Array<any> = [];
  doctorIndex: number = 0;

  constructor(private loadingCtrl: LoadingController,
              private navParams: NavParams,
              private toastCtrl: ToastController,
              private patientService: PatientProvider,
              private sharedFilesService: SharedFilesProvider,
              private authService: AuthProvider) {
  }

  ionViewCanEnter() {
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if (typeof tokenAndUserID == 'object') {
        this.tokenAndUserID = tokenAndUserID;

        this.fileID = this.navParams.get('fileID');
        let loading = this.loadingCtrl.create({
          spinner: 'crescent'
        });
        loading.present();
        this.patientService.getMyPhysicians(this.tokenAndUserID.token).subscribe(response => {
          const res: any = response;
          if (!res.bool) {
            let toast = this.toastCtrl.create({
              message: res.message,
              duration: 3000,
              position: 'top',
              cssClass: 'error'
            });
            toast.present();
          }
          this.doctors = res.myDoctors;
          if (this.doctors.length <= 0) {
            let toast = this.toastCtrl.create({
              message: 'You have no physician.',
              duration: 3000,
              position: 'top',
              cssClass: 'error'
            });
            toast.present();
          }
          this.doRefresh({
            complete: () => {
              loading.dismiss();
              resolve();
            }
          });
        });
      }
    });
  }

  async doRefresh(refresher) {
    await this.getSharedFiles(this.tokenAndUserID.token);
    refresher.complete();
  }

  private getSharedFiles(token) {
    return new Promise(resolve => {
      this.sharedFilesService.getAll(token, this.fileID).subscribe(response => {
        const res: any = response;
        if (!res.bool) {
          let toast = this.toastCtrl.create({
            message: res.message,
            duration: 3000,
            position: 'top',
            cssClass: 'error'
          });
          toast.present();
        }
        this.sharedFiles = res.message.files;
        resolve();
      });
    });
  }

  async share() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.sharedFilesService.share(this.tokenAndUserID.token, this.fileID, this.doctors[this.doctorIndex].userid).subscribe(response => {
      const res: any = response;
      if (res.bool) {
        this.doRefresh({
          complete: () => {
            loading.dismiss();
            let toast = this.toastCtrl.create({
              message: res.message,
              duration: 3000,
              position: 'top',
              cssClass: 'success'
            });
            toast.present();
          }
        });
      } else {
        loading.dismiss();
        let toast = this.toastCtrl.create({
          message: res.message,
          duration: 3000,
          position: 'top',
          cssClass: 'error'
        });
        toast.present();
      }
    });
  }

}
