import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, MenuController} from 'ionic-angular';

/**
 * Generated class for the WelcomePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {

  isEnabled: boolean = false;
  canLeave: boolean = false;

  profileCompletion: any;

  profileCompletionLinks: any = {
    'Profile': 'BasicInformationPage',
    'Health': 'HealthConditionPage',
    'Medication': 'MedicationPage',
    'Allergies': 'AllergiesPage',
    'Surgeries': 'SurgeriesPage',
    'Complete': 'AllSetPage'
  };

  constructor(private navCtrl: NavController, private navParams: NavParams, private menu: MenuController) {
    this.profileCompletion = this.navParams.get('profileCompletion');
    if (!this.profileCompletion) {
      this.profileCompletion = {
        type: 'Profile'
      };
    }
  }

  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }

  ionViewCanLeave(): boolean {
    return this.canLeave;
  }

  openBasicInformation() {
    this.canLeave = true;
    this.isEnabled = true;
    this.navCtrl.push(this.profileCompletionLinks[this.profileCompletion.type], {
      profileCompletionLinks: this.profileCompletionLinks
    });
  }

}
