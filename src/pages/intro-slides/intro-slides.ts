import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, Platform } from 'ionic-angular';
import {Storage} from '@ionic/storage';

/**
 * Generated class for the IntroSlidesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-intro-slides',
  templateUrl: 'intro-slides.html',
})
export class IntroSlidesPage {

  @ViewChild('slides') slides: Slides;

  slidesImages: Array<string> = [
    'assets/img/slide-1.png',
    'assets/img/slide-2.png',
    'assets/img/slide-3.png'
  ];

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage, private platform: Platform) {
    if (this.platform.is('ipad') || this.platform.is('tablet')) {
      this.slidesImages = [
        'assets/img/step-ipad-1.png',
        'assets/img/step-ipad-2.png',
        'assets/img/step-ipad-3.png'
      ];
    }
  }

  ionViewDidLoad() {
    this.slides.lockSwipeToNext(true);
  }

  slideChanged(event) {
    // console.log(event);
    // console.log(this.slides);
  }

  unlockSlideNext() {
    this.slides.lockSwipeToNext(false);
    this.slides.slideNext();
  }

  slideNext() {
    this.slides.slideNext();
  }

  slidePrev() {
    this.slides.slidePrev();
  }

  closeMenu() {
    this.storage.ready().then(() => {
      this.storage.set('tutorialWatched', true);
      this.navCtrl.setRoot('Dashboard');
    });
  }

}
