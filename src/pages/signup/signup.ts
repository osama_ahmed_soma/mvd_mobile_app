import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController, LoadingController, MenuController} from 'ionic-angular';

// providers
import {UserProvider} from '../../providers/user/user';

/**
 * Generated class for the Signup page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class Signup {

  userData: { first_name: string, last_name: string, email: string, agreed: boolean };

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private toastCtrl: ToastController,
              private loadingCtrl: LoadingController,
              private userService: UserProvider,
              private menuCtrl: MenuController) {
    this.userData = {
      first_name: '',
      last_name: '',
      email: '',
      agreed: false
    };
  }

  ionViewDidEnter() {
    this.menuCtrl.swipeEnable(false, 'mvd_menu');
  }

  private validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  showToast(text, cssClasses: string = 'error', time: number = 3000) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: time,
      position: 'top',
      cssClass: cssClasses
    });
    toast.present();
  }

  signupSubmit() {
    if (!this.userData.agreed) {
      this.showToast('Please agree with Terms of Use and Privacy Policy');
      return;
    }
    if (this.userData.first_name == '') {
      this.showToast('First Name is required.');
      return;
    }
    if (this.userData.last_name == '') {
      this.showToast('Last Name is required.');
      return;
    }
    if (this.userData.email == '') {
      this.showToast('Email is required.');
      return;
    }
    if (!this.validateEmail(this.userData.email)) {
      this.showToast(this.userData.email + " is not valid email");
      return;
    }
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();

    this.userService.register(this.userData)
      .subscribe(res => {
        loading.dismiss();
        if (!res.bool) {
          this.showToast(res.message);
          return;
        }
        this.showToast(res.message, 'success', 10000);
        this.navCtrl.setRoot('Login');
      }, err => {
        loading.dismiss();
        this.showToast('Oppss Something went wrong. Please try again.');
      });
  }

  openLoginPage() {
    this.navCtrl.setRoot('Login');
  }

}
