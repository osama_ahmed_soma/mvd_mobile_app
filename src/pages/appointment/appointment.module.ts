import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Appointment } from './appointment';

@NgModule({
  declarations: [
    Appointment,
  ],
  imports: [
    IonicPageModule.forChild(Appointment),
  ],
  exports: [
    Appointment
  ]
})
export class AppointmentModule {}
