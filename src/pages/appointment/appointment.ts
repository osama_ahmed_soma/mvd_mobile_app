import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

/**
 * Generated class for the Appointment page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-appointment',
  templateUrl: 'appointment.html',
})
export class Appointment {

  appointment_past: any;
  appointment_cancel: any;
  appointment_upcoming: any;

  autoSelected: number = 2;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.autoSelected = (this.navParams.get('index') ? this.navParams.get('index') : ((this.navParams.get('index') === 0) ? this.navParams.get('index') : this.autoSelected));
    this.appointment_past = 'AppointmentPast';
    this.appointment_cancel = 'AppointmentCancel';
    this.appointment_upcoming = 'AppointmentUpcoming';
  }

}
