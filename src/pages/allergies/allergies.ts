import {Component} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ToastController,
  ModalController,
  MenuController
} from 'ionic-angular';

// Providers
import {AuthProvider} from '../../providers/auth/auth';
import {AllergiesDataProvider} from '../../providers/allergies-data/allergies-data';
import {PatientProvider} from '../../providers/patient/patient';
import {AllergyProvider} from '../../providers/allergy/allergy';

/**
 * Generated class for the AllergiesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-allergies',
  templateUrl: 'allergies.html',
})
export class AllergiesPage {

  tokenAndUserID: any;

  isAllergies: boolean = false;

  allergiesData: Array<any>;
  allergiesDataForAdd: Array<any> = [];
  isOther: boolean = false;
  // initialOtherID: number = 1;
  // otherFields: Array<any> = [{
  //   name: '',
  //   symptom: 'Severity',
  //   reaction: ''
  // }];

  otherField: {
    name: string,
    symptom: string,
    reaction: string,
    isChecked: boolean
  } = {
    name: '',
    symptom: 'Severity',
    reaction: '',
    isChecked: false
  };

  canLeave: boolean = false;

  profileCompletionLinks: any;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private authService: AuthProvider,
              private loadingCtrl: LoadingController,
              private allergiesDataService: AllergiesDataProvider,
              private toastCtrl: ToastController,
              private patientService: PatientProvider,
              private modalCtrl: ModalController,
              private allergyService: AllergyProvider,
              private menu: MenuController) {
    this.profileCompletionLinks = this.navParams.get('profileCompletionLinks');
  }

  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }

  ionViewCanLeave(): boolean {
    return this.canLeave;
  }

  ionViewCanEnter() {
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if (typeof tokenAndUserID == 'object') {
        this.tokenAndUserID = tokenAndUserID;
        if (this.tokenAndUserID.data.profileCompletionData.type != 'Allergies') {
          this.goNext();
          return resolve();
        }
        let loading = this.loadingCtrl.create({
          spinner: 'crescent'
        });
        loading.present();
        this.allergiesDataService.getAll(this.tokenAndUserID.token).subscribe(response => {
          const res: any = response;
          if (!res.bool) {
            loading.dismiss();
            let toast = this.toastCtrl.create({
              message: res.message,
              duration: 3000,
              position: 'top',
              cssClass: 'error'
            });
            toast.present();
            resolve();
            return;
          }
          loading.dismiss();
          this.allergiesData = res.message;
          // console.log(this.allergiesData);
          resolve();
        });
      }
    });
  }

  updateIsAllergies() {
    return new Promise(resolve => {
      this.patientService.updateIsAllergies(this.tokenAndUserID.token, !this.isAllergies).subscribe(() => {
        resolve();
      });
    });
  }

  openOtherFieldsModal(index) {
    if (this.allergiesData[index].hasOwnProperty('afterAdded')) {
      if (this.allergiesData[index].afterAdded) {
        this.allergiesData[index].afterAdded = false;
        return;
      }
    }
    if (this.allergiesData[index].isChecked) {
      this.allergiesData[index].symptom = 'Severity';
      let modal = this.modalCtrl.create('AllergieDataOtherFieldsComponent', {
        allergy: this.allergiesData[index],
        isOther: false
      });
      modal.onDidDismiss(data => {
        if (data.isCancelled) {
          this.allergiesData[index].isChecked = false;
        } else {
          if (data.allergiesData.symptom != '' && data.allergiesData.reaction != '') {
            this.allergiesData[index] = data.allergiesData;
          } else {
            this.allergiesData[index].isChecked = false;
          }
        }
      });
      modal.present();
    }
  }

  // openOtherFieldsModalForOtherData(index) {
  //   let modal = this.modalCtrl.create('AllergieDataOtherFieldsComponent', {
  //     allergy: this.otherFields[index],
  //     isOther: true
  //   });
  //   modal.onDidDismiss(data => {
  //     if (!data.isCancelled) {
  //       if (data.allergiesData.name != '' && data.allergiesData.symptom != '' && data.allergiesData.reaction != '') {
  //         this.otherFields[index] = {
  //           name: data.allergiesData.name,
  //           symptom: data.allergiesData.symptom,
  //           reaction: data.allergiesData.reaction
  //         };
  //       }
  //     }
  //   });
  //   modal.present();
  // }

  private emptyOtherFields() {
    this.otherField = {
      name: '',
      symptom: 'Severity',
      reaction: '',
      isChecked: false
    };
  }

  openOtherFieldsModalForOtherData() {
    if (this.isOther) {
      let modal = this.modalCtrl.create('AllergieDataOtherFieldsComponent', {
        allergy: this.otherField,
        isOther: true
      });
      modal.onDidDismiss(data => {
        this.isOther = false;
        if (!data.isCancelled) {
          if (data.allergiesData.name != '' && data.allergiesData.symptom != '' && data.allergiesData.reaction != '') {
            data.allergiesData.isChecked = true;
            data.allergiesData.afterAdded = true;
            this.allergiesData.push(data.allergiesData);
            this.emptyOtherFields();
          }
        }
      });
      modal.present();
    }
  }

  private insertDataInFinalArray() {
    return new Promise(resolve => {
      if (this.allergiesData.length > 0) {
        for (let i = 0; i < this.allergiesData.length; i++) {
          if (this.allergiesData[i].hasOwnProperty('isChecked')) {
            if (this.allergiesData[i].isChecked) {
              this.allergiesDataForAdd.push({
                condition_term: this.allergiesData[i].name,
                symptom: this.allergiesData[i].symptom,
                reaction: this.allergiesData[i].reaction
              });
            }
          }
          if (i == (this.allergiesData.length - 1)) {
            resolve();
          }
        }
      } else {
        resolve();
      }
    });
  }

  async addBunchAllergies() {
    await this.insertDataInFinalArray();
    if (this.allergiesDataForAdd.length <= 0) {
      let toast = this.toastCtrl.create({
        message: 'You have to select at least one allergy',
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
      return;
    }
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.allergyService.insertBunch(this.tokenAndUserID.token, {
      allergies: this.allergiesDataForAdd
    }).subscribe(async response => {
      const res: any = response;
      await this.updateIsAllergies();
      this.tokenAndUserID = await this.authService.authenticate();
      loading.dismiss();
      let cssClass = 'error';
      if (res.bool) {
        cssClass = 'success';
      }
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: cssClass
      });
      toast.present();
      this.goNext();
    });
  }

  private goNext() {
    this.canLeave = true;
    this.navCtrl.push(this.profileCompletionLinks[this.tokenAndUserID.data.profileCompletionData.type], {
      profileCompletionLinks: this.profileCompletionLinks
    });
  }

}
