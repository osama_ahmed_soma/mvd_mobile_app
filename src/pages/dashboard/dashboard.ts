import {Component, ElementRef} from '@angular/core';
import {
  IonicPage, Platform, NavController, NavParams, MenuController, Events, PopoverController,
  ToastController
} from 'ionic-angular';
import {Storage} from '@ionic/storage';

import {AuthProvider} from '../../providers/auth/auth';
import {Searchform} from "../../components/searchform/searchform";

/**
 * Generated class for the Dashboard page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html'
})
export class Dashboard {

  tokenAndUserID: any = '';

  messageCount: number;
  messageInboxCount: number = 0;

  upcomingAppointmentCount: number;
  appointmentTotalCount: number = 0;

  physiciansCount: number = 0;
  filesCount: number = 0;

  isProfileComplete: string = 'Incomplete';
  isMyHealthComplete: string = 'Incomplete';

  data: any;
  popover: any;

  isSearchTriggered: boolean = false;

  constructor(public platform: Platform,
              public navCtrl: NavController,
              public navParams: NavParams,
              private menuCtrl: MenuController,
              private storage: Storage,
              private events: Events,
              private authService: AuthProvider,
              private domElements: ElementRef,
              private popoverCtrl: PopoverController,
              private toastCtrl: ToastController) {
    this.storage.get('isFirstTime').then(isFirstTime => {
      if (!isFirstTime) {
        this.addAnimation();
        setTimeout(() => {
          this.removeAnimation();
        }, 12000);
      }
    });
    const trigger = this.navParams.get('trigger');
    if(trigger){
      switch (trigger) {
        case 'search':
          setTimeout(() => {
            if(!this.isSearchTriggered){
              let event = new MouseEvent('click', {bubbles: true});
              this.domElements.nativeElement.querySelector('[icon-search]').dispatchEvent(event);
              this.isSearchTriggered = true;
            }
          }, 1500);
          break;
      }
    }

  }

  private addAnimation(){
    this.domElements.nativeElement.querySelector('[icon-search]').setAttribute('animatedSearchButton', '');
    this.domElements.nativeElement.querySelector('#btn-up').style.display = 'block';
    this.domElements.nativeElement.querySelector('#fix-btns').style.display = 'block';
    this.domElements.nativeElement.querySelector('#btn-up').classList.add('btn-up');
    this.domElements.nativeElement.querySelector('#fix-btns').classList.add('fix-btns');
    this.storage.set('isFirstTime', true);
  }

  private removeAnimation(){
    this.domElements.nativeElement.querySelector('[icon-search]').removeAttribute('animatedSearchButton');
    this.domElements.nativeElement.querySelector('#btn-up').style.display = 'none';
    this.domElements.nativeElement.querySelector('#fix-btns').style.display = 'none';
    this.domElements.nativeElement.querySelector('#btn-up').classList.remove('btn-up');
    this.domElements.nativeElement.querySelector('#fix-btns').classList.remove('fix-btns');
    this.storage.set('isFirstTime', true);
  }

  ionViewCanEnter() {
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if (typeof tokenAndUserID == 'object') {
        this.tokenAndUserID = tokenAndUserID;

        const res: any = this.tokenAndUserID.data;
        this.data = res;
        if (res.bool) {
          this.messageCount = res.message.inbox.unread;
          // this.upcomingAppointmentCount = res.appointment.upcoming;
          this.upcomingAppointmentCount = res.appointment.upcomingAppointmentNewData.todayTotal;
          if (res.message.inbox.total > 0) {
            this.messageInboxCount = res.message.inbox.total;
          }
          if (res.appointment.upcomingAppointmentNewData.total > 0) {
            this.appointmentTotalCount = res.appointment.upcomingAppointmentNewData.total;
          }
          this.physiciansCount = res.myPhysician.total;
          this.filesCount = res.files.total;
          if (res.profileCompletionData.type != 'Profile') {
            this.isProfileComplete = 'Complete';
          }
          if (res.profileCompletionData.type == 'Complete') {
            this.isMyHealthComplete = 'Complete';
          }
          if (res.profileCompletionData.percentage < 100) {
            this.navCtrl.setRoot('WelcomePage', {
              profileCompletion: res.profileCompletionData
            });
          }
        }
        resolve();
      }
    });
  }

  presentPopover(myEvent) {
    this.popover = this.popoverCtrl.create(Searchform, {
      states: this.data.states,
      specialties: this.data.specialties
    });
    this.popover.present({
      ev: myEvent
    });
    this.popover.onDidDismiss(data => {
      if (data) {
        data.instances.loading.dismiss();
        delete data.instances;
        this.removeAnimation();
        if (data.doctors.length > 0) {
          this.navCtrl.push('SearchResult', data);
        } else {
          let toast = this.toastCtrl.create({
            message: 'No Doctor Found.',
            duration: 3000,
            position: 'top',
            cssClass: 'error'
          });
          toast.present();
        }
      }
    });
  }

  ionViewDidEnter() {
    this.menuCtrl.swipeEnable(true, 'mvd_menu');
  }

  openFAQ() {
    this.platform.ready().then(async () => {
      window.open('mailto:support@myvirtualdoctor.com');
    });
    // this.navCtrl.push('Faq');
  }

  openAppointment() {
    this.navCtrl.push('Appointment');
  }

  openMessages() {
    this.navCtrl.push('Message');
  }

  openPhysicans() {
    this.navCtrl.push('MyPhysiciansPage');
  }

  openSettings() {
    // this.navCtrl.push('ChangePasswordPage');
    this.navCtrl.setRoot('SettingsPage');
  }

  openHealth() {
    this.navCtrl.push('Myhealth');
  }

  openMyfiles() {
    this.navCtrl.push('MyFilesPage');
  }

  openMyprofile() {
    this.navCtrl.push('Myprofile');
  }

  async logout() {
    this.events.publish('logout');
  }

}
