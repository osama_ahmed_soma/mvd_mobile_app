import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Dashboard } from './dashboard';

// all custom components
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    Dashboard,
  ],
  imports: [
    IonicPageModule.forChild(Dashboard),
    ComponentsModule
  ],
  exports: [
    Dashboard
  ]
})
export class DashboardModule {}
