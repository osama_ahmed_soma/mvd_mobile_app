import {Component} from '@angular/core';
import {
  IonicPage,
  Platform,
  LoadingController,
  ToastController,
  ActionSheetController
} from 'ionic-angular';
import {Camera, CameraOptions} from '@ionic-native/camera';
import {Diagnostic} from '@ionic-native/diagnostic';
import {PhotoViewer} from '@ionic-native/photo-viewer';

// Providers
import {PatientProvider} from '../../providers/patient/patient';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the Myprofile page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-myprofile',
  templateUrl: 'myprofile.html',
})
export class Myprofile {

  tokenAndUserID: any = '';

  profileImage: string = '';
  fullName: string = '';

  profileData: {
    firstName: string,
    lastName: string,
    email: string,
    address: string,
    city: string,
    optionalState: string,
    stateID: number,
    zip: string,
    country: string,
    dob: string,
    gender: number, // 1 = male and 2 = female
    timeZone: string,
    cellPhone: string,
    weight: number,
    height: {
      feet: number,
      inches: number
    }
  } = {
    firstName: '',
    lastName: '',
    email: '',
    address: '',
    city: '',
    optionalState: '',
    stateID: 0,
    zip: '',
    country: 'United States',
    dob: '',
    gender: 1,
    timeZone: '',
    cellPhone: '',
    weight: 0,
    height: {
      feet: 0,
      inches: 0
    }
  };

  profileCompletion: {
    percentage: string,
    text: string,
    type: string
  } = {
    percentage: '0%',
    text: '',
    type: ''
  };

  countries: Array<any> = [];

  isStates: boolean = true;
  states: Array<any> = [];

  timeZones: Array<any> = [];

  constructor(private platform: Platform,
              private loadingCtrl: LoadingController,
              private toastCtrl: ToastController,
              private actionSheetCtrl: ActionSheetController,
              private patientService: PatientProvider,
              private camera: Camera,
              private diagnostic: Diagnostic,
              private photoViewer: PhotoViewer,
              private authService: AuthProvider) {
  }

  ionViewCanEnter() {
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if (typeof tokenAndUserID == 'object') {
        this.tokenAndUserID = tokenAndUserID;

        let loading = this.loadingCtrl.create({
          spinner: 'crescent'
        });
        loading.present();
        this.patientService.getProfile(this.tokenAndUserID.token).subscribe(response => {
          const res: any = response;
          if (!res.bool) {
            loading.dismiss();
            let toast = this.toastCtrl.create({
              message: res.message,
              duration: 3000,
              position: 'top',
              cssClass: 'error'
            });
            toast.present();
            resolve();
            return;
          }

          this.profileImage = res.message.patientData.image_url;
          this.fullName = res.message.patientData.firstname + ' ' + res.message.patientData.lastname;

          this.profileData.firstName = res.message.patientData.firstname;
          this.profileData.lastName = res.message.patientData.lastname;
          this.profileData.email = res.message.patientData.email;
          this.profileData.address = res.message.patientData.address;
          this.profileData.city = res.message.patientData.city;
          this.profileData.optionalState = res.message.patientData.optional_state;
          this.profileData.stateID = res.message.patientData.state_id;
          this.profileData.zip = res.message.patientData.zip;
          this.profileData.country = res.message.patientData.country || this.profileData.country;
          this.profileData.dob = res.message.patientData.dob;
          this.profileData.gender = res.message.patientData.gender;
          this.profileData.timeZone = res.message.patientData.timezone;
          this.profileData.cellPhone = res.message.patientData.phone_mobile;
          this.profileData.weight = res.message.patientData.weight;
          this.profileData.height.feet = res.message.patientData.height_ft;
          this.profileData.height.inches = res.message.patientData.height_inch;

          this.profileCompletion = res.message.profileCompletionData;
          this.profileCompletion.percentage = this.profileCompletion.percentage + '%';

          this.countries = res.message.countries;

          this.checkCountryChangeState();

          this.states = res.message.states;

          this.timeZones = res.message.timeZones;

          loading.dismiss();
          resolve();
        });
      }
    });
  }

  checkCountryChangeState() {
    this.isStates = (this.profileData.country == 'United States');
  }

  private showToast(text, cssClass: string = 'error') {
    if (text) {
      let toast = this.toastCtrl.create({
        message: text,
        duration: 3000,
        position: 'top',
        cssClass: cssClass
      });
      toast.present();
    }
  }

  private validatePhonenNumber(inputtxt) {
    const phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    return !!inputtxt.match(phoneno);
  }

  async saveProfileData() {
    if (this.profileData.firstName.trim() == '') {
      this.showToast('First Name is required.');
      return;
    }
    if (this.profileData.lastName.trim() == '') {
      this.showToast('Last Name is required.');
      return;
    }
    if (this.profileData.city.trim() == '') {
      this.showToast('City is required.');
      return;
    }
    if (this.isStates) {
      if (!this.profileData.stateID) {
        this.showToast('State is required.');
        return;
      }
    }
    if (this.profileData.country.trim() == '') {
      this.showToast('Country is required.');
      return;
    }
    if (!this.profileData.gender) {
      this.showToast('Gender is required.');
      return;
    }
    if (this.profileData.timeZone.trim() == '') {
      this.showToast('Time Zone is required.');
      return;
    }
    if (this.profileData.cellPhone.trim() == '') {
      this.showToast('Cell Phone is required.');
      return;
    }
    if (!this.validatePhonenNumber(this.profileData.cellPhone.trim())) {
      this.showToast('Cell Phone is not valid.');
      return;
    }
    if (this.profileData.weight <= 0) {
      this.showToast('Weight is required.');
      return;
    }
    if (this.profileData.height.feet <= 0) {
      this.showToast('Height Ft. is required.');
      return;
    }
    if (this.profileData.height.inches < 0) {
      this.showToast('Height In. is required.');
      return;
    }

    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.patientService.update(this.tokenAndUserID.token, this.profileData).subscribe(response => {
      const res: any = response;
      loading.dismiss();
      if (!res.bool) {
        this.showToast(res.message);
        return;
      }
      this.showToast(res.message, 'success');
    });
  }

  private async openCamera(sourceType) {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: sourceType,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      cameraDirection: this.camera.Direction.FRONT,
      allowEdit: true
    };
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    try {
      const imageData = await this.camera.getPicture(options);
      this.profileImage = "data:image/jpeg;base64," + imageData;
      this.patientService.updateImage(this.tokenAndUserID.token, {
        image_url: this.profileImage
      }).subscribe(async response => {
        const res: any = response;
        if (!res.bool) {
          loading.dismiss();
          this.showToast(res.message);
          return;
        }
        this.tokenAndUserID = await this.authService.authenticate();
        loading.dismiss();
        this.showToast(res.message, 'success');
      });
    } catch (e) {
      loading.dismiss();
    }
  }

  private async cameraMethod(sourceType) {
    try {
      await this.platform.ready();
      const present = await this.diagnostic.isCameraPresent();
      if (present) {
        const authorized = await this.diagnostic.isCameraAuthorized();
        if (authorized) {
          this.openCamera(sourceType);
        } else {
          const status = await this.diagnostic.getCameraAuthorizationStatus();
          if (status == this.diagnostic.permissionStatus.GRANTED) {
            this.openCamera(sourceType);
          } else {
            this.diagnostic.requestCameraAuthorization().then(() => {
              this.openCamera(sourceType);
            });
          }
        }
      } else {
        this.showToast('Camera is not available.');
      }
    } catch (e) {
      this.showToast(e.toString());
    }
  }

  private async viewImage() {
    try {
      const authorized = await this.diagnostic.isExternalStorageAuthorized();
      if (authorized) {
        this.photoViewer.show(this.profileImage);
      } else {
        await this.diagnostic.requestExternalStorageAuthorization();
        this.photoViewer.show(this.profileImage);
      }
    } catch (e) {
      this.showToast(e.toString());
    }
  }

  getImage() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Update Profile Picture',
      buttons: [
        {
          text: 'Choose From Gallery',
          handler: () => {
            this.cameraMethod(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Capture From Camera',
          handler: () => {
            this.cameraMethod(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'View',
          handler: () => {
            this.viewImage();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {

          }
        }
      ]
    });
    actionSheet.present();
  }

}
