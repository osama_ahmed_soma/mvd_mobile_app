import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {Myprofile} from './myprofile';

import {ComponentsModule} from '../../components/components.module';

@NgModule({
  declarations: [
    Myprofile,
  ],
  imports: [
    IonicPageModule.forChild(Myprofile),
    ComponentsModule
  ],
  exports: [
    Myprofile
  ]
})
export class MyprofileModule {
}
