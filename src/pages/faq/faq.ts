import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the Faq page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html',
})
export class Faq {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  openFAQdetails(){
    this.navCtrl.push('FaqDetail');
  }
}
