import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Faq } from './faq';
// all custom components
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    Faq,
  ],
  imports: [
    IonicPageModule.forChild(Faq),
    ComponentsModule
  ],
  exports: [
    Faq
  ]
})
export class FaqModule {}
