import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {MyPhysiciansPage} from './my-physicians';

// all custom components
import {ComponentsModule} from '../../components/components.module';

@NgModule({
  declarations: [
    MyPhysiciansPage,
  ],
  imports: [
    IonicPageModule.forChild(MyPhysiciansPage),
    ComponentsModule
  ],
  exports: [
    MyPhysiciansPage
  ]
})
export class MyPhysiciansPageModule {
}
