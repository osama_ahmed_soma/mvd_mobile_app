import {Component} from '@angular/core';
import {IonicPage, ToastController, LoadingController, Events} from 'ionic-angular';

// Providers
import {PatientProvider} from '../../providers/patient/patient';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the MyPhysiciansPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-my-physicians',
  templateUrl: 'my-physicians.html',
})
export class MyPhysiciansPage {

  tokenAndUserID: any = '';

  doctors: Array<any> = [];

  constructor(public toastCtrl: ToastController,
              public loadingCtrl: LoadingController,
              private patientService: PatientProvider,
              private authService: AuthProvider,
              private events: Events) {
    this.events.subscribe('doctorRemovedFromFav', async () => {
      let loading = this.loadingCtrl.create({
        spinner: 'crescent'
      });
      loading.present();
      await this.getDoctors();
      loading.dismiss();
    });
  }

  ionViewCanEnter() {
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if (typeof tokenAndUserID == 'object') {
        this.tokenAndUserID = tokenAndUserID;

        let loading = this.loadingCtrl.create({
          spinner: 'crescent'
        });
        loading.present();
        this.getDoctors().then(() => {
          loading.dismiss();
          resolve();
        });
      }
    });
  }

  private getDoctors() {
    return new Promise(async (resolve) => {
      this.patientService.getMyPhysicians(this.tokenAndUserID.token).subscribe(response => {
        const res: any = response;
        if (!res.bool) {
          let toast = this.toastCtrl.create({
            message: res.message,
            duration: 3000,
            position: 'top',
            cssClass: 'error'
          });
          toast.present();
          return resolve();
        }
        for (let i = 0; i < res.myDoctors.length; i++) {
          res.myDoctors[i].isFav = true;
        }
        this.doctors = res.myDoctors;
        resolve();
      });
    });
  }

  async doRefresh(refresher) {
    await this.getDoctors();
    refresher.complete();
  }

}
