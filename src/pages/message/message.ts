import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

/**
 * Generated class for the Message page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-message',
  templateUrl: 'message.html',
})
export class Message {

  message_inbox: any;
  message_sent: any;
  message_drafts: any;
  message_trash: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    // this.message_inbox = 'MessageCompose';
    this.message_inbox = 'MessageInboxComponent';
    // this.message_send = 'MessageSend';
    this.message_sent = 'MessageSentComponent';
    // this.message_draft = 'Drafth';
    this.message_drafts = 'MessageDraftsComponent';
    this.message_trash = 'MessageTrashComponent';
  }

}
