import {Component} from '@angular/core';
import {Platform, IonicPage, NavController, NavParams, LoadingController, ToastController} from 'ionic-angular';
import {Diagnostic} from '@ionic-native/diagnostic';
import {Socket} from 'ng-socket-io';
import 'rxjs/add/operator/map';

// Providers
import {AppointmentProvider} from '../../providers/appointment/appointment';
import {AuthProvider} from '../../providers/auth/auth';

// declare let OT: any;

declare let OT: any;

/**
 * Generated class for the VideoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-video',
  templateUrl: 'video.html',
})
export class VideoPage {

  tokenAndUserID: any = '';

  appointmentID: number;

  apiKey: string = '';
  sessionId: string = '';
  token: string = '';

  publisher: any;
  subscriber: any;
  session: any;

  publisherOptions: any = {
    insertMode: 'append',
    width: '100%',
    height: '100%',
    showControls: false
  };
  subscriberOptions: any = {
    width: '100%',
    height: "100%",
    showControls: false,
    insertMode: 'append'
  };

  video: boolean = true;
  audio: boolean = true;
  mic: string = 'mic';
  volume: string = 'volume-up';

  formatedTime: string = '00:00';

  constructor(private platform: Platform,
              private navParams: NavParams,
              private navCtrl: NavController,
              private loadingCtrl: LoadingController,
              private appointmentService: AppointmentProvider,
              private toastCtrl: ToastController,
              private diagnostic: Diagnostic,
              private authService: AuthProvider,
              private socket: Socket) {
  }

  ionViewCanEnter() {
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if (typeof tokenAndUserID == 'object') {
        this.tokenAndUserID = tokenAndUserID;

        this.appointmentID = this.navParams.get('appointmentID');
        if (!this.appointmentID) {
          let toast = this.toastCtrl.create({
            message: 'No Appointment ID.',
            duration: 3000,
            position: 'top',
            cssClass: 'error'
          });
          toast.present();
          this.navCtrl.pop();
          resolve();
          return;
        }

        let loading = this.loadingCtrl.create({
          spinner: 'crescent'
        });
        loading.present();
        this.appointmentService.startVideo(this.tokenAndUserID.token, this.appointmentID).subscribe(async (response) => {
          const res: any = response;
          this.apiKey = res.result.apiKey;
          this.sessionId = res.result.appointment.tokbox_session;
          this.token = res.result.openTokToken;
          // this.initSession();
          try {
            await this.initSession();
          } catch (e) {
            loading.dismiss();
            let toast = this.toastCtrl.create({
              message: e.message,
              duration: 3000,
              position: 'top',
              cssClass: 'error'
            });
            toast.present();
            this.navCtrl.pop();
            resolve();
            return;
          }
          loading.dismiss();
          this.socket.emit('checkVideoEnd', JSON.stringify({
            appointmentID: res.result.appointment.id,
            userID: this.tokenAndUserID.userID
          }));
          this.socket.fromEvent('appointment:doctorEndConsultation').map(data => data.toString()).subscribe(data => {
            // The consultation is now over. The doctor will prepare a report for you shortly so make sure to check
            // back for this appointment under “Past Appointments” section on your
            let toast = this.toastCtrl.create({
              message: 'The consultation is now over. The doctor will prepare a report for you shortly so make sure to check back for this appointment under “Past Appointments” section on your “My Appointments” “Past Tab”.',
              duration: 10000,
              position: 'top',
              cssClass: 'success'
            });
            toast.present();
            this.navCtrl.setRoot('Appointment', {
              index: 0
            });
          });
          resolve();
        });
      }
    });
  }

  ionViewWillLeave() {
    this.session.disconnect();
  }

  private handleError(error) {
    console.log(error.message);
    if (error) {
      let toast = this.toastCtrl.create({
        message: error.message,
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
    }
  }

  private authCamera() {
    return new Promise(async (resolve, reject) => {
      try {
        await this.diagnostic.isCameraPresent();
        const authorized = await this.diagnostic.isCameraAuthorized();
        if (authorized) {
          // authorized
          return resolve();
        }
        // request
        await this.diagnostic.requestCameraAuthorization();
        // if (status == this.diagnostic.permissionStatus.DENIED || status == this.diagnostic.permissionStatus.DENIED_ALWAYS) {
        //   return reject("Authorization request for camera use was denied.");
        // }
        resolve();
      } catch (e) {
        reject(e);
      }
    });
  }

  private authMicrophone() {
    return new Promise(async (resolve, reject) => {
      try {
        const authorized = await this.diagnostic.isMicrophoneAuthorized();
        if (authorized) {
          // authorized
          return resolve();
        }
        // request
        await this.diagnostic.requestMicrophoneAuthorization();
        // if (status == this.diagnostic.permissionStatus.DENIED || status == this.diagnostic.permissionStatus.DENIED_ALWAYS) {
        //   return reject("Authorization request for microphone use was denied.");
        // }
        resolve();
      } catch (e) {
        reject(e);
      }
    });
  }

  private authorized() {
    return new Promise(async (resolve, reject) => {
      try {
        await this.authCamera();
        await this.authMicrophone();
        resolve();
      } catch (e) {
        reject(e);
      }
    });
  }

  private async initSession() {
    try {
      await this.platform.ready();
      await this.authorized();
      this.session = OT.initSession(this.apiKey, this.sessionId);

      // Subscribe to a newly created stream
      this.session.on({
        streamCreated: (event) => {
          // this.session.subscribe(event.stream, 'subscriber');
          // this.subscriber = this.session.subscribe(event.stream, 'subscriber', this.subscriberOptions, this.handleError);
          this.subscriber = this.session.subscribe(event.stream, 'subscriber', this.subscriberOptions);
          this.timer(0);
        },
        streamDestroyed: (event) => {
          console.log(`Stream ${event.stream.name} ended because ${event.reason}`);
        }
      });
      // this.session.on('streamCreated', event => {
      //   this.subscriber = this.session.subscribe(event.stream, 'subscriber', this.subscriberOptions, this.handleError);
      //   this.timer(0);
      // });

      // Create a publisher
      // this.publisher = OT.initPublisher('publisher', this.publisherOptions, this.handleError);

      // Connect to the session
      this.session.connect(this.token, () => {
        this.publisher = OT.initPublisher('publisher', this.publisherOptions);
        // this.session.publish(this.publisher, this.handleError);
        this.session.publish(this.publisher);
      });
      // this.session.connect(this.token, error => {
      //   // If the connection is successful, publish to the session
      //   if (error) {
      //     this.handleError(error);
      //   } else {
      //     this.session.publish(this.publisher, this.handleError);
      //   }
      // });
    } catch (e) {
      this.handleError({
        message: "The following error occurred: " + e
      });
    }
  }

  private timer(time) {
    const minutes: any = Math.floor(time / 60);
    const seconds: any = time - minutes * 60;
    this.formatedTime = ("0" + minutes).slice(-2) + ':' + ("0" + seconds).slice(-2);
    setTimeout(() => {
      this.timer(++time);
    }, 1000);
  }

  toggleCamera() {
    this.video = !this.video;
    this.publisher.publishVideo(this.video);
  }

  toggleMic() {
    this.mic = (this.mic == 'mic') ? 'mic-off' : 'mic';
    this.audio = !this.audio;
    this.publisher.publishAudio(this.audio);
  }

  toggleVol() {
    if (this.volume == 'volume-up') {
      this.volume = 'volume-off';
      this.subscriber.setAudioVolume(0);
    } else {
      this.volume = 'volume-up';
      this.subscriber.setAudioVolume(100);
    }
  }

}
