import {Component} from '@angular/core';
import {IonicPage, LoadingController, ToastController} from 'ionic-angular';

// Providers
import {PatientProvider} from '../../providers/patient/patient';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the PrimaryPhysicianPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-primary-physician',
  templateUrl: 'primary-physician.html',
})
export class PrimaryPhysicianPage {

  tokenAndUserID: any = '';

  states: Array<any> = [];
  physicianData: {
    firstName: string,
    lastName: string,
    email: string,
    phoneNumber: string,
    city: string,
    state: any,
    zip: string
  } = {
    firstName: '',
    lastName: '',
    email: '',
    phoneNumber: '',
    city: '',
    state: null,
    zip: '',
  };

  constructor(private loadingCtrl: LoadingController,
              private toastCtrl: ToastController,
              private patientService: PatientProvider,
              private authService: AuthProvider) {
  }

  ionViewCanEnter() {
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if (typeof tokenAndUserID == 'object') {
        this.tokenAndUserID = tokenAndUserID;

        let loading = this.loadingCtrl.create({
          spinner: 'crescent'
        });
        loading.present();
        this.patientService.getPrimaryPhysician(this.tokenAndUserID.token).subscribe(response => {
          const res: any = response;
          loading.dismiss();
          if (!res.bool) {
            this.showToast(res.message);
            resolve();
            return;
          }
          this.physicianData.firstName = res.message.primaryPhysician.firstname || '';
          this.physicianData.lastName = res.message.primaryPhysician.lastname || '';
          this.physicianData.email = res.message.primaryPhysician.email || '';
          this.physicianData.phoneNumber = res.message.primaryPhysician.phone_mobile || '';
          this.physicianData.city = res.message.primaryPhysician.city || '';
          this.physicianData.state = res.message.primaryPhysician.state_id || null;
          this.physicianData.zip = res.message.primaryPhysician.zip || '';
          this.states = res.message.states;
          resolve();
        });
      }
    });
  }

  private showToast(text, cssClass: string = 'error') {
    if (text) {
      let toast = this.toastCtrl.create({
        message: text,
        duration: 3000,
        position: 'top',
        cssClass: cssClass
      });
      toast.present();
    }
  }

  private validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  async update() {
    if (this.physicianData.firstName.trim() == '') {
      return this.showToast('First Name is required.');
    }
    if (this.physicianData.lastName.trim() == '') {
      return this.showToast('Last Name is required.');
    }
    if (this.physicianData.email.trim() == '') {
      return this.showToast('Email is required.');
    }
    if (!this.validateEmail(this.physicianData.email.trim())) {
      return this.showToast('Email is not valid.');
    }
    if (this.physicianData.phoneNumber.trim() == '') {
      return this.showToast('Phone Number is required.');
    }
    if (this.physicianData.city.trim() == '') {
      return this.showToast('City is required.');
    }
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.patientService.updatePrimaryPhysician(this.tokenAndUserID.token, {
      firstname: this.physicianData.firstName.trim(),
      lastname: this.physicianData.lastName.trim(),
      email: this.physicianData.email.trim(),
      phone_mobile: this.physicianData.phoneNumber.trim(),
      city: this.physicianData.city.trim(),
      state_id: (this.physicianData.state) ? this.states[this.physicianData.state].id : null,
      zip: this.physicianData.zip.trim()
    }).subscribe(response => {
      const res: any = response;
      loading.dismiss();
      if (!res.bool) {
        return this.showToast(res.message);
      }
      this.showToast(res.message, 'success');
    });

  }

}
