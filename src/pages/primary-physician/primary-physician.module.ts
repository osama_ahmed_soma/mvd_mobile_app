import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {PrimaryPhysicianPage} from './primary-physician';

import {ComponentsModule} from '../../components/components.module';

@NgModule({
  declarations: [
    PrimaryPhysicianPage,
  ],
  imports: [
    IonicPageModule.forChild(PrimaryPhysicianPage),
    ComponentsModule
  ],
  exports: [
    PrimaryPhysicianPage
  ]
})
export class PrimaryPhysicianPageModule {
}
