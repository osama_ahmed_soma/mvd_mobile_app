import {Component} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController,
  LoadingController,
  MenuController,
  Events,
  Platform
} from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {Socket} from 'ng-socket-io';

// providers
import {UserProvider} from '../../providers/user/user';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class Login {

  userData: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private toastCtrl: ToastController,
              private userService: UserProvider,
              private loadingCtrl: LoadingController,
              private storage: Storage,
              private menuCtrl: MenuController,
              private socket: Socket,
              private events: Events,
              private authService: AuthProvider,
              private platform: Platform) {

    this.userData = {
      email: '',
      password: '',
      deviceID: '',
      deviceType: 'android'
    };

    if(this.platform.is('ios')){
      this.userData.deviceType = 'ios';
    }

    // this.events.subscribe('deviceID:set', () => {
    //   this.userData.deviceID = this.authService.deviceID;
    //   alert(this.userData.deviceID);
    // });



  }

  ionViewDidEnter() {
    this.menuCtrl.swipeEnable(false, 'mvd_menu');
  }

  showToast(text: string, cssClasses: string = 'error') {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top',
      cssClass: cssClasses
    });
    toast.present();
  }

  loginSubmit() {
    if (!this.userData.hasOwnProperty('email') || !this.userData.hasOwnProperty('password')) {
      this.showToast('Username and Password is required.');
      return;
    }
    if (this.userData.email == '') {
      this.showToast('Email is required.');
      return;
    }
    if (this.userData.password == '') {
      this.showToast('Password is required.');
      return;
    }
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.userData.deviceID = this.authService.deviceID;
    this.userService.login(this.userData)
      .subscribe(res => {
        loading.dismiss();
        if (!res.bool) {
          this.showToast(res.message);
          return;
        }
        this.socket.emit('checkAuth', res.token);
        this.showToast(res.message, 'success');
        this.storage.ready().then(() => {
          this.storage.set('jwt-token', res.token);
          this.storage.set('userID', res.userID);
          this.storage.set('tutorialWatched', false);

          this.events.publish('setTokenAndUserID', {
            token: res.token,
            userID: res.userID
          });
          this.events.publish('appointment:main:start');
          this.navCtrl.setRoot('Dashboard');
        });
      }, err => {
        loading.dismiss();
        this.showToast('Oppss Something went wrong. Please try again.');
      });
  }

  openSignupPage() {
    this.navCtrl.setRoot('Signup');
  }
  openForgetPage(){
    this.navCtrl.setRoot('ForgotPasswordPage');
  }

}
