import {Component} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ToastController,
  ModalController,
  MenuController
} from 'ionic-angular';

// Providers
import {AuthProvider} from '../../providers/auth/auth';
import {MedicationDataProvider} from '../../providers/medication-data/medication-data';
import {PatientProvider} from '../../providers/patient/patient';
import {MedicationProvider} from '../../providers/medication/medication';

/**
 * Generated class for the MedicationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-medication',
  templateUrl: 'medication.html',
})
export class MedicationPage {

  tokenAndUserID: any;

  isOther: boolean;
  isMedication: boolean = false;

  // initialOtherID: number = 1;
  // otherFields: Array<any> = [{
  //   id: '',
  //   name: '',
  //   dosage: '',
  //   frequency: 'Once',
  //   time: 'Daily'
  // }];

  otherField: {
    medication_id: string,
    name: string,
    isChecked: boolean,
    dosage: string,
    frequency: string,
    time: string
  } = {
    medication_id: '',
    name: '',
    isChecked: false,
    dosage: '',
    frequency: 'Once',
    time: 'Daily',
  };

  medicationData: Array<any> = [];
  medicationDataForAdd: Array<any> = [];

  canLeave: boolean = false;

  profileCompletionLinks: any;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private authService: AuthProvider,
              private loadingCtrl: LoadingController,
              private medicationDataService: MedicationDataProvider,
              private toastCtrl: ToastController,
              private patientService: PatientProvider,
              private modalCtrl: ModalController,
              private medicationService: MedicationProvider,
              private menu: MenuController) {
    this.profileCompletionLinks = this.navParams.get('profileCompletionLinks');
  }

  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }

  ionViewCanLeave(): boolean {
    return this.canLeave;
  }

  ionViewCanEnter() {
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if (typeof tokenAndUserID == 'object') {
        this.tokenAndUserID = tokenAndUserID;
        if (this.tokenAndUserID.data.profileCompletionData.type != 'Medication') {
          this.goNext();
          return resolve();
        }
        let loading = this.loadingCtrl.create({
          spinner: 'crescent'
        });
        loading.present();
        this.medicationDataService.getAll(this.tokenAndUserID.token).subscribe(response => {
          const res: any = response;
          if (!res.bool) {
            loading.dismiss();
            let toast = this.toastCtrl.create({
              message: res.message,
              duration: 3000,
              position: 'top',
              cssClass: 'error'
            });
            toast.present();
            resolve();
            return;
          }
          loading.dismiss();
          this.medicationData = res.message;
          resolve();
        });
      }
    });
  }

  updateIsMedication() {
    return new Promise(resolve => {
      this.patientService.updateIsMedication(this.tokenAndUserID.token, !this.isMedication).subscribe(() => {
        resolve();
      });
    });
  }

  openOtherFieldsModal(index) {
    if (this.medicationData[index].hasOwnProperty('afterAdded')) {
      if (this.medicationData[index].afterAdded) {
        this.medicationData[index].afterAdded = false;
        return;
      }
    }
    if (this.medicationData[index].isChecked) {
      let modal = this.modalCtrl.create('MedicationDataOtherFieldsComponent', {
        medication: this.medicationData[index],
        isOther: false
      });
      modal.onDidDismiss(data => {
        if (data.isCancelled) {
          this.medicationData[index].isChecked = false;
        } else {
          if (data.medicationData.dosage != '' && data.medicationData.frequency != '' && data.medicationData.time != '') {
            this.medicationData[index] = data.medicationData;
          } else {
            this.medicationData[index].isChecked = false;
          }
        }
      });
      modal.present();
    }
  }

  private emptyOtherFields() {
    this.otherField = {
      medication_id: '',
      name: '',
      isChecked: false,
      dosage: '',
      frequency: 'Once',
      time: 'Daily',
    };
  }

  openOtherFieldsModalForOtherData() {
    // let modal = this.modalCtrl.create('MedicationDataOtherFieldsComponent', {
    //   medication: this.otherFields[index],
    //   isOther: true
    // });
    // modal.onDidDismiss(data => {
    //   if (!data.isCancelled) {
    //     if (data.medicationData.id != '' && data.medicationData.name != '' && data.medicationData.dosage != '' && data.medicationData.frequency != '' && data.medicationData.time != '') {
    //       this.otherFields[index] = {
    //         id: data.medicationData.id,
    //         name: data.medicationData.name,
    //         dosage: data.medicationData.dosage,
    //         frequency: data.medicationData.frequency,
    //         time: data.medicationData.time
    //       };
    //     }
    //   }
    // });
    // modal.present();
    if (this.isOther) {
      let modal = this.modalCtrl.create('MedicationDataOtherFieldsComponent', {
        medication: this.otherField,
        isOther: true
      });
      modal.onDidDismiss(data => {
        this.isOther = false;
        if (!data.isCancelled) {
          if (data.medicationData.medication_id != '' && data.medicationData.name != '' && data.medicationData.dosage != '' && data.medicationData.frequency != '' && data.medicationData.time != '') {
            data.medicationData.isChecked = true;
            data.medicationData.afterAdded = true;
            this.medicationData.push(data.medicationData);
            this.emptyOtherFields();
          }
        }
      });
      modal.present();
    }
  }

  // addMoreOtherFields() {
  //   this.otherFields.push({
  //     id: '',
  //     name: '',
  //     dosage: '',
  //     frequency: 'Once',
  //     time: 'Daily'
  //   });
  // }

  // removeOtherField(index) {
  //   this.otherFields.splice(index, 1);
  // }

  private insertMedicationData() {
    return new Promise(resolve => {
      for (let i = 0; i < this.medicationData.length; i++) {
        if (this.medicationData[i].hasOwnProperty('isChecked')) {
          if (this.medicationData[i].isChecked) {
            this.medicationDataForAdd.push({
              medication_id: this.medicationData[i].medication_id,
              condition_term: this.medicationData[i].name,
              dosage: this.medicationData[i].dosage.trim(),
              frequency: this.medicationData[i].frequency,
              time: this.medicationData[i].time
            });
          }
        }
        if (i == (this.medicationData.length - 1)) {
          resolve();
        }
      }
    });
  }

  // private insertOtherFieldsData() {
  //   return new Promise(resolve => {
  //     for (let x = 0; x < this.otherFields.length; x++) {
  //       if (this.otherFields[x].id != '' && this.otherFields[x].name != '' && this.otherFields[x].dosage != '' && this.otherFields[x].frequency != '' && this.otherFields[x].time != '') {
  //         this.medicationDataForAdd.push({
  //           medication_id: this.otherFields[x].id,
  //           condition_term: this.otherFields[x].name,
  //           dosage: this.otherFields[x].dosage.trim(),
  //           frequency: this.otherFields[x].frequency,
  //           time: this.otherFields[x].time
  //         });
  //       }
  //       if (x == (this.otherFields.length - 1)) {
  //         resolve();
  //       }
  //     }
  //   });
  // }

  private insertDataInFinalArray() {
    return new Promise(async resolve => {
      if (this.medicationData.length > 0) {
        await this.insertMedicationData();
        // if (this.otherFields.length > 0) {
        //   await this.insertOtherFieldsData();
        //   resolve();
        // } else {
        //   resolve();
        // }
        resolve();
      } else {
        // if (this.otherFields.length > 0) {
        //   await this.insertOtherFieldsData();
        //   resolve();
        // } else {
        //   // nothing
        //   resolve();
        // }
        resolve();
      }
    });
  }

  async addBunchMedication() {
    await this.insertDataInFinalArray();
    if (this.medicationDataForAdd.length <= 0) {
      let toast = this.toastCtrl.create({
        message: 'You have to select at least one medication',
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
      return;
    }
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.medicationService.insertBunch(this.tokenAndUserID.token, {
      medications: this.medicationDataForAdd
    }).subscribe(async response => {
      const res: any = response;
      await this.updateIsMedication();
      this.tokenAndUserID = await this.authService.authenticate();
      loading.dismiss();
      let cssClass = 'error';
      if (res.bool) {
        cssClass = 'success';
      }
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: cssClass
      });
      toast.present();
      this.goNext();
    });
  }

  private goNext() {
    this.canLeave = true;
    this.navCtrl.push(this.profileCompletionLinks[this.tokenAndUserID.data.profileCompletionData.type], {
      profileCompletionLinks: this.profileCompletionLinks
    });
  }

}
