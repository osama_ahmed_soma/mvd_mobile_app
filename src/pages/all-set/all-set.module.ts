import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AllSetPage } from './all-set';

@NgModule({
  declarations: [
    AllSetPage,
  ],
  imports: [
    IonicPageModule.forChild(AllSetPage),
  ],
  exports: [
    AllSetPage
  ]
})
export class AllSetPageModule {}
