import {Component} from '@angular/core';
import {IonicPage, NavController, MenuController} from 'ionic-angular';

/**
 * Generated class for the AllSetPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-all-set',
  templateUrl: 'all-set.html',
})
export class AllSetPage {

  canLeave: boolean = false;

  constructor(private navCtrl: NavController, private menu: MenuController) {
  }

  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }

  ionViewCanLeave(): boolean {
    return this.canLeave;
  }

  allSet() {
    this.canLeave = true;
    this.navCtrl.setRoot('Dashboard', {
      trigger: 'search'
    });
  }

}
