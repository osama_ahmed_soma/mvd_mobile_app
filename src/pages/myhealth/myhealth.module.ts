import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Myhealth } from './myhealth';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    Myhealth,
  ],
  imports: [
    IonicPageModule.forChild(Myhealth),
    ComponentsModule
  ],
  exports: [
    Myhealth
  ]
})
export class MyhealthModule {}
