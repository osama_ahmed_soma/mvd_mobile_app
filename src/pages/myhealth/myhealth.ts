import {Component} from '@angular/core';
import {IonicPage, NavParams, LoadingController} from 'ionic-angular';

// Service
import {PatientProvider} from '../../providers/patient/patient';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the Myhealth page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-myhealth',
  templateUrl: 'myhealth.html',
})
export class Myhealth {

  tokenAndUserID: any = '';

  health: any;
  medication: any;
  allergies: any;
  surgeries: any;

  activeNow: boolean = false;

  isData: {
    is_health: number,
    is_allergies: number,
    is_medication: number,
    is_surgeries: number,
    loading: any
  };

  autoSelected: number = 0;

  constructor(private loadingCtrl: LoadingController,
              private navParams: NavParams,
              private patientService: PatientProvider,
              private authService: AuthProvider) {
  }

  ionViewCanEnter() {
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if (typeof tokenAndUserID == 'object') {
        this.tokenAndUserID = tokenAndUserID;

        this.autoSelected = (this.navParams.get('index') ? this.navParams.get('index') : this.autoSelected );
        let loading = this.loadingCtrl.create({
          spinner: 'crescent'
        });
        loading.present();
        this.patientService.getMyHealthData(this.tokenAndUserID.token).subscribe(response => {
          const res: any = response;
          this.isData = res.message.patient;
          this.health = 'HealthComponent';
          this.allergies = 'AllergiesComponent';
          this.medication = 'MedicationsComponent';
          this.surgeries = 'SurgeriesComponent';
          this.activeNow = true;
          this.isData.loading = loading;
          resolve();
        });
      }
    });
  }

}
