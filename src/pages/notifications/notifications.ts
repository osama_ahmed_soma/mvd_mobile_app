import {Component} from '@angular/core';
import {IonicPage, ToastController, LoadingController} from 'ionic-angular';

// Providers
import {AuthProvider} from '../../providers/auth/auth';
import {RegisterDevicesProvider} from '../../providers/register-devices/register-devices';

/**
 * Generated class for the NotificationsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {

  loading: any;

  tokenAndUserID: any = '';
  notify: {
    message: boolean,
    appointment: boolean
  } = {
    message: true,
    appointment: true
  };
  isDisabled: boolean = false;

  constructor(private authService: AuthProvider,
              private registerDevicesService: RegisterDevicesProvider,
              private toastCtrl: ToastController,
              private loadingCtrl: LoadingController) {
    this.loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    this.loading.present();
    this.registerDevicesService.get().subscribe(response => {
      const res: any = response;
      this.loading.dismiss();
      if (!res.bool) {
        let toast = this.toastCtrl.create({
          message: res.message,
          duration: 3000,
          position: 'top',
          cssClass: 'error'
        });
        toast.present();
        this.isDisabled = true;
        return;
      }
      this.isDisabled = false;
      this.notify.message = (res.data.messageNotify === 'yes');
      this.notify.appointment = (res.data.appointmentNotify === 'yes');
    });
  }

  ionViewCanEnter() {
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if (typeof tokenAndUserID == 'object') {
        this.tokenAndUserID = tokenAndUserID;
        console.log(this.tokenAndUserID);
        resolve();
      }
    });
  }

  notifyMessageChange() {
    if (!this.isDisabled) {
      const loading = this.loadingCtrl.create({
        spinner: 'crescent'
      });
      loading.present();
      this.registerDevicesService.changeNotification(((this.notify.message) ? 'yes' : 'no'), 'message').subscribe(res => {
        loading.dismiss();
      });
    }
  }

  notifyAppointmentChange() {
    if (!this.isDisabled) {
      const loading = this.loadingCtrl.create({
        spinner: 'crescent'
      });
      loading.present();
      this.registerDevicesService.changeNotification(((this.notify.appointment) ? 'yes' : 'no'), 'appointment').subscribe(res => {
        loading.dismiss();
      });
    }
  }


}
