import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {MyFilesPage} from './my-files';

import {ComponentsModule} from '../../components/components.module';

@NgModule({
  declarations: [
    MyFilesPage,
  ],
  imports: [
    IonicPageModule.forChild(MyFilesPage),
    ComponentsModule
  ],
  exports: [
    MyFilesPage
  ]
})
export class MyFilesPageModule {
}
