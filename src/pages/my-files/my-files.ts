import {Component} from '@angular/core';
import {
  Platform,
  IonicPage,
  NavController,
  LoadingController,
  ToastController,
  ActionSheetController,
  AlertController
} from 'ionic-angular';
import {FileOpener} from '@ionic-native/file-opener';
import {FileChooser} from '@ionic-native/file-chooser';

// Providers
import {FilesUploadProvider} from '../../providers/files-upload/files-upload';
import {AuthProvider} from '../../providers/auth/auth';

declare const window: any;

/**
 * Generated class for the MyFilesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-my-files',
  templateUrl: 'my-files.html',
})
export class MyFilesPage {

  tokenAndUserID: any = '';

  files: Array<any> = [];
  clickedIndex: number;

  isAndroid: boolean = true;

  allowedMimeTypesWithExtensions: any = {
    'image/gif': '.gif',
    'image/jpeg': '.jpg',
    'image/png': '.png',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document': '.docx',
    'application/msword': '	.doc',
    'application/vnd.ms-excel': '.xls',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': '.xlsx',
    'application/pdf': '.pdf',
    'image/bmp': '.bmp',
    'text/plain': '.txt'
  };

  // selectedFile: any;

  constructor(private platform: Platform,
              private navCtrl: NavController,
              private loadingCtrl: LoadingController,
              private toastCtrl: ToastController,
              private actionSheetCtrl: ActionSheetController,
              private filesUploadService: FilesUploadProvider,
              private alertCtrl: AlertController,
              private fileOpener: FileOpener,
              private fileChooser: FileChooser,
              private authService: AuthProvider) {
    // platform.ready().then(() => {
    //   if (platform.is('ios')) {
    //     this.isAndroid = false;
    //   }
    // });
    // let loading = loadingCtrl.create({
    //   spinner: 'crescent'
    // });
    // loading.present();
    // this.doRefresh({
    //   complete: () => {
    //     loading.dismiss();
    //   }
    // });
  }

  ionViewCanEnter() {
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if (typeof tokenAndUserID == 'object') {
        this.tokenAndUserID = tokenAndUserID;

        await this.platform.ready();
        if (this.platform.is('ios')) {
          this.isAndroid = false;
        }
        let loading = this.loadingCtrl.create({
          spinner: 'crescent'
        });
        loading.present();
        this.doRefresh({
          complete: () => {
            loading.dismiss();
            resolve();
          }
        });
      }
    });
  }

  async doRefresh(refresher) {
    await this.getDocuments(this.tokenAndUserID.token);
    refresher.complete();
  }

  private getDocuments(token) {
    return new Promise(resolve => {
      this.filesUploadService.getAll(token).subscribe(response => {
        const res: any = response;
        console.log(res);
        if (!res.bool) {
          let toast = this.toastCtrl.create({
            message: res.message,
            duration: 3000,
            position: 'top',
            cssClass: 'error'
          });
          toast.present();
        }
        this.files = res.message.files;
        resolve();
      });
    });
  }

  private guid() {
    return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' +
      this.s4() + '-' + this.s4() + this.s4() + this.s4();
  }

  private s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }

  private async uploadFileAndroid() {
    try {
      const uri = await this.fileChooser.open();
      window.resolveLocalFileSystemURL(uri, fileEntry => {
        fileEntry.file(async file => {
          const type = file.type;
          if (this.allowedMimeTypesWithExtensions.hasOwnProperty(type)) {
            try {
              let loading = this.loadingCtrl.create({
                spinner: 'crescent'
              });
              loading.present();
              const fileName = this.guid() + this.allowedMimeTypesWithExtensions[type];
              await this.filesUploadService.upload(this.tokenAndUserID.token, uri, type, fileName);
              await this.getDocuments(this.tokenAndUserID.token);
              loading.dismiss();
              let toast = this.toastCtrl.create({
                message: 'File successfully uploaded',
                duration: 3000,
                position: 'top',
                cssClass: 'success'
              });
              toast.present();
            } catch (e) {
              let toast = this.toastCtrl.create({
                message: e,
                duration: 3000,
                position: 'top',
                cssClass: 'error'
              });
              toast.present();
            }
          } else {
            let toast = this.toastCtrl.create({
              message: 'The filetype you are attempting to upload is not allowed.',
              duration: 3000,
              position: 'top',
              cssClass: 'error'
            });
            toast.present();
          }
        });
      }, e => {
        let toast = this.toastCtrl.create({
          message: e,
          duration: 3000,
          position: 'top',
          cssClass: 'error'
        });
        toast.present();
      });
    } catch (e) {
      let toast = this.toastCtrl.create({
        message: e,
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
    }
  }

  async upload() {
    if (this.isAndroid) {
      this.uploadFileAndroid();
    } else {
      // ios
      // this.uploadFileIOS(event);
    }
  }

  openActions(index) {
    this.clickedIndex = index;
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Download',
          icon: 'download',
          handler: () => {
            this.download();
          }
        },
        {
          text: 'Rename',
          icon: 'create',
          handler: () => {
            this.rename();
          }
        },
        {
          text: 'Share',
          icon: 'share',
          handler: () => {
            this.share();
          }
        },
        {
          text: 'Remove',
          icon: 'trash',
          role: 'destructive',
          handler: () => {
            this.removeDialog();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {

          }
        }
      ]
    });
    actionSheet.present();
  }

  private async download() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    try {
      const downloadText = await this.filesUploadService.download(this.tokenAndUserID.token, this.files[this.clickedIndex].id, this.files[this.clickedIndex].filename);
      await this.platform.ready();
      window.resolveLocalFileSystemURL(downloadText, fileEntry => {
        loading.dismiss();
        fileEntry.file(file => {
          const type = file.type;
          this.fileOpener.open(downloadText, type).then(() => {
          }).catch(e => console.log('Error openening file', e));
        });
      }, e => {
        loading.dismiss();
        let toast = this.toastCtrl.create({
          message: e,
          duration: 3000,
          position: 'top',
          cssClass: 'error'
        });
        toast.present();
      });
    } catch (e) {
      loading.dismiss();
      let toast = this.toastCtrl.create({
        message: e,
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
    }
  }

  private rename() {
    let alert = this.alertCtrl.create({
      title: 'Rename',
      inputs: [
        {
          name: 'fileName',
          placeholder: 'Document Title',
          value: this.files[this.clickedIndex].title
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Update',
          handler: data => {
            let loading = this.loadingCtrl.create({
              spinner: 'crescent'
            });
            loading.present();
            this.filesUploadService.update(this.tokenAndUserID.token, this.files[this.clickedIndex].id, data.fileName).subscribe(response => {
              const res: any = response;
              loading.dismiss();
              let cssClass = 'error';
              if (res.bool) {
                cssClass = 'success';
                this.files[this.clickedIndex].title = data.fileName;
              }
              let toast = this.toastCtrl.create({
                message: res.message,
                duration: 3000,
                position: 'top',
                cssClass: cssClass
              });
              toast.present();
            });
          }
        }
      ]
    });
    alert.present();
  }

  private share() {
    this.navCtrl.push('FileSharePage', {
      fileID: this.files[this.clickedIndex].id
    });
  }

  private removeDialog() {
    let alert = this.alertCtrl.create({
      title: 'Confirm',
      message: 'Are you sure to delete this file?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.remove();
          }
        }
      ],
      enableBackdropDismiss: false
    });
    alert.present();
  }

  private async remove() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.filesUploadService.remove(this.tokenAndUserID.token, this.files[this.clickedIndex].id).subscribe(response => {
      const res: any = response;
      loading.dismiss();
      let cssClass = 'error';
      if (!res.bool) {
        cssClass = 'error';
      } else {
        cssClass = 'success';
        this.files.splice(this.clickedIndex, 1);
      }
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: cssClass
      });
      toast.present();
    });
  }

}
