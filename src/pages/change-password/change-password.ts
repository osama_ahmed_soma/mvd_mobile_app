import {Component} from '@angular/core';
import {IonicPage, ToastController, LoadingController} from 'ionic-angular';

// Providers
import {UserProvider} from '../../providers/user/user';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the ChangePasswordPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {

  tokenAndUserID: any = '';

  passwordData: {
    currentPassword: string,
    newPassword: string,
    newPasswordAgain: string
  } = {
    currentPassword: '',
    newPassword: '',
    newPasswordAgain: ''
  };

  isPasswordVisible: {
    current: boolean,
    newP: boolean,
    newPAgain: boolean
  } = {
    current: false,
    newP: false,
    newPAgain: false
  };

  constructor(private toastCtrl: ToastController,
              private loadingCtrl: LoadingController,
              private userService: UserProvider,
              private authService: AuthProvider) {
  }

  ionViewCanEnter(){
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if(typeof tokenAndUserID == 'object'){
        this.tokenAndUserID = tokenAndUserID;
        resolve();
      }
    });
  }

  showCurrentPassword() {
    this.isPasswordVisible.current = !this.isPasswordVisible.current;
  }

  showNewPassword() {
    this.isPasswordVisible.newP = !this.isPasswordVisible.newP;
  }

  showNewPasswordAgain() {
    this.isPasswordVisible.newPAgain = !this.isPasswordVisible.newPAgain;
  }

  private showToast(text, cssClass: string = 'error') {
    if (text) {
      let toast = this.toastCtrl.create({
        message: text,
        duration: 3000,
        position: 'top',
        cssClass: cssClass
      });
      toast.present();
    }
  }

  async changePassword() {
    if (this.passwordData.currentPassword.trim() == '') {
      return this.showToast('Current Password is required.');
    }
    if (this.passwordData.newPassword.trim() == '') {
      return this.showToast('New Password is required.');
    }
    if (this.passwordData.newPasswordAgain.trim() == '') {
      return this.showToast('New Password Again is required.');
    }
    if (this.passwordData.newPassword.trim() != this.passwordData.newPasswordAgain.trim()) {
      return this.showToast('Passwords are not same.');
    }
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.userService.changePassword(this.tokenAndUserID.token, this.passwordData).subscribe(response => {
      const res: any = response;
      loading.dismiss();
      if (!res.bool) {
        return this.showToast(res.message);
      }
      this.showToast(res.message, 'success');
      this.passwordData.currentPassword = '';
      this.passwordData.newPassword = '';
      this.passwordData.newPasswordAgain = '';
    });
  }

}
