import {Component} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ToastController,
  ModalController,
  MenuController
} from 'ionic-angular';

// Providers
import {AuthProvider} from '../../providers/auth/auth';
import {SurgeriesDataProvider} from '../../providers/surgeries-data/surgeries-data';
import {PatientProvider} from '../../providers/patient/patient';
import {SurgeryProvider} from '../../providers/surgery/surgery';

/**
 * Generated class for the SurgeriesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-surgeries',
  templateUrl: 'surgeries.html',
})
export class SurgeriesPage {

  tokenAndUserID: any;
  surgeriesData: Array<any> = [];
  surgeriesDataForAdd: Array<any> = [];
  isSurgeries: boolean = false;
  isOther: boolean = false;
  otherField: {
    name: string,
    year: number
  } = {
    name: '',
    year: new Date().getFullYear()
  };

  canLeave: boolean = false;

  profileCompletionLinks: any;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private authService: AuthProvider,
              private loadingCtrl: LoadingController,
              private surgeriesDataService: SurgeriesDataProvider,
              private toastCtrl: ToastController,
              private patientService: PatientProvider,
              private modalCtrl: ModalController,
              private surgeryService: SurgeryProvider,
              private menu: MenuController) {
    this.profileCompletionLinks = this.navParams.get('profileCompletionLinks');
  }

  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }

  ionViewCanLeave(): boolean {
    return this.canLeave;
  }

  ionViewCanEnter() {
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if (typeof tokenAndUserID == 'object') {
        this.tokenAndUserID = tokenAndUserID;
        if (this.tokenAndUserID.data.profileCompletionData.type != 'Surgeries') {
          this.goNext();
          return resolve();
        }
        let loading = this.loadingCtrl.create({
          spinner: 'crescent'
        });
        loading.present();
        this.surgeriesDataService.getAll(this.tokenAndUserID.token).subscribe(response => {
          const res: any = response;
          if (!res.bool) {
            loading.dismiss();
            let toast = this.toastCtrl.create({
              message: res.message,
              duration: 3000,
              position: 'top',
              cssClass: 'error'
            });
            toast.present();
            resolve();
            return;
          }
          loading.dismiss();
          this.surgeriesData = res.message;
          resolve();
        });
      }
    });
  }

  updateIsSurgeries() {
    return new Promise(resolve => {
      this.patientService.updateIsSurgeries(this.tokenAndUserID.token, !this.isSurgeries).subscribe(() => {
        resolve();
      });
    });
  }

  openOtherFieldsModal(index) {
    if (this.surgeriesData[index].hasOwnProperty('afterAdded')) {
      if (this.surgeriesData[index].afterAdded) {
        this.surgeriesData[index].afterAdded = false;
        return;
      }
    }
    if (this.surgeriesData[index].isChecked) {
      let modal = this.modalCtrl.create('SurgeriesDataOtherFieldsComponent', {
        surgery: this.surgeriesData[index],
        isOther: false
      });
      modal.onDidDismiss(data => {
        if (data.isCancelled) {
          this.surgeriesData[index].isChecked = false;
        } else {
          if (data.surgeryData.year != '') {
            this.surgeriesData[index] = data.surgeryData;
          } else {
            this.surgeriesData[index].isChecked = false;
          }
        }
      });
      modal.present();
    }
  }

  private emptyOtherFields() {
    this.otherField = {
      name: '',
      year: new Date().getFullYear()
    };
  }

  openOtherFieldsModalForOtherData() {
    if (this.isOther) {
      let modal = this.modalCtrl.create('SurgeriesDataOtherFieldsComponent', {
        surgery: this.otherField,
        isOther: true
      });
      modal.onDidDismiss(data => {
        this.isOther = false;
        if (!data.isCancelled) {
          if (data.surgeryData.name != '' && data.surgeryData.year > 0) {
            data.surgeryData.isChecked = true;
            data.surgeryData.afterAdded = true;
            this.surgeriesData.push(data.surgeryData);
            this.emptyOtherFields();
          }
        }
      });
      modal.present();
    }
  }

  private insertDataInFinalArray() {
    return new Promise(resolve => {
      if (this.surgeriesData.length > 0) {
        for (let i = 0; i < this.surgeriesData.length; i++) {
          if (this.surgeriesData[i].hasOwnProperty('isChecked')) {
            if (this.surgeriesData[i].isChecked) {
              this.surgeriesDataForAdd.push({
                condition_term: this.surgeriesData[i].name,
                source: this.surgeriesData[i].year
              });
            }
          }
          if (i == (this.surgeriesData.length - 1)) {
            resolve();
          }
        }
      } else {
        resolve();
      }
    });
  }

  async addBunchSurgeries() {
    await this.insertDataInFinalArray();
    if (this.surgeriesDataForAdd.length <= 0) {
      let toast = this.toastCtrl.create({
        message: 'You have to select at least one surgery',
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
      return;
    }
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();

    this.surgeryService.insertBunch(this.tokenAndUserID.token, {
      surgeries: this.surgeriesDataForAdd
    }).subscribe(async response => {
      const res: any = response;
      await this.updateIsSurgeries();
      this.tokenAndUserID = await this.authService.authenticate();
      loading.dismiss();
      let cssClass = 'error';
      if (res.bool) {
        cssClass = 'success';
      }
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: cssClass
      });
      toast.present();
      this.goNext();
    });
  }

  private goNext() {
    this.canLeave = true;
    this.navCtrl.push(this.profileCompletionLinks[this.tokenAndUserID.data.profileCompletionData.type], {
      profileCompletionLinks: this.profileCompletionLinks
    });
  }

}
