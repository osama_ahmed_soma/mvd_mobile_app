import {Component} from '@angular/core';
import {IonicPage} from 'ionic-angular';

/**
 * Generated class for the SettingsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  changePassword: any;
  notifications: any;

  constructor() {

    this.changePassword = 'ChangePasswordPage';
    this.notifications = 'NotificationsPage';

  }

}
