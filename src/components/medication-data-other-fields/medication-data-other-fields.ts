import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavParams, ViewController, ToastController} from 'ionic-angular';
import { AutoCompleteComponent } from 'ionic2-auto-complete';

// Providers
import {MedicationProvider} from '../../providers/medication/medication';

/**
 * Generated class for the MedicationDataOtherFieldsComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'medication-data-other-fields',
  templateUrl: 'medication-data-other-fields.html'
})
export class MedicationDataOtherFieldsComponent {

  @ViewChild('searchBar')
  searchBar: AutoCompleteComponent;

  medication: any = {
    medication_id: '',
    name: '',
    dosage: '',
    frequency: 'Once',
    time: 'Daily'
  };
  isCancelled: boolean = true;
  isOther: boolean = false;

  constructor(private navParams: NavParams,
              private viewCtrl: ViewController,
              private toastCtrl: ToastController,
              public medicationService: MedicationProvider,) {
    this.medication = this.navParams.get('medication');
    this.isOther = this.navParams.get('isOther');
  }

  addData() {
    if (this.isOther) {
      return this.addDataForOther();
    }
    if (this.medication.dosage == '' || this.medication.frequency == '' || this.medication.time == '') {
      // error
      let toast = this.toastCtrl.create({
        message: 'All fields are required.',
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
      return;
    }
    this.isCancelled = false;
    this.dismiss();
  }

  private addDataForOther() {
    if (this.medication.medication_id == '' || this.medication.name == '' || this.medication.dosage == '' || this.medication.frequency == '' || this.medication.time == '') {
      // error
      let toast = this.toastCtrl.create({
        message: 'All fields are required.',
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
      return;
    }
    this.isCancelled = false;
    this.dismiss();
  }

  onInput(event) {
    this.medication.medication_id = event.medicine_id;
    this.medication.name = event.name;
  }

  dismiss() {
    this.viewCtrl.dismiss({
      medicationData: this.medication,
      isCancelled: this.isCancelled
    });
  }

}
