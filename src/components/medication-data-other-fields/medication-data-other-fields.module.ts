import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {MedicationDataOtherFieldsComponent} from './medication-data-other-fields';
import { AutoCompleteModule } from 'ionic2-auto-complete';

@NgModule({
  declarations: [
    MedicationDataOtherFieldsComponent,
  ],
  imports: [
    IonicPageModule.forChild(MedicationDataOtherFieldsComponent),
    AutoCompleteModule
  ],
  exports: [
    MedicationDataOtherFieldsComponent
  ]
})
export class MedicationDataOtherFieldsComponentModule {
}
