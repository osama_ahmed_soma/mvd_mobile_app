import {Component} from '@angular/core';
import {IonicPage, NavParams, ViewController, ToastController} from 'ionic-angular';

// Providers
import {AllergyProvider} from '../../providers/allergy/allergy';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the EditAllergyComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'edit-allergy',
  templateUrl: 'edit-allergy.html'
})
export class EditAllergyComponent {

  tokenAndUserID: any = '';

  allergy: any;
  condition_term: string;
  severity: string;
  reaction: string;
  index: number;

  constructor(private navParams: NavParams,
              private viewCtrl: ViewController,
              private allergyService: AllergyProvider,
              private toastCtrl: ToastController,
              private authService: AuthProvider) {
    // this.index = navParams.get('index');
    // this.allergy = navParams.get('allergy');
    // this.condition_term = this.allergy.condition_term;
    // this.severity = this.allergy.severity;
    // this.reaction = this.allergy.reaction;
  }

  ionViewCanEnter(){
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if(typeof tokenAndUserID == 'object'){
        this.tokenAndUserID = tokenAndUserID;

        this.index = this.navParams.get('index');
        this.allergy = this.navParams.get('allergy');
        this.condition_term = this.allergy.condition_term;
        this.severity = this.allergy.severity;
        this.reaction = this.allergy.reaction;

        resolve();
      }
    });
  }

  async changeAllery() {
    if (this.condition_term.trim() == '' || this.severity.trim() == '' || this.reaction.trim() == '') {
      let toast = this.toastCtrl.create({
        message: 'You have to fill all fields',
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
      return;
    }
    this.allergy.condition_term = this.condition_term;
    this.allergy.severity = this.severity;
    this.allergy.reaction = this.reaction;
    this.allergyService.update(this.tokenAndUserID.token, this.allergy).subscribe(response => {
      const res: any = response;
      let cssClass = 'error';
      if (!res.bool) {
        cssClass = 'error';
      } else {
        cssClass = 'success';
      }
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: cssClass
      });
      toast.present();
      this.dismiss();
    });
  }

  dismiss() {
    this.viewCtrl.dismiss({
      allergy: this.allergy,
      index: this.index
    });
  }

}
