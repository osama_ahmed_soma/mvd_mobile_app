import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditAllergyComponent } from './edit-allergy';

@NgModule({
  declarations: [
    EditAllergyComponent,
  ],
  imports: [
    IonicPageModule.forChild(EditAllergyComponent),
  ],
  exports: [
    EditAllergyComponent
  ]
})
export class EditAllergyComponentModule {}
