import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ScheduleAppointment} from './schedule-appointment';

@NgModule({
  declarations: [
    ScheduleAppointment,
  ],
  imports: [
    IonicPageModule.forChild(ScheduleAppointment)
  ],
  exports: [
    ScheduleAppointment
  ]
})
export class ScheduleAppointmentModule {
}
