import {Component, ViewChild} from '@angular/core';
import {Platform, IonicPage, NavParams, ViewController, ToastController, LoadingController, Content} from 'ionic-angular';
import {Stripe} from '@ionic-native/stripe';

// Providers
import {ReasonProvider} from '../../providers/reason/reason';
import {DoctorProvider} from '../../providers/doctor/doctor';
import {WaiveFeeProvider} from '../../providers/waive-fee/waive-fee';
import {AppointmentProvider} from '../../providers/appointment/appointment';
import {DoctorConsultationTypesProvider} from '../../providers/doctor-consultation-types/doctor-consultation-types';
import {AuthProvider} from '../../providers/auth/auth';
import {PatientProvider} from '../../providers/patient/patient';

/**
 * Generated class for the ScheduleAppointment component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'schedule-appointment',
  templateUrl: 'schedule-appointment.html'
})
export class ScheduleAppointment {

  @ViewChild(Content) content: Content;

  tokenAndUserID: any = '';

  isDoctor: boolean = false;
  doctor: any;
  doctors: [any];
  selectedDoctor: number;
  isDoctors: boolean = false;

  stepNumber: number = 1;
  formData: any;

  reasons: Array<{
    id: number,
    name: string
  }>;

  times: Array<{
    availableID: number,
    start_time: string,
    end_time: string,
    date: string,
    full_date: string,
    start_date: number,
    end_date: number
  }>;
  timeLength: number = 0;

  currentDate: any;

  titles: Array<string> = [
    'Create New Appointment',
    'Consent to Treat',
    'Payment',
    'Thank You'
  ];

  agreed: boolean = false;

  isWaived: boolean = false;

  consultationTypes: Array<{
    id: number,
    doctor_id: number,
    duration: number,
    duration_id: number,
    rate: number
  }>;

  months: Array<{
    number: string,
    shortName: string
  }> = [
    {
      number: '01',
      shortName: 'Jan'
    },
    {
      number: '02',
      shortName: 'Feb'
    },
    {
      number: '03',
      shortName: 'Mar'
    },
    {
      number: '04',
      shortName: 'Apr'
    },
    {
      number: '05',
      shortName: 'May'
    },
    {
      number: '06',
      shortName: 'Jun'
    },
    {
      number: '07',
      shortName: 'Jul'
    },
    {
      number: '08',
      shortName: 'Aug'
    },
    {
      number: '09',
      shortName: 'Sep'
    },
    {
      number: '10',
      shortName: 'Oct'
    },
    {
      number: '11',
      shortName: 'Nov'
    },
    {
      number: '12',
      shortName: 'Dec'
    }
  ];

  years: Array<any> = [];

  isSheduled: boolean = false;

  isTime: boolean = false;

  constructor(private platform: Platform,
              private navParams: NavParams,
              private viewCtrl: ViewController,
              private reasonService: ReasonProvider,
              private doctorService: DoctorProvider,
              private toastCtrl: ToastController,
              private waiveFeeService: WaiveFeeProvider,
              private appointmentService: AppointmentProvider,
              private doctorConsultationTypesService: DoctorConsultationTypesProvider,
              private stripe: Stripe,
              private loadingCtrl: LoadingController,
              private authService: AuthProvider,
              private patientService: PatientProvider) {
    const date = new Date();
    let month: any = (date.getMonth() + 1);
    if (month > 0 && month < 10) {
      month = '0' + month;
    }
    let day: any = date.getDate();
    if (day > 0 && day < 10) {
      day = '0' + day;
    }
    this.currentDate = date.getFullYear() + '-' + month + '-' + day;

    this.formData = {
      date: '',
      time: '',
      reasonID: '',
      note: '',
      consultationTypeID: '',
      creditCard: {
        number: '',
        code: '',
        month: this.months[0].number,
        year: ''
      }
    };
    const doctor = this.navParams.get('doctor');
    if (doctor) {
      this.doctor = doctor;
      this.isDoctor = true;
    } else {
      this.isDoctors = true;
    }

    this.authService.authenticate().then(tokenAndUserID => {
      if (typeof tokenAndUserID == 'object') {
        this.tokenAndUserID = tokenAndUserID;
        this.init();
      }
    });

  }

  private async init() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.formData.date = this.currentDate;
    if(!this.isDoctor){
      // get list of all doctors
      await this.getFavouriteDoctors();
    } else {
      await this.setTime();
    }
    // this.formData.date = this.currentDate;
    await this.generateYears();
    this.formData.creditCard.year = this.years[0];
    this.reasonService.getAll(this.tokenAndUserID.token).subscribe(async response => {
      const res: any = response;
      if (res.bool) {
        this.reasons = res.reasons;
        this.formData.reasonID = this.reasons[0].id;
      }
      if(this.isDoctor){
        await this.setWaiveFee();
        if (!this.isWaived) {
          try {
            await this.setConsultationTypes();
            loading.dismiss();
          } catch (e) {
            loading.dismiss();
            this.dismiss();
          }
        } else {
          loading.dismiss();
        }
      } else {
        loading.dismiss();
      }
    });
  }

  private getFavouriteDoctors(){
    return new Promise((resolve, reject) => {
      this.patientService.getMyPhysicians(this.tokenAndUserID.token).subscribe(response => {
        const res: any = response;
        if (!res.bool) {
          let toast = this.toastCtrl.create({
            message: res.message,
            duration: 3000,
            position: 'top',
            cssClass: 'error'
          });
          toast.present();
          return resolve();
        }
        for (let i = 0; i < res.myDoctors.length; i++) {
          res.myDoctors[i].isFav = true;
        }
        this.doctors = res.myDoctors;
        resolve();
      });
    });
  }

  private setTime() {
    return new Promise(async resolve => {
      const date = this.formData.date.split('-');
      this.doctorService.getAvailableTime(this.tokenAndUserID.token, {
        doctorID: this.doctor.userid,
        date: {
          year: date[0],
          month: date[1],
          day: date[2],
        }
      }).subscribe(async (response) => {
        const res: any = response;
        if (res.bool) {
          this.currentDate = res.currentDate;
          if(!this.isTime){
            this.isTime = true;
            this.formData.date = this.currentDate;
            return resolve(await this.setTime());
          }
          this.times = res.times;
          this.timeLength = this.times.length;
          if (this.timeLength > 0) {
            this.formData.time = this.times[0].start_time;
          }
        } else {
          this.timeLength = 0;
          this.formData.time = '';
          this.times = [];
        }
        resolve();
      });
    });
  }

  private generateYears() {
    return new Promise(resolve => {
      let currentYear: number = new Date().getFullYear();
      for (let i = 0; i < 25; i++) {
        this.years.push(currentYear);
        currentYear++;
        if (i == 24) {
          return resolve();
        }
      }
    });
  }

  private setWaiveFee() {
    return new Promise(async resolve => {
      this.waiveFeeService.isWaived(this.tokenAndUserID.token, this.doctor.userid).subscribe(response => {
        const res: any = response;
        this.isWaived = res.bool;
        resolve();
      });
    });
  }

  private setConsultationTypes() {
    return new Promise(async (resolve, reject) => {
      this.doctorConsultationTypesService.getByDoctorID(this.tokenAndUserID.token, this.doctor.userid).subscribe(response => {
        const res: any = response;
        if (res.bool) {
          this.consultationTypes = res.message;
          if (this.consultationTypes.length <= 0) {
            let toast = this.toastCtrl.create({
              message: 'The selected doctor has not configured his payment settings. Please select another doctor or notify your doctor to setup his payment information.',
              duration: 3000,
              position: 'top',
              cssClass: 'error'
            });
            toast.present();
            reject();
            return;
          }
          this.formData.consultationTypeID = this.consultationTypes[0].id;
          resolve();
        }
      });
    });
  }

  onSelectChange(index){
    this.doctor = this.doctors[index];
    this.isDoctor = true;
    this.init();
  }

  async nextStep() {
    if (this.stepNumber == 1) {
      if (this.formData.time == '') {
        let toast = this.toastCtrl.create({
          message: 'Please select appointment time.',
          duration: 3000,
          position: 'top',
          cssClass: 'error'
        });
        toast.present();
        return;
      }
    }
    if (this.stepNumber == 2) {
      if (!this.agreed) {
        let toast = this.toastCtrl.create({
          message: 'Please acknowledge and agree to the Terms and Conditions and Consent to Treatment stated below.',
          duration: 3000,
          position: 'top',
          cssClass: 'error'
        });
        toast.present();
        return;
      }
      let loading = this.loadingCtrl.create({
        spinner: 'crescent'
      });
      loading.present();
      await this.setWaiveFee();
      loading.dismiss();
    }
    if (this.stepNumber == 3) {
      setTimeout(() => {
        this.viewCtrl.dismiss(true);
      }, 5000);
    }
    this.stepNumber++;
    this.content.resize();
  }

  backStep() {
    this.stepNumber--;
    this.content.resize();
  }

  async dateChanged() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    if(this.isDoctor){
      await this.setTime();
    }
    loading.dismiss();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  private getAvailableTime(time: string) {
    return new Promise(resolve => {
      if (this.timeLength > 0) {
        for (let i = 0; i < this.timeLength; i++) {
          if (this.times[i].start_time === time) {
            return resolve(this.times[i]);
          }
          if (i == (this.timeLength - 1)) {
            // last
            resolve({});
          }
        }
      } else {
        resolve({});
      }
    });
  }

  proceed() {
    this.platform.ready().then(async () => {
      let loading = this.loadingCtrl.create({
        spinner: 'crescent'
      });
      loading.present();
      if (this.isWaived) {
        const body = {
          doctorID: this.doctor.userid,
          availableTime: await this.getAvailableTime(this.formData.time),
          reasonID: this.formData.reasonID,
          notes: this.formData.note,
          consultationTypeID: this.formData.consultationTypeID,
          creditCard: '',
          token: ''
        };
        this.appointmentService.schedule(this.tokenAndUserID.token, body).subscribe(response => {
          const res: any = response;
          loading.dismiss();
          let switchClass = 'success';
          if (!res.bool) {
            switchClass = 'error';
          }
          let toast = this.toastCtrl.create({
            message: res.message,
            duration: 3000,
            position: 'top',
            cssClass: switchClass
          });
          toast.present();
          if (switchClass == 'success') {
            this.isSheduled = true;
            this.nextStep();
          }
        });
      } else {
        this.stripe.setPublishableKey(this.doctor.stripe_two);
        try {
          await this.stripe.validateCardNumber(this.formData.creditCard.number);
          await this.stripe.validateCVC(this.formData.creditCard.code);
          await this.stripe.validateExpiryDate(this.formData.creditCard.month, this.formData.creditCard.year);
          const stripeToken = await this.stripe.createCardToken({
            number: this.formData.creditCard.number,
            expMonth: this.formData.creditCard.month,
            expYear: this.formData.creditCard.year,
            cvc: this.formData.creditCard.code
          });
          const body = {
            doctorID: this.doctor.userid,
            availableTime: await this.getAvailableTime(this.formData.time),
            reasonID: this.formData.reasonID,
            notes: this.formData.note,
            consultationTypeID: this.formData.consultationTypeID,
            creditCard: this.formData.creditCard,
            token: stripeToken
          };
          this.appointmentService.schedule(this.tokenAndUserID.token, body).subscribe(response => {
            const res: any = response;
            loading.dismiss();
            let switchClass = 'success';
            if (!res.bool) {
              switchClass = 'error';
            }
            let toast = this.toastCtrl.create({
              message: res.message,
              duration: 3000,
              position: 'top',
              cssClass: switchClass
            });
            toast.present();
            if (switchClass == 'success') {
              this.isSheduled = true;
              this.nextStep();
            }
          });
        } catch (e) {
          loading.dismiss();
          let toast = this.toastCtrl.create({
            message: e,
            duration: 3000,
            position: 'top',
            cssClass: 'error'
          });
          toast.present();
        }
      }
    });
  }

}
