import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DoctorProfile } from './doctor-profile';

import { ComponentsModule } from '../components.module';


@NgModule({
  declarations: [
    DoctorProfile,
  ],
  imports: [
    IonicPageModule.forChild(DoctorProfile),
    ComponentsModule
  ],
  exports: [
    DoctorProfile
  ]
})
export class DoctorProfileModule {}
