import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ModalController, LoadingController, ToastController} from 'ionic-angular';

import {DoctorProvider} from '../../providers/doctor/doctor';
import {FavoriteProvider} from '../../providers/favorite/favorite';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the DoctorProfile component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'doctor-profile',
  templateUrl: 'doctor-profile.html'
})
export class DoctorProfile {

  tokenAndUserID: any = '';

  doctor: any;
  pet: string = 'about';
  licen: string = 'licensed';

  loading: any;

  constructor(private navCtrl: NavController,
              private modalCtrl: ModalController,
              private navParams: NavParams,
              private loadingCtrl: LoadingController,
              private doctorService: DoctorProvider,
              private favoriteService: FavoriteProvider,
              private toastCtrl: ToastController,
              private authService: AuthProvider) {
  }

  ionViewCanEnter(){
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if(typeof tokenAndUserID == 'object'){
        this.tokenAndUserID = tokenAndUserID;

        this.loading = this.navParams.get('loading');
        await this.init(this.navParams.get('doctor').userid);

        resolve();
      }
    });
  }

  private init(doctor_id) {
    return new Promise(async (resolve) => {
      if (!this.loading) {
        this.loading = this.loadingCtrl.create({
          spinner: 'crescent'
        });
        this.loading.present();
      }
      this.doctorService.getByUserID(this.tokenAndUserID.token, doctor_id).subscribe(res => {
        this.doctor = res;
        this.loading.dismiss();
        resolve();
      });
    });
  }

  async addPhysician() {
    this.favoriteService.add(this.tokenAndUserID.token, this.doctor.userid).subscribe(response => {
      const res: any = response;
      let className: string = '';
      if (res.bool) {
        className = 'success';
      } else {
        className = 'error';
      }
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: className
      });
      toast.present();
      this.doctor.isFav = true;
    });
  }

  async removePhysician() {
    this.favoriteService.remove(this.tokenAndUserID.token, this.doctor.userid).subscribe(response => {
      const res: any = response;
      let className: string = '';
      if (res.bool) {
        className = 'success';
      } else {
        className = 'error';
      }
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: className
      });
      toast.present();
      this.doctor.isFav = false;
    });
  }

  openSchedule() {
    let modal = this.modalCtrl.create('ScheduleAppointment', {
      doctor: this.doctor
    });
    modal.onDidDismiss((isRedirect: boolean = false) => {
      if(isRedirect){
        this.navCtrl.push('Appointment');
      }
    });
    modal.present();
  }

  openCompose() {
    if(this.doctor.isFav){
      this.navCtrl.push('Compose', {
        doctor: this.doctor
      });
    } else {
      let toast = this.toastCtrl.create({
        message: 'You cannot message physician unless they are added as a favorite. Please search and add favorites',
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
    }
  }

}
