import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentUpcoming } from './appointment-upcoming';

import {ComponentsModule} from '../components.module';

@NgModule({
  declarations: [
    AppointmentUpcoming,
  ],
  imports: [
    IonicPageModule.forChild(AppointmentUpcoming),
    ComponentsModule
  ],
  exports: [
    AppointmentUpcoming
  ]
})
export class AppointmentUpcomingModule {}
