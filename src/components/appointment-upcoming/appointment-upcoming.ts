import {Component} from '@angular/core';
import {IonicPage, LoadingController, NavController, Events, ActionSheetController} from 'ionic-angular';

import {AppointmentProvider} from '../../providers/appointment/appointment';
import {AuthProvider} from '../../providers/auth/auth';

// declare const OT: any;

/**
 * Generated class for the AppointmentUpcoming component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'appointment-upcoming',
  templateUrl: 'appointment-upcoming.html'
})
export class AppointmentUpcoming {

  tokenAndUserID: any = '';

  page: number = 1;
  appointments: Array<any>;
  isAppointments: boolean = true;

  constructor(private appointmentService: AppointmentProvider,
              private loadingCtrl: LoadingController,
              private navCtrl: NavController,
              private authService: AuthProvider,
              private events: Events,
              private actionSheetCtrl: ActionSheetController) {
  }

  ionViewCanEnter() {
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if (typeof tokenAndUserID == 'object') {
        this.tokenAndUserID = tokenAndUserID;
        let loading = this.loadingCtrl.create({
          spinner: 'crescent'
        });
        loading.present();
        await this.initData();
        loading.dismiss();
        resolve();
      }
    });
  }

  private initData() {
    return new Promise(async (resolve) => {
      this.page = 1;
      this.appointmentService.getUpcomingData(this.tokenAndUserID.token, this.page).subscribe(response => {
        const res: any = response;
        this.page++;
        if (res.bool) {
          this.appointments = res.appointments;
          if (this.appointments.length <= 0) {
            this.isAppointments = false;
          } else {
            // this.events.publish('appointment:upcoming:check', this.appointments[0]);
          }
        }
        resolve();
      });
    });
  }

  openDoctor(doctorID: number) {
    this.navCtrl.push('DoctorProfile', {
      doctor: {
        userid: doctorID
      }
    });
  }

  async doInfinite(infiniteScroll) {
    this.appointmentService.getUpcomingData(this.tokenAndUserID.token, this.page).subscribe(response => {
      const res: any = response;
      this.page++;
      if (res.bool) {
        if (res.appointments.length <= 0) {
          infiniteScroll.enable(false);
        }
        if (res.appointments.length > 0) {
          this.appointments.push.apply(this.appointments, res.appointments);
        }
        infiniteScroll.complete();
      }
    });
  }

  async doRefresh(refresher) {
    await this.initData();
    refresher.complete();
  }

  startVideo(appointment) {
    this.events.publish('waiting:room:open', true, appointment);
  }

  openActions(appointment) {
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Start Appointment',
          icon: 'open',
          handler: () => {
            this.startVideo(appointment);
          }
        },
        {
          text: 'Send Message',
          icon: 'send',
          handler: () => {
            this.navCtrl.push('Compose', {
              userid: appointment.doctors_id,
              firstname: appointment.firstname,
              lastname: appointment.lastname
            });
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {

          }
        }
      ]
    });
    actionSheet.present();
  }


}
