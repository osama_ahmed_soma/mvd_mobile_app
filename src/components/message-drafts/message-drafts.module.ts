import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MessageDraftsComponent } from './message-drafts';

// custom component
import {ComponentsModule} from '../components.module';

@NgModule({
  declarations: [
    MessageDraftsComponent,
  ],
  imports: [
    IonicPageModule.forChild(MessageDraftsComponent),
    ComponentsModule
  ],
  exports: [
    MessageDraftsComponent
  ]
})
export class MessageDraftsComponentModule {}
