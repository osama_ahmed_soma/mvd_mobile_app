import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddSurgeryComponent } from './add-surgery';

@NgModule({
  declarations: [
    AddSurgeryComponent,
  ],
  imports: [
    IonicPageModule.forChild(AddSurgeryComponent),
  ],
  exports: [
    AddSurgeryComponent
  ]
})
export class AddSurgeryComponentModule {}
