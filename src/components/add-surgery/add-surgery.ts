import {Component} from '@angular/core';
import {IonicPage, ViewController, ToastController, LoadingController} from 'ionic-angular';

// Providers
import {SurgeryProvider} from '../../providers/surgery/surgery';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the AddSurgeryComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'add-surgery',
  templateUrl: 'add-surgery.html'
})
export class AddSurgeryComponent {

  tokenAndUserID: any = '';

  condition_term: string = '';
  source: number;

  years: Array<number> = [];

  constructor(private viewCtrl: ViewController,
              private toastCtrl: ToastController,
              private loadingCtrl: LoadingController,
              private surgeryService: SurgeryProvider,
              private authService: AuthProvider) {
    const currentYear: number = new Date().getFullYear();
    this.source = currentYear;
    let firstYear: number = currentYear - 100;
    let lastYear: number = firstYear + 100;
    for (let i = firstYear; i <= lastYear; i++) {
      this.years.push(i);
    }
  }

  ionViewCanEnter(){
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if(typeof tokenAndUserID == 'object'){
        this.tokenAndUserID = tokenAndUserID;
        resolve();
      }
    });
  }

  async addSurgery() {
    if (this.condition_term.trim() == '' || this.source <= 0) {
      let toast = this.toastCtrl.create({
        message: 'You have to fill all fields',
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
      return;
    }
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.surgeryService.insert(this.tokenAndUserID.token, {
      condition_term: this.condition_term.trim(),
      source: this.source
    }).subscribe(response => {
      const res: any = response;
      loading.dismiss();
      let cssClass = 'error';
      if (res.bool) {
        cssClass = 'success';
      }
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: cssClass
      });
      toast.present();
      if (res.bool) {
        this.dismiss(res.insertedData);
      }
    });
  }

  dismiss(object?) {
    this.viewCtrl.dismiss(object);
  }

}
