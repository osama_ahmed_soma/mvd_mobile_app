import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditHealthComponent } from './edit-health';

@NgModule({
  declarations: [
    EditHealthComponent,
  ],
  imports: [
    IonicPageModule.forChild(EditHealthComponent),
  ],
  exports: [
    EditHealthComponent
  ]
})
export class EditHealthComponentModule {}
