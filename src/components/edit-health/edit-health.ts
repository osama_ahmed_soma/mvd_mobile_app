import {Component} from '@angular/core';
import {IonicPage, NavParams, ViewController, ToastController} from 'ionic-angular';

// Providers
import {HealthProvider} from '../../providers/health/health';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the EditHealthComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'edit-health',
  templateUrl: 'edit-health.html'
})
export class EditHealthComponent {

  tokenAndUserID: any = '';

  condition: any;
  condition_term: string;
  index: number;

  constructor(private navParams: NavParams,
              private viewCtrl: ViewController,
              private healthService: HealthProvider,
              private toastCtrl: ToastController,
              private authService: AuthProvider) {
  }

  ionViewCanEnter(){
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if(typeof tokenAndUserID == 'object'){
        this.tokenAndUserID = tokenAndUserID;

        this.index = this.navParams.get('index');
        this.condition = this.navParams.get('condition');
        this.condition_term = this.condition.condition_term;

        resolve();
      }
    });
  }

  async changeCondition() {
    if (this.condition_term.trim() == '') {
      let toast = this.toastCtrl.create({
        message: 'You have to fill all fields',
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
      return;
    }
    this.condition.condition_term = this.condition_term;
    this.healthService.update(this.tokenAndUserID.token, this.condition).subscribe(response => {
      const res: any = response;
      let cssClass = 'error';
      if (!res.bool) {
        cssClass = 'error';
      } else {
        cssClass = 'success';
      }
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: cssClass
      });
      toast.present();
      this.dismiss();
    });
  }

  dismiss() {
    this.viewCtrl.dismiss({
      condition: this.condition,
      index: this.index
    });
  }


}
