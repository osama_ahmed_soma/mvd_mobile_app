import {Component} from '@angular/core';
import {NavParams, ViewController, LoadingController, ToastController, Events} from 'ionic-angular';

// Providers
import {AuthProvider} from '../../providers/auth/auth';
import {NotificationProvider} from '../../providers/notification/notification';

/**
 * Generated class for the NotificationComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'notification',
  templateUrl: 'notification.html'
})
export class NotificationComponent {

  notifications: Array<any>;
  tokenUserIDAndData: any;

  constructor(private navParams: NavParams,
              private viewCtrl: ViewController,
              private authService: AuthProvider,
              private notificationService: NotificationProvider,
              private loadingCtrl: LoadingController,
              private toastCtrl: ToastController,
              private events: Events) {
    this.notifications = this.navParams.get('notifications');
    this.tokenUserIDAndData = this.authService.getData();
  }

  close() {
    this.viewCtrl.dismiss();
  }

  clearAll() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.notificationService.clearAll(this.tokenUserIDAndData.token).subscribe(response => {
      const res: any = response;
      loading.dismiss();
      if (!res.bool) {
        let toast = this.toastCtrl.create({
          message: res.message,
          duration: 3000,
          position: 'top',
          cssClass: 'error'
        });
        toast.present();
        return;
      }
      this.notifications = [];
      this.events.publish('notification:cleared:all');
    });
  }

  clear(index) {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.notificationService.clear(this.tokenUserIDAndData.token, this.notifications[index].id).subscribe(response => {
      const res: any = response;
      loading.dismiss();
      if (!res.bool) {
        let toast = this.toastCtrl.create({
          message: res.message,
          duration: 3000,
          position: 'top',
          cssClass: 'error'
        });
        toast.present();
        return;
      }
      this.notifications.splice(index, 1);
      this.events.publish('notification:clear:one');
    });
  }

}
