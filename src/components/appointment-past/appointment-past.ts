import {Component} from '@angular/core';
import {
  IonicPage,
  NavController,
  LoadingController,
  ModalController,
  ActionSheetController,
  Platform,
  ToastController
} from 'ionic-angular';

import {FileTransfer, FileTransferObject} from '@ionic-native/file-transfer';
import {File} from '@ionic-native/file';
import {FileOpener} from '@ionic-native/file-opener';

import {AppointmentProvider} from '../../providers/appointment/appointment';
import {AuthProvider} from '../../providers/auth/auth';

// Config
import * as MvdConfig from '../../config/main.config';

declare const window: any;

/**
 * Generated class for the AppointmentPast component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'appointment-past',
  templateUrl: 'appointment-past.html'
})
export class AppointmentPast {

  tokenAndUserID: any = '';

  page: number = 1;
  appointments: any;
  isAppointments: boolean = true;

  constructor(private appointmentService: AppointmentProvider,
              private loadingCtrl: LoadingController,
              private navCtrl: NavController,
              private authService: AuthProvider,
              private modalCtrl: ModalController,
              private actionSheetCtrl: ActionSheetController,
              private transfer: FileTransfer,
              private file: File,
              private platform: Platform,
              private fileOpener: FileOpener,
              private toastCtrl: ToastController) {
    // let loading = this.loadingCtrl.create({
    //   spinner: 'crescent'
    // });
    // loading.present();
    // this.initData().then(() => {
    //   loading.dismiss();
    // });
  }

  ionViewCanEnter() {
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if (typeof tokenAndUserID == 'object') {
        this.tokenAndUserID = tokenAndUserID;
        let loading = this.loadingCtrl.create({
          spinner: 'crescent'
        });
        loading.present();
        await this.initData();
        loading.dismiss();
        resolve();
      }
    });
  }

  private initData() {
    return new Promise(async (resolve) => {
      this.page = 1;
      this.appointmentService.getPastData(this.tokenAndUserID.token, this.page).subscribe(response => {
        const res: any = response;
        this.page++;
        if (res.bool) {
          this.appointments = res.appointments;
          if (this.appointments.length <= 0) {
            this.isAppointments = false;
          }
        }
        resolve();
      });
    });
  }

  openDoctor(doctorID: number) {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.navCtrl.push('DoctorProfile', {
      doctor: {
        userid: doctorID
      },
      loading: loading
    });
  }

// 03312382683
  openReport(appointment) {
    let modal = this.modalCtrl.create('ConsultationReportComponent', {
      appointment: appointment
    });
    modal.onDidDismiss(() => {
      // this.events.publish('waiting:room:closed');
    });
    modal.present();
  }

  private downloadReport(appointment) {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    const fileTransfer: FileTransferObject = this.transfer.create();
    let directory: string = this.file.externalDataDirectory;
    if (this.platform.is('ios')) {
      directory = this.file.syncedDataDirectory;
    }
    const url = MvdConfig.config.socketURL + '/Phpapi/downloadPdf/' + appointment.id;
    fileTransfer.download(url, this.file.dataDirectory + 'Report_pdf_' + appointment.id + '.pdf').then((entry) => {
      console.log('download complete: ' + entry.toURL());
      const downloadText = entry.toURL();
      window.resolveLocalFileSystemURL(downloadText, fileEntry => {
        loading.dismiss();
        fileEntry.file(file => {
          const type = file.type;
          this.fileOpener.open(downloadText, type).then(() => {
          }).catch(e => console.log('Error openening file', e));
        });
      }, e => {
        loading.dismiss();
        let toast = this.toastCtrl.create({
          message: e.message,
          duration: 3000,
          position: 'top',
          cssClass: 'error'
        });
        toast.present();
      });
    }, (error) => {
      // handle error
      loading.dismiss();
      let toast = this.toastCtrl.create({
        message: error.message,
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
    });
  }

  openActions(appointment) {
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'View Report',
          icon: 'document',
          handler: () => {
            this.openReport(appointment);
          }
        },
        {
          text: 'Download Report',
          icon: 'download',
          handler: () => {
            this.downloadReport(appointment);
          }
        },
        {
          text: 'Send Message',
          icon: 'send',
          handler: () => {
            this.navCtrl.push('Compose', {
              userid: appointment.doctors_id,
              firstname: appointment.firstname,
              lastname: appointment.lastname
            });
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {

          }
        }
      ]
    });
    actionSheet.present();
  }

  async doInfinite(infiniteScroll) {
    this.appointmentService.getPastData(this.tokenAndUserID.token, this.page).subscribe(response => {
      const res: any = response;
      this.page++;
      if (res.bool) {
        if (res.appointments.length <= 0) {
          infiniteScroll.enable(false);
        }
        if (res.appointments.length > 0) {
          this.appointments.push.apply(this.appointments, res.appointments);
        }
        infiniteScroll.complete();
      }
    });
  }

  async doRefresh(refresher) {
    await this.initData();
    refresher.complete();
  }

}
