import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {AppointmentPast} from './appointment-past';

import {ComponentsModule} from '../components.module';

@NgModule({
  declarations: [
    AppointmentPast,
  ],
  imports: [
    IonicPageModule.forChild(AppointmentPast),
    ComponentsModule
  ],
  exports: [
    AppointmentPast
  ]
})
export class AppointmentPastModule {
}
