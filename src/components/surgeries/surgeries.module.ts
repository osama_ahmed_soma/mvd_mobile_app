import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SurgeriesComponent } from './surgeries';
import { ComponentsModule } from '../components.module';

@NgModule({
  declarations: [
    SurgeriesComponent,
  ],
  imports: [
    IonicPageModule.forChild(SurgeriesComponent),
    ComponentsModule
  ],
  exports: [
    SurgeriesComponent
  ]
})
export class SurgeriesComponentModule {}
