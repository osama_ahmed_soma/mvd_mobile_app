import {Component} from '@angular/core';
import {
  IonicPage,
  NavParams,
  ToastController,
  AlertController,
  ModalController,
  LoadingController
} from 'ionic-angular';
import {isUndefined} from "util";

// Providers
import {PatientProvider} from '../../providers/patient/patient';
import {SurgeryProvider} from '../../providers/surgery/surgery';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the SurgeriesComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage({
  name: 'surgeries-component',
  segment: 'surgeries-component-path'
})
@Component({
  selector: 'surgeries',
  templateUrl: 'surgeries.html'
})
export class SurgeriesComponent {

  tokenAndUserID: any = '';

  token: string;
  is_surgeries: boolean = false;
  loading: any;
  surgeryData: any = [];

  editedIndex: number;

  constructor(private navParams: NavParams,
              private patientService: PatientProvider,
              private surgeryService: SurgeryProvider,
              private toastCtrl: ToastController,
              private alertCtrl: AlertController,
              private modalCtrl: ModalController,
              private loadingCtrl: LoadingController,
              private authService: AuthProvider) {
  }

  ionViewCanEnter(){
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if(typeof tokenAndUserID == 'object'){
        this.tokenAndUserID = tokenAndUserID;

        this.loading = this.navParams.get('loading');
        let isSurgeries = this.navParams.get('is_surgeries');
        this.token = this.tokenAndUserID.token;
        if (isUndefined(this.loading) && isUndefined(isSurgeries)) {
          if (isUndefined(this.loading)) {
            this.loading = this.loadingCtrl.create({
              spinner: 'crescent'
            });
            this.loading.present();
          }
          this.patientService.getMyHealthData(this.token).subscribe(response => {
            const res: any = response;
            isSurgeries = res.message.patient.is_health;
            this.initialize(isSurgeries);
          });
        } else {
          this.initialize(isSurgeries);
        }

        resolve();
      }
    });
  }

  private initialize(isSurgeries) {
    if (isSurgeries == 2) {
      this.is_surgeries = true;
    } else if (isSurgeries == 1) {
      this.is_surgeries = false;
    } else {
      this.is_surgeries = null;
    }
    // this.is_surgeries = (isSurgeries == 2) ? true : false;
    this.surgeryService.get(this.token).subscribe(response => {
      const res: any = response;
      if (!res.bool) {
        let toast = this.toastCtrl.create({
          message: res.message,
          duration: 3000,
          position: 'top',
          cssClass: 'error'
        });
        toast.present();
      } else {
        this.surgeryData = res.message;
      }
      this.loading.dismiss();
    });
  }

  changeIsSurgery(isSurgeries) {
    this.is_surgeries = isSurgeries;
    this.patientService.updateIsSurgeries(this.token, this.is_surgeries).subscribe();
  }

  openAdd() {
    let modal = this.modalCtrl.create('AddSurgeryComponent');
    modal.onDidDismiss(data => {
      if (typeof data !== 'undefined') {
        this.surgeryData.push(data);
      }
    });
    modal.present();
  }

  openEdit(index) {
    this.editedIndex = index;
    const surgery = this.surgeryData[this.editedIndex];
    let modal = this.modalCtrl.create('EditSurgeryComponent', {
      surgery: surgery,
      index: this.editedIndex
    });
    modal.onDidDismiss(data => {
      this.surgeryData[data.index] = data.surgery;
    });
    modal.present();
  }

  removeDialog(index) {
    let alert = this.alertCtrl.create({
      title: this.surgeryData[index].condition_term,
      message: 'Are you sure that you want to delete this surgery?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.remove(index);
          }
        }
      ],
      enableBackdropDismiss: false
    });
    alert.present();
  }

  private remove(index) {
    this.surgeryService.remove(this.token, this.surgeryData[index].id).subscribe(response => {
      const res: any = response;
      let cssClass = 'error';
      if (!res.bool) {
        cssClass = 'error';
      } else {
        cssClass = 'success';
        this.surgeryData.splice(index, 1);
      }
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: cssClass
      });
      toast.present();
    });
  }

}
