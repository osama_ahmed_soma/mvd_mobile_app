import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {AllergiesComponent} from './allergies';

import {ComponentsModule} from '../components.module';

@NgModule({
  declarations: [
    AllergiesComponent,
  ],
  imports: [
    IonicPageModule.forChild(AllergiesComponent),
    ComponentsModule
  ],
  exports: [
    AllergiesComponent
  ]
})
export class AllergiesComponentModule {
}
