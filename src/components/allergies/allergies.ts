import {Component} from '@angular/core';
import {
  IonicPage,
  NavParams,
  ToastController,
  AlertController,
  ModalController,
  LoadingController
} from 'ionic-angular';
import {isUndefined} from "util";

// Providers
import {PatientProvider} from '../../providers/patient/patient';
import {AllergyProvider} from '../../providers/allergy/allergy';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the AllergiesComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage({
  name: 'allergies-component',
  segment: 'allergies-component-path'
})
@Component({
  selector: 'allergies',
  templateUrl: 'allergies.html'
})
export class AllergiesComponent {

  tokenAndUserID: any = '';

  token: string;
  is_allergies: boolean = false;
  loading: any;
  allergyData: any = [];

  isAllergies: any;

  editedIndex: number;

  constructor(private navParams: NavParams,
              private patientService: PatientProvider,
              private allergyService: AllergyProvider,
              private toastCtrl: ToastController,
              private alertCtrl: AlertController,
              private modalCtrl: ModalController,
              private loadingCtrl: LoadingController,
              private authService: AuthProvider) {
  }

  ionViewCanEnter(){
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if(typeof tokenAndUserID == 'object'){
        this.tokenAndUserID = tokenAndUserID;
        this.token = this.tokenAndUserID.token;
        this.loading = this.navParams.get('loading');
        this.isAllergies = this.navParams.get('is_allergies');
        if (isUndefined(this.loading) && isUndefined(this.isAllergies)) {
          if (isUndefined(this.loading)) {
            this.loading = this.loadingCtrl.create({
              spinner: 'crescent'
            });
            this.loading.present();
          }
          this.patientService.getMyHealthData(this.token).subscribe(response => {
            const res: any = response;
            this.isAllergies = res.message.patient.is_allergies;
            this.initialize(this.isAllergies);
            resolve();
          });
        } else {
          this.initialize(this.isAllergies);
          resolve();
        }
      }
    });
  }

  private initialize(isAllergies) {
    if (isAllergies == 2) {
      this.is_allergies = true;
    } else if (isAllergies == 1) {
      this.is_allergies = false;
    } else {
      this.is_allergies = null;
    }
    // this.is_allergies = (isAllergies == 2) ? true : false;
    this.allergyService.get(this.token).subscribe(response => {
      const res: any = response;
      if (!res.bool) {
        let toast = this.toastCtrl.create({
          message: res.message,
          duration: 3000,
          position: 'top',
          cssClass: 'error'
        });
        toast.present();
      } else {
        this.allergyData = res.message;
      }
      this.loading.dismiss();
    });
  }

  changeIsAlleries(isAllergies) {
    this.is_allergies = isAllergies;
    this.patientService.updateIsAllergies(this.token, this.is_allergies).subscribe();
  }

  openAdd() {
    let modal = this.modalCtrl.create('AddAllergyComponent');
    modal.onDidDismiss(data => {
      this.initialize(this.isAllergies);
    });
    modal.present();
  }

  openEdit(index) {
    this.editedIndex = index;
    const allergy = this.allergyData[this.editedIndex];
    let modal = this.modalCtrl.create('EditAllergyComponent', {
      allergy: allergy,
      index: this.editedIndex
    });
    modal.onDidDismiss(data => {
      this.allergyData[data.index] = data.allergy;
    });
    modal.present();
  }

  removeDialog(index) {
    let alert = this.alertCtrl.create({
      title: this.allergyData[index].condition_term,
      message: 'Are you sure that you want to delete this allergy?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.remove(index);
          }
        }
      ],
      enableBackdropDismiss: false
    });
    alert.present();
  }

  private remove(index) {
    this.allergyService.remove(this.token, this.allergyData[index].id).subscribe(response => {
      const res: any = response;
      let cssClass = 'error';
      if (!res.bool) {
        cssClass = 'error';
      } else {
        cssClass = 'success';
        this.allergyData.splice(index, 1);
      }
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: cssClass
      });
      toast.present();
    });
  }

}
