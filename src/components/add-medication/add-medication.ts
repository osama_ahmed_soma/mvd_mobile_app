import {Component, ViewChild} from '@angular/core';
import {IonicPage, ViewController, ToastController, LoadingController} from 'ionic-angular';

import { AutoCompleteComponent } from 'ionic2-auto-complete';

// Providers
import {MedicationProvider} from '../../providers/medication/medication';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the AddMedicationComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'add-medication',
  templateUrl: 'add-medication.html'
})
export class AddMedicationComponent {
  tokenAndUserID: any = '';

  @ViewChild('searchBar')
  searchBar: AutoCompleteComponent;

  condition_term: string = '';
  dosage: string = '';
  frequency: string = 'Once';
  time: string = 'Daily';

  constructor(private viewCtrl: ViewController,
              private toastCtrl: ToastController,
              private loadingCtrl: LoadingController,
              public medicationService: MedicationProvider,
              private authService: AuthProvider) {
  }

  ionViewCanEnter(){
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if(typeof tokenAndUserID == 'object'){
        this.tokenAndUserID = tokenAndUserID;
        resolve();
      }
    });
  }

  onInput(event) {
    // console.log(123);
    this.condition_term = event.name;
  }

  async addMedication() {
    if (this.condition_term.trim() == '' || this.dosage.trim() == '' || this.frequency.trim() == '' || this.time.trim() == '') {
      let toast = this.toastCtrl.create({
        message: 'You have to fill all fields',
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
      return;
    }
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.medicationService.insert(this.tokenAndUserID.token, {
      condition_term: this.condition_term.trim(),
      source: 'Self Reported',
      dosage: this.dosage.trim(),
      frequency: this.frequency.trim(),
      time: this.time.trim()
    }).subscribe(response => {
      const res: any = response;
      loading.dismiss();
      let cssClass = 'error';
      if (res.bool) {
        cssClass = 'success';
      }
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: cssClass
      });
      toast.present();
      if (res.bool) {
        this.dismiss(res.insertedData);
      }
    });

  }

  dismiss(object?) {
    this.viewCtrl.dismiss(object);
  }

}
