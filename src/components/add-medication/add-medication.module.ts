import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';

import { AutoCompleteModule } from 'ionic2-auto-complete';

import {AddMedicationComponent} from './add-medication';

@NgModule({
  declarations: [
    AddMedicationComponent,
  ],
  imports: [
    IonicPageModule.forChild(AddMedicationComponent),
    AutoCompleteModule
  ],
  exports: [
    AddMedicationComponent
  ]
})
export class AddMedicationComponentModule {
}
