import {Component} from '@angular/core';
import {IonicPage, ViewController, ToastController, LoadingController} from 'ionic-angular';

// Providers
import {AllergyProvider} from '../../providers/allergy/allergy';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the AddAllergyComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'add-allergy',
  templateUrl: 'add-allergy.html'
})
export class AddAllergyComponent {

  tokenAndUserID: any = '';

  allAllergies: Array<any> = [];

  multipleConditionTerms: Array<string> = [];

  condition_term: string = '';
  severity: string = 'Severity';
  reaction: string = '';

  isOther: boolean = false;

  constructor(private viewCtrl: ViewController,
              private toastCtrl: ToastController,
              private loadingCtrl: LoadingController,
              private allergyService: AllergyProvider,
              private authService: AuthProvider) {

  }

  ionViewCanEnter() {
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if (typeof tokenAndUserID == 'object') {
        this.tokenAndUserID = tokenAndUserID;
        resolve();
      }
    });
  }

  ionViewDidEnter() {
    this.allergyService.getAllPreDefined(this.tokenAndUserID.token).subscribe(response => {
      const res: any = response;
      if (!res.bool) {
        let toast = this.toastCtrl.create({
          message: res.message,
          duration: 3000,
          position: 'top',
          cssClass: 'error'
        });
        toast.present();
        return;
      }
      this.allAllergies = res.message;
    });
  }

  checkMultipleAllergies(evn) {
    if (this.multipleConditionTerms.indexOf('other') !== -1) {
      this.isOther = true;
    } else {
      this.isOther = false;
    }
  }

  async addAllergy() {
    if(this.isOther) {
      if (this.condition_term.trim() == '' || this.severity.trim() == '' || this.reaction.trim() == '') {
        let toast = this.toastCtrl.create({
          message: 'You have to fill all fields',
          duration: 3000,
          position: 'top',
          cssClass: 'error'
        });
        toast.present();
        return;
      }
    } else {
      if (this.multipleConditionTerms.length <= 0 || this.severity.trim() == '' || this.reaction.trim() == '') {
        let toast = this.toastCtrl.create({
          message: 'You have to fill all fields',
          duration: 3000,
          position: 'top',
          cssClass: 'error'
        });
        toast.present();
        return;
      }
    }
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.allergyService.insert(this.tokenAndUserID.token, {
      condition_term: this.condition_term.trim(),
      multiple_condition_terms: this.multipleConditionTerms,
      source: 'Self Reported',
      severity: this.severity.trim(),
      reaction: this.reaction.trim()
    }).subscribe(response => {
      const res: any = response;
      loading.dismiss();
      let cssClass = 'error';
      if (res.bool) {
        cssClass = 'success';
      }
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: cssClass
      });
      toast.present();
      if (res.bool) {
        // this.dismiss(res.insertedData);
        this.dismiss();
      }
    });

  }

  dismiss(object?) {
    this.viewCtrl.dismiss(object);
  }

}
