import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddAllergyComponent } from './add-allergy';

@NgModule({
  declarations: [
    AddAllergyComponent,
  ],
  imports: [
    IonicPageModule.forChild(AddAllergyComponent),
  ],
  exports: [
    AddAllergyComponent
  ]
})
export class AddAllergyComponentModule {}
