import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {MessageView} from './message-view';

// custom component
import {ComponentsModule} from '../components.module';

@NgModule({
  declarations: [
    MessageView,
  ],
  imports: [
    IonicPageModule.forChild(MessageView),
    ComponentsModule
  ],
  exports: [
    MessageView
  ]
})
export class MessageViewModule {
}
