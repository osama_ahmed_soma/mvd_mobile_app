import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController, ToastController} from 'ionic-angular';

// Providers
import {MessageProvider} from '../../providers/message/message';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the MessageView component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'message-view',
  templateUrl: 'message-view.html'
})
export class MessageView {

  tokenAndUserID: any = '';

  messageID: any;
  type: string;

  message: any;
  replies: Array<any> = [];

  loading: any;

  constructor(public navCtrl: NavController,
              private navParams: NavParams,
              private messageService: MessageProvider,
              private toastCtrl: ToastController,
              private loadingCtrl: LoadingController,
              private authService: AuthProvider) {
    // let loading = loadingCtrl.create({
    //   spinner: 'crescent'
    // });
    // loading.present();
    // this.messageID = navParams.get('messageID');
    // this.type = navParams.get('type');
    // this.doRefresh({
    //   complete: () => {
    //     loading.dismiss();
    //   }
    // });
  }

  ionViewCanEnter(){
    return new Promise(async (resolve) => {
      this.loading = this.loadingCtrl.create({
        spinner: 'crescent'
      });
      this.loading.present();
      const tokenAndUserID = await this.authService.authenticate();
      if(typeof tokenAndUserID == 'object'){
        this.tokenAndUserID = tokenAndUserID;
        resolve();
      }
    });
  }

  ionViewDidEnter(){
    // let loading = this.loadingCtrl.create({
    //   spinner: 'crescent'
    // });
    // loading.present();
    this.messageID = this.navParams.get('messageID');
    this.type = this.navParams.get('type');
    this.doRefresh({
      complete: () => {
        this.loading.dismiss();
      }
    });
  }

  async doRefresh(refresher) {
    await this.getMessages(this.tokenAndUserID.token);
    refresher.complete();
  }

  private getMessages(token) {
    return new Promise(resolve => {
      this.messageService.viewMessages(token, this.messageID, this.type).subscribe(response => {
        const res: any = response;
        if (!res.bool) {
          let toast = this.toastCtrl.create({
            message: res.message,
            duration: 3000,
            position: 'top',
            cssClass: 'error'
          });
          toast.present();
          return;
        }
        this.message = res.message.messages.message;
        this.replies = res.message.messages.replies;
        resolve();
      });
    });
  }

  openCompose() {
    let obj;
    if(this.type == 'sent'){
      obj = {
        userid: this.message.to_userid,
        firstname: this.message.receiver_name.split(" ")[0],
        lastname: this.message.receiver_name.split(" ")[1]
      };
    } else {
      obj = {
        userid: this.message.from_userid,
        firstname: this.message.sender_name.split(" ")[0],
        lastname: this.message.sender_name.split(" ")[1]
      };
    }
    this.navCtrl.push('Compose', {
      doctor: obj,
      subject: this.message.subject
    });
  }
}
