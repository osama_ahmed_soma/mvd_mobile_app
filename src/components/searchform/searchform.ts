import {Component} from '@angular/core';
import {NavParams, ToastController, ViewController, LoadingController} from 'ionic-angular';

import {SearchProvider} from '../../providers/search/search';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the Searchform component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'searchform',
  templateUrl: 'searchform.html'
})
export class Searchform {

  tokenAndUserID: any = '';

  states: Array<{ id: number, code: string, name: string }>;
  specialties: Array<{ id: number, name: string }>;

  state: number = 0;
  specialty: string = '';
  doctor_name: string = '';
  doctor_phone_number: string = '';
  onlineNow: boolean = false;
  isStateAssigned: boolean = false;

  constructor(private params: NavParams,
              private searchService: SearchProvider,
              private toastCtrl: ToastController,
              private viewCtrl: ViewController,
              private loadingCtrl: LoadingController,
              private authService: AuthProvider) {
    // this.states = params.get('states');
    // this.specialties = params.get('specialties');
    // this.state = this.states[0].id;
  }

  ionViewCanEnter() {
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if (typeof tokenAndUserID == 'object') {
        this.tokenAndUserID = tokenAndUserID;
        this.states = this.params.get('states');
        this.specialties = this.params.get('specialties');
        if (this.tokenAndUserID.data.userData.state_id !== null && !this.isStateAssigned) {
          this.state = this.tokenAndUserID.data.userData.state_id;
          this.isStateAssigned = true;
        }
        // this.state = this.states[0].id;
        resolve();
      }
    });
  }

  private getState(state_id: number) {
    return new Promise(resolve => {
      for (let i = 0; i < this.states.length; i++) {
        if (this.states[i].id == state_id) {
          resolve(this.states[i]);
        }
      }
    });
  }

  stateChange(evt) {
    this.state = evt;
  }

  async searchSubmit() {
    console.log(this.state);
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.searchService.search(this.tokenAndUserID.token, {
      query: this.doctor_name,
      phone_number: this.doctor_phone_number,
      state: this.state,
      specialty: this.specialty,
      online_now: this.onlineNow
    }).subscribe(async (response) => {
      const res: any = response;
      if (!res.bool) {
        loading.dismiss();
        let toast = this.toastCtrl.create({
          message: res.message,
          duration: 3000,
          position: 'top',
          cssClass: 'error'
        });
        toast.present();
        return;
      }
      let selectedState = null;
      if (this.state) {
        selectedState = await this.getState(this.state);
      }
      this.viewCtrl.dismiss({
        search_data: {
          selected_state: selectedState,
          selected_specialty: this.specialty,
          query: this.doctor_name,
          phone_number: this.doctor_phone_number,
          online_now: this.onlineNow
        },
        doctors: res.search_result,
        instances: {
          loading: loading
        }
      });
      // if (!this.onlineNow) {
      //   this.viewCtrl.dismiss({
      //     search_data: {
      //       selected_state: selectedState,
      //       selected_specialty: this.specialty,
      //       query: this.doctor_name,
      //       phone_number: this.doctor_phone_number
      //     },
      //     doctors: res.search_result,
      //     instances: {
      //       loading: loading
      //     }
      //   });
      //   return;
      // }
      // emit socket event with searched doctors data for check and receive online doctors only
      // this.socket.emit('checkOnlineDoctors', JSON.stringify(res.search_result));
      // this.socket.on('getOnlineDoctors', data => {
      //   const search_result = JSON.parse(data);
      //   this.viewCtrl.dismiss({
      //     search_data: {
      //       selected_state: selectedState,
      //       selected_specialty: this.specialty,
      //       query: this.doctor_name,
      //       phone_number: this.doctor_phone_number
      //     },
      //     doctors: search_result,
      //     instances: {
      //       loading: loading
      //     }
      //   });
      // });
    });
  }

}
