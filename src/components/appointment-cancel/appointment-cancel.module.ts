import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentCancel } from './appointment-cancel';

import {ComponentsModule} from '../components.module';

@NgModule({
  declarations: [
    AppointmentCancel,
  ],
  imports: [
    IonicPageModule.forChild(AppointmentCancel),
    ComponentsModule
  ],
  exports: [
    AppointmentCancel
  ]
})
export class AppointmentCancelModule {}
