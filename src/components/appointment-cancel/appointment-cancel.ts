import {Component} from '@angular/core';
import {IonicPage, NavController, LoadingController} from 'ionic-angular';

import {AppointmentProvider} from '../../providers/appointment/appointment';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the AppointmentCancel component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'appointment-cancel',
  templateUrl: 'appointment-cancel.html'
})
export class AppointmentCancel {

  tokenAndUserID: any = '';

  page: number = 1;
  appointments: any;
  isAppointments: boolean = true;

  constructor(private appointmentService: AppointmentProvider,
              private loadingCtrl: LoadingController,
              private navCtrl: NavController,
              private authService: AuthProvider) {
    // let loading = this.loadingCtrl.create({
    //   spinner: 'crescent'
    // });
    // loading.present();
    // this.initData().then(() => {
    //   loading.dismiss();
    // });
  }

  ionViewCanEnter() {
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if (typeof tokenAndUserID == 'object') {
        this.tokenAndUserID = tokenAndUserID;
        let loading = this.loadingCtrl.create({
          spinner: 'crescent'
        });
        loading.present();
        await this.initData();
        loading.dismiss();
        resolve();
      }
    });
  }

  private initData() {
    return new Promise(async (resolve) => {
      this.page = 1;
      this.appointmentService.getCancelData(this.tokenAndUserID.token, this.page).subscribe(response => {
        const res: any = response;
        this.page++;
        if (res.bool) {
          this.appointments = res.appointments;
          if (this.appointments.length <= 0) {
            this.isAppointments = false;
          }
        }
        resolve();
      });
    });
  }

  openDoctor(doctorID: number) {
    this.navCtrl.push('DoctorProfile', {
      doctor: {
        userid: doctorID
      }
    });
  }

  async doInfinite(infiniteScroll) {
    this.appointmentService.getCancelData(this.tokenAndUserID.token, this.page).subscribe(response => {
      const res: any = response;
      this.page++;
      if (res.bool) {
        if (res.appointments.length <= 0) {
          infiniteScroll.enable(false);
        }
        if (res.appointments.length > 0) {
          this.appointments.push.apply(this.appointments, res.appointments);
        }
        infiniteScroll.complete();
      }
    });
  }

  async doRefresh(refresher) {
    await this.initData();
    refresher.complete();
  }

}
