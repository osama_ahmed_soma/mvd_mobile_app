import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HealthComponent } from './health';
import { ComponentsModule } from '../components.module';

@NgModule({
  declarations: [
    HealthComponent,
  ],
  imports: [
    IonicPageModule.forChild(HealthComponent),
    ComponentsModule
  ],
  exports: [
    HealthComponent
  ]
})
export class HealthComponentModule {}
