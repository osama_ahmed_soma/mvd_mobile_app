import {Component} from '@angular/core';
import {
  IonicPage,
  NavParams,
  ToastController,
  AlertController,
  ModalController,
  LoadingController
} from 'ionic-angular';
import {isUndefined} from "util";

// Providers
import {PatientProvider} from '../../providers/patient/patient';
import {HealthProvider} from '../../providers/health/health';
import {AuthProvider} from '../../providers/auth/auth';


/**
 * Generated class for the HealthComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'health',
  templateUrl: 'health.html'
})
export class HealthComponent {

  tokenAndUserID: any = '';

  token: string;
  is_health: boolean = false;
  loading: any;
  healthData: any = [];
  editedIndex: number;

  constructor(private navParams: NavParams,
              private patientService: PatientProvider,
              private healthService: HealthProvider,
              private toastCtrl: ToastController,
              private alertCtrl: AlertController,
              private modalCtrl: ModalController,
              private loadingCtrl: LoadingController,
              private authService: AuthProvider) {
  }

  ionViewCanEnter() {
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if (typeof tokenAndUserID == 'object') {
        this.tokenAndUserID = tokenAndUserID;

        this.loading = this.navParams.get('loading');
        let isHealth = this.navParams.get('is_health');
        this.token = this.tokenAndUserID.token;
        if (isUndefined(this.loading) && isUndefined(isHealth)) {
          if (isUndefined(this.loading)) {
            this.loading = this.loadingCtrl.create({
              spinner: 'crescent'
            });
            this.loading.present();
          }
          this.patientService.getMyHealthData(this.token).subscribe(response => {
            const res: any = response;
            isHealth = res.message.patient.is_health;
            this.initialize(isHealth);
            resolve();
          });
        } else {
          this.initialize(isHealth);
          resolve();
        }
      }
    });
  }

  private initialize(isHealth) {
    if (isHealth == 2) {
      this.is_health = true;
    } else if (isHealth == 1) {
      this.is_health = false;
    } else {
      this.is_health = null;
    }
    // this.is_health = (isHealth == 2) ? true : false;
    this.healthService.get(this.token).subscribe(response => {
      // if (!res.bool) {
      //   let toast = this.toastCtrl.create({
      //     message: res.message,
      //     duration: 3000,
      //     position: 'top',
      //     cssClass: 'error'
      //   });
      //   toast.present();
      // } else {
      //   this.healthData = res.message;
      // }
      const res: any = response;
      if(res.bool){
        this.healthData = res.message;
      }
      this.loading.dismiss();
    });
  }

  changeIsHealth(isHealth) {
    this.is_health = isHealth;
    this.patientService.updateIsHealth(this.token, this.is_health).subscribe();

  }

  openAdd() {
    let modal = this.modalCtrl.create('AddHealthComponent');
    modal.onDidDismiss(data => {
      if (typeof data !== 'undefined') {
        this.healthData.push(data);
      }
    });
    modal.present();
  }

  openEdit(index) {
    this.editedIndex = index;
    const condition = this.healthData[this.editedIndex];
    let modal = this.modalCtrl.create('EditHealthComponent', {
      condition: condition,
      index: this.editedIndex
    });
    modal.onDidDismiss(data => {
      this.healthData[data.index] = data.condition;
    });
    modal.present();
  }

  removeDialog(index) {
    let alert = this.alertCtrl.create({
      title: this.healthData[index].condition_term,
      message: 'Are you sure that you want to delete this condition?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.remove(index);
          }
        }
      ],
      enableBackdropDismiss: false
    });
    alert.present();
  }

  private remove(index) {
    this.healthService.remove(this.token, this.healthData[index].id).subscribe(response => {
      const res: any = response;
      let cssClass = 'error';
      if (!res.bool) {
        cssClass = 'error';
      } else {
        cssClass = 'success';
        this.healthData.splice(index, 1);
      }
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: cssClass
      });
      toast.present();
    });
  }

}
