import {Component} from '@angular/core';
import {
  IonicPage,
  NavParams,
  ViewController,
  ToastController,
  LoadingController,
  Events
} from 'ionic-angular';

// Provider
import {AuthProvider} from '../../providers/auth/auth';
import {AppointmentProvider} from '../../providers/appointment/appointment';

/**
 * Generated class for the WaitingRoomComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'waiting-room',
  templateUrl: 'waiting-room.html'
})
export class WaitingRoomComponent {

  appointmentTime: string = '00:00';
  canClose: boolean = true;
  appointment: any = '';
  isKnocking: boolean = false;
  isKnocked: boolean = false;

  appointmentID: any;

  constructor(private viewCtrl: ViewController,
              private events: Events,
              private navParams: NavParams,
              private authService: AuthProvider,
              private appointmentService: AppointmentProvider,
              private loadingCtrl: LoadingController,
              private toastCtrl: ToastController) {
    this.canClose = this.navParams.get('canClose');
    this.appointment = this.navParams.get('appointment');
    let interval;
    if (this.appointment) {
      this.appointmentID = this.appointment.id;
      let time = this.appointment.start.secondLeft;
      interval = setInterval(() => {
        const timeObj = this.secondsToHms(time);
        if (time >= 3600) {
          // hours with minutes and seconds
          this.appointmentTime = timeObj.hours + ':' + timeObj.minutes + ':' + timeObj.seconds;
        } else {
          // minutes with seconds
          this.appointmentTime = timeObj.minutes + ':' + timeObj.seconds;
          if (this.canClose) {
            if (time <= 30) {
              this.canClose = false;
            }
          }
          if (time == 0) {
            clearInterval(interval);
          }
        }
        time--;
      }, 1000);
    }
    this.events.subscribe('appointment:upcoming:show:time', data => {
      const time = data.time;
      this.appointmentID = data.appointmentID;
      if (this.canClose) {
        const newTime = time.split(":");
        const minutes = newTime[0];
        const seconds = (minutes * 60) + newTime[1];
        if (parseInt(seconds) <= 30) {
          this.canClose = false;
        }
      }
      this.appointmentTime = time;
      clearInterval(interval);
    });
    this.events.subscribe('appointment:upcoming:hide:time', () => {
      this.appointmentTime = '';
    });
    this.events.subscribe('appointment:upcoming:knocking:doctor:available', data => {
      if (!data) {
        // hide
        this.isKnocking = false;
      } else {
        // show
        if (data[0].is_patient_approved === 1) {
          // show knocked message
          this.isKnocking = false;
          this.isKnocked = true;
          // check doctor available
          this.events.publish('appointment:upcoming:check:doctor', this.appointmentID);
        } else {
          // show is knocking data
          if (!this.isKnocked) {
            this.isKnocking = true;
          }
        }
      }
    });
    this.events.subscribe('appointment:upcoming:hide:waiting:room', () => {
      this.dismiss();
    });
  }

  private secondsToHms(d) {
    d = Number(d);

    const h = Math.floor(d / 3600);
    const m = Math.floor(d % 3600 / 60);
    const s = Math.floor(d % 3600 % 60);
    return {
      hours: ('0' + h).slice(-2),
      minutes: ('0' + m).slice(-2),
      seconds: ('0' + s).slice(-2)
    };
  }

  async knockDoctor() {
    // http request and start waiting for doctor starts
    const tokenAndUserID: any = await this.authService.authenticate();
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.appointmentService.patientKnock(tokenAndUserID.token, this.appointmentID).subscribe(response => {
      const res: any = response;
      let cssClasses = 'error';
      if (res.bool) {
        cssClasses = 'success';
        this.isKnocked = true;
        this.isKnocking = false;
      }
      loading.dismiss();
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: cssClasses
      });
      toast.present();
    });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
