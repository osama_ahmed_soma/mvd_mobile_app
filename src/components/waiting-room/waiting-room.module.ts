import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WaitingRoomComponent } from './waiting-room';

@NgModule({
  declarations: [
    WaitingRoomComponent,
  ],
  imports: [
    IonicPageModule.forChild(WaitingRoomComponent),
  ],
  exports: [
    WaitingRoomComponent
  ]
})
export class WaitingRoomComponentModule {}
