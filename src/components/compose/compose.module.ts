import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Compose } from './compose';

// custom component
import {ComponentsModule} from '../components.module';

@NgModule({
  declarations: [
    Compose,
  ],
  imports: [
    IonicPageModule.forChild(Compose),
    ComponentsModule
  ],
  exports: [
    Compose
  ]
})
export class ComposeModule {}
