import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController, ToastController} from 'ionic-angular';

// Providers
import {PatientProvider} from '../../providers/patient/patient';
import {MessageProvider} from '../../providers/message/message';
import {AuthProvider} from '../../providers/auth/auth';


/**
 * Generated class for the Compose component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'compose',
  templateUrl: 'compose.html'
})
export class Compose {

  tokenAndUserID: any = '';

  doctors: Array<any> = [];
  isDoctorPass: boolean = false;
  isSubjectPass: boolean = false;
  title: string = 'COMPOSE';

  formData: {
    recipient: number,
    subject: string,
    message: string
  } = {
    recipient: 0,
    subject: '',
    message: ''
  };

  loading: any;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private loadingCtrl: LoadingController,
              private toastCtrl: ToastController,
              private patientService: PatientProvider,
              private messageService: MessageProvider,
              private authService: AuthProvider) {
  }

  ionViewCanEnter() {
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if (typeof tokenAndUserID == 'object') {
        this.tokenAndUserID = tokenAndUserID;
        const doctor = this.navParams.get('doctor');
        let subject = this.navParams.get('subject');
        if (doctor) {
          this.doctors = [
            doctor
          ];
          if (subject) {
            if (subject.indexOf('Re:') === -1) {
              subject = 'Re: '+subject.trim();
            }
            this.formData.subject = subject;
            this.isSubjectPass = true;
            this.title = 'REPLY';
          }
          this.isDoctorPass = true;
        } else {
          this.loading = this.loadingCtrl.create({
            spinner: 'crescent'
          });
          this.loading.present();
        }
        resolve();
      }
    });
  }

  ionViewDidEnter() {
    if (!this.isDoctorPass) {
      this.patientService.getMyPhysicians(this.tokenAndUserID.token).subscribe(response => {
        const res: any = response;
        if (!res.bool) {
          let toast = this.toastCtrl.create({
            message: res.message,
            duration: 3000,
            position: 'top',
            cssClass: 'error'
          });
          toast.present();
          this.loading.dismiss();
          this.navCtrl.pop();
          return;
        }
        this.doctors = res.myDoctors;
        if (this.doctors.length <= 0) {
          let toast = this.toastCtrl.create({
            message: 'You have no physician.',
            duration: 3000,
            position: 'top',
            cssClass: 'error'
          });
          toast.present();
          this.loading.dismiss();
          this.navCtrl.pop();
          return;
        }
        this.loading.dismiss();
      });
    }
    // const doctor = this.navParams.get('doctor');
    // const subject = this.navParams.get('subject');
    // if (doctor) {
    //   this.doctors = [
    //     doctor
    //   ];
    //   if(subject){
    //     this.formData.subject = subject;
    //   }
    // } else {
    //   let loading = this.loadingCtrl.create({
    //     spinner: 'crescent'
    //   });
    //   loading.present();
    //   this.patientService.getMyPhysicians(this.tokenAndUserID.token).subscribe(res => {
    //     if (!res.bool) {
    //       let toast = this.toastCtrl.create({
    //         message: res.message,
    //         duration: 3000,
    //         position: 'top',
    //         cssClass: 'error'
    //       });
    //       toast.present();
    //       loading.dismiss();
    //       this.navCtrl.pop();
    //       return;
    //     }
    //     this.doctors = res.myDoctors;
    //     if (this.doctors.length <= 0) {
    //       let toast = this.toastCtrl.create({
    //         message: 'You have no physician.',
    //         duration: 3000,
    //         position: 'top',
    //         cssClass: 'error'
    //       });
    //       toast.present();
    //       loading.dismiss();
    //       this.navCtrl.pop();
    //       return;
    //     }
    //     loading.dismiss();
    //   });
    // }
  }

  async sendMessage() {
    if (!this.doctors[this.formData.recipient].userid) {
      let toast = this.toastCtrl.create({
        message: 'Please select a recipient',
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
      return;
    }
    if (this.formData.subject.trim() == '') {
      let toast = this.toastCtrl.create({
        message: 'You must provide a Subject.',
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
      return;
    }
    if (this.formData.message.trim() == '') {
      let toast = this.toastCtrl.create({
        message: 'The Message field is required.',
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
      return;
    }
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.messageService.create(this.tokenAndUserID.token, {
      recipientID: this.doctors[this.formData.recipient].userid,
      subject: this.formData.subject.trim(),
      message: this.formData.message.trim()
    }).subscribe(response => {
      const res: any = response;
      loading.dismiss();
      let cssClass = 'error';
      if (res.bool) {
        cssClass = 'success';
      }
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: cssClass
      });
      toast.present();
      if (res.bool) {
        this.navCtrl.pop();
      }
    });
  }

}
