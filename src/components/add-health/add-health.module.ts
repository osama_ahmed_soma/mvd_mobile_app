import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddHealthComponent } from './add-health';

@NgModule({
  declarations: [
    AddHealthComponent,
  ],
  imports: [
    IonicPageModule.forChild(AddHealthComponent),
  ],
  exports: [
    AddHealthComponent
  ]
})
export class AddHealthComponentModule {}
