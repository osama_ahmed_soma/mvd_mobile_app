import {Component} from '@angular/core';
import {IonicPage, ViewController, ToastController, LoadingController} from 'ionic-angular';

// Providers
import {HealthProvider} from '../../providers/health/health';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the AddHealthComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'add-health',
  templateUrl: 'add-health.html'
})
export class AddHealthComponent {

  tokenAndUserID: any = '';

  condition_term: string = '';

  constructor(private viewCtrl: ViewController,
              private toastCtrl: ToastController,
              private loadingCtrl: LoadingController,
              private healthService: HealthProvider,
              private authService: AuthProvider) {
  }

  ionViewCanEnter(){
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if(typeof tokenAndUserID == 'object'){
        this.tokenAndUserID = tokenAndUserID;
        resolve();
      }
    });
  }

  async addCondition() {
    if (this.condition_term.trim() == '') {
      let toast = this.toastCtrl.create({
        message: 'You have to fill all fields',
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
      return;
    }
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.healthService.insert(this.tokenAndUserID.token, {
      condition: this.condition_term.trim(),
      source: 'Self Reported'
    }).subscribe(response => {
      const res: any = response;
      loading.dismiss();
      let cssClass = 'error';
      if (res.bool) {
        cssClass = 'success';
      }
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: cssClass
      });
      toast.present();
      if (res.bool) {
        this.dismiss(res.insertedData);
      }
    });
  }

  dismiss(object?) {
    this.viewCtrl.dismiss(object);
  }

}
