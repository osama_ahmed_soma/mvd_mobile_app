import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { MvdTopheader } from './mvd-topheader/mvd-topheader';
import { DoctorListComponent } from './doctor-list/doctor-list';

import { TooltipsModule } from 'ionic-tooltips';


@NgModule({
  declarations: [
    MvdTopheader,
    DoctorListComponent,
  ],
  imports: [
    IonicModule,
    TooltipsModule
  ],
  exports: [
    MvdTopheader,
    DoctorListComponent,
  ]
})
export class ComponentsModule {}
