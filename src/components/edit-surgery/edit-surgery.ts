import {Component} from '@angular/core';
import {IonicPage, NavParams, ViewController, ToastController} from 'ionic-angular';

// Providers
import {SurgeryProvider} from '../../providers/surgery/surgery';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the EditSurgeryComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'edit-surgery',
  templateUrl: 'edit-surgery.html'
})
export class EditSurgeryComponent {

  tokenAndUserID: any = '';

  surgery: any;
  condition_term: string;
  source: number = 0;
  index: number;

  years: Array<number> = [];

  constructor(private navParams: NavParams,
              private viewCtrl: ViewController,
              private surgeryService: SurgeryProvider,
              private toastCtrl: ToastController,
              private authService: AuthProvider) {
  }

  ionViewCanEnter(){
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if(typeof tokenAndUserID == 'object'){
        this.tokenAndUserID = tokenAndUserID;

        this.index = this.navParams.get('index');
        this.surgery = this.navParams.get('surgery');
        this.condition_term = this.surgery.condition_term;
        this.source = this.surgery.source;
        let firstYear: number = new Date().getFullYear() - 100;
        let lastYear: number = firstYear + 100;
        for (let i = firstYear; i <= lastYear; i++) {
          this.years.push(i);
        }

        resolve();
      }
    });
  }

  async changeSurgery() {
    if (this.condition_term.trim() == '' || this.source <= 0) {
      let toast = this.toastCtrl.create({
        message: 'You have to fill all fields',
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
      return;
    }
    this.surgery.condition_term = this.condition_term;
    this.surgery.source = this.source;
    this.surgeryService.update(this.tokenAndUserID.token, this.surgery).subscribe(response => {
      const res: any = response;
      let cssClass = 'error';
      if (!res.bool) {
        cssClass = 'error';
      } else {
        cssClass = 'success';
      }
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: cssClass
      });
      toast.present();
      this.dismiss();
    });
  }

  dismiss() {
    this.viewCtrl.dismiss({
      surgery: this.surgery,
      index: this.index
    });
  }

}
