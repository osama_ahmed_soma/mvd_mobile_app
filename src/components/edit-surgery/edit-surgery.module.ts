import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditSurgeryComponent } from './edit-surgery';

@NgModule({
  declarations: [
    EditSurgeryComponent,
  ],
  imports: [
    IonicPageModule.forChild(EditSurgeryComponent),
  ],
  exports: [
    EditSurgeryComponent
  ]
})
export class EditSurgeryComponentModule {}
