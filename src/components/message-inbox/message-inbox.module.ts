import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {MessageInboxComponent} from './message-inbox';

// custom component
import {ComponentsModule} from '../components.module';

@NgModule({
  declarations: [
    MessageInboxComponent,
  ],
  imports: [
    IonicPageModule.forChild(MessageInboxComponent),
    ComponentsModule
  ],
  exports: [
    MessageInboxComponent
  ]
})
export class MessageInboxComponentModule {
}
