import {Component} from '@angular/core';
import {
  IonicPage,
  NavController,
  LoadingController,
  ToastController,
  AlertController,
  FabContainer
} from 'ionic-angular';

// Providers
import {MessageProvider} from '../../providers/message/message';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the MessageInboxComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'message-inbox',
  templateUrl: 'message-inbox.html'
})
export class MessageInboxComponent {

  tokenAndUserID: any = '';

  messages: Array<any> = [];

  constructor(private navCtrl: NavController,
              private loadingCtrl: LoadingController,
              private messageService: MessageProvider,
              private toastCtrl: ToastController,
              private alertCtrl: AlertController,
              private authService: AuthProvider) {
    // let loading = loadingCtrl.create({
    //   spinner: 'crescent'
    // });
    // loading.present();
    // this.doRefresh({
    //   complete: () => {
    //     loading.dismiss();
    //   }
    // });
  }

  ionViewCanEnter(){
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if(typeof tokenAndUserID == 'object'){
        this.tokenAndUserID = tokenAndUserID;

        let loading = this.loadingCtrl.create({
          spinner: 'crescent'
        });
        loading.present();
        this.doRefresh({
          complete: () => {
            loading.dismiss();
            resolve();
          }
        });
      }
    });
  }

  openMessage(index) {
    this.navCtrl.push('MessageView', {
      messageID: this.messages[index].id,
      type: 'inbox'
    });
  }

  openCompose(fab: FabContainer) {
    fab.close();
    this.navCtrl.push('Compose');
  }

  openSearch(fab: FabContainer) {
    let alert = this.alertCtrl.create({
      title: 'Search',
      inputs: [
        {
          type: 'text',
          name: 'search',
          placeholder: 'Search'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {

          }
        },
        {
          text: 'Search',
          handler: data => {
            if (data.search != '') {
              fab.close();
              this.navCtrl.push('MessageSearchComponent', {
                search: data.search,
                type: 'inbox'
              });
            }
          }
        }
      ],
      enableBackdropDismiss: false
    });
    alert.present();
  }

  removeDialog(index) {
    let alert = this.alertCtrl.create({
      title: this.messages[index].firstname + ' ' + this.messages[index].lastname,
      message: 'Are you sure that you want to DELETE ' + this.messages[index].subject,
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.remove(index);
          }
        }
      ],
      enableBackdropDismiss: false
    });
    alert.present();
  }

  async doRefresh(refresher) {
    await this.getMessages(this.tokenAndUserID.token);
    refresher.complete();
  }

  private getMessages(token) {
    return new Promise(resolve => {
      this.messageService.getInbox(token).subscribe(response => {
        const res: any = response;
        if (!res.bool) {
          let toast = this.toastCtrl.create({
            message: res.message,
            duration: 3000,
            position: 'top',
            cssClass: 'error'
          });
          toast.present();
          return;
        }
        this.messages = res.message.messages;
        resolve();
      });
    });
  }

  private async remove(index) {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.messageService.remove(this.tokenAndUserID.token, this.messages[index].id).subscribe(response => {
      const res: any = response;
      loading.dismiss();
      let cssClass = 'error';
      if (res.bool) {
        cssClass = 'success';
        this.messages.splice(index, 1);
      }
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: cssClass
      });
      toast.present();
    });

  }

}
