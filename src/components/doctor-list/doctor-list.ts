import {Component, Input} from '@angular/core';
import {NavController, ActionSheetController, ModalController, LoadingController, ToastController, Events} from 'ionic-angular';

// Providers
import {FavoriteProvider} from '../../providers/favorite/favorite';

/**
 * Generated class for the DoctorListComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'doctor-list',
  templateUrl: 'doctor-list.html'
})
export class DoctorListComponent {

  // tokenAndUserID: any = '';

  @Input() doctor;
  @Input() tokenAndUserID;

  constructor(private navCtrl: NavController,
              private actionSheetCtrl: ActionSheetController,
              private modalCtrl: ModalController,
              private loadingCtrl: LoadingController,
              private favoriteService: FavoriteProvider,
              private toastCtrl: ToastController,
              private events: Events) {
  }

  ionViewCanEnter(){
  }

  openDoctor() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.navCtrl.push('DoctorProfile', {
      doctor: this.doctor,
      loading: loading
    });
  }

  private async addPhysician() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.favoriteService.add(this.tokenAndUserID.token, this.doctor.userid).subscribe(response => {
      const res: any = response;
      loading.dismiss();
      let className: string = 'error';
      if (res.bool) {
        className = 'success';
        this.doctor.isFav = true;
      }
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: className
      });
      toast.present();
    });
  }

  private async removePhysician() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.favoriteService.remove(this.tokenAndUserID.token, this.doctor.userid).subscribe(response => {
      const res: any = response;
      loading.dismiss();
      let className: string = 'error';
      if (res.bool) {
        className = 'success';
        this.doctor.isFav = false;
        this.events.publish('doctorRemovedFromFav');
      }
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: className
      });
      toast.present();
    });
  }

  openActions() {
    let addOrRemove: {
      text: string,
      icon: string
    } = {
      text: 'Add to',
      icon: 'add'
    };
    if (this.doctor.isFav) {
      addOrRemove = {
        text: 'Remove from',
        icon: 'remove'
      };
    }
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Profile',
          icon: 'person',
          handler: () => {
            this.openDoctor();
          }
        },
        {
          text: 'Schedule',
          icon: 'time',
          handler: () => {
            let modal = this.modalCtrl.create('ScheduleAppointment', {
              doctor: this.doctor
            });
            modal.onDidDismiss((isRedirect: boolean = false) => {
              if(isRedirect){
                this.navCtrl.push('Appointment');
              }
            });
            modal.present();
          }
        },
        {
          text: 'Message',
          icon: 'mail',
          handler: () => {
            if(!this.doctor.isFav){
              let toast = this.toastCtrl.create({
                message: 'You cannot message physician unless they are added as a favorite. Please search and add favorites',
                duration: 3000,
                position: 'top',
                cssClass: 'error'
              });
              toast.present();
              return;
            }
            this.navCtrl.push('Compose', {
              doctor: this.doctor
            });
          }
        },
        {
          text: addOrRemove.text + ' Favorites',
          icon: addOrRemove.icon,
          handler: () => {
            if (addOrRemove.icon == 'add') {
              this.addPhysician();
            } else {
              this.removePhysician();
            }
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {

          }
        }
      ]
    });

    actionSheet.present();
  }

}
