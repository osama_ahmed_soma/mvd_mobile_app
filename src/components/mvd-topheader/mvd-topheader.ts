import {Component, Input} from '@angular/core';
import {NavController, PopoverController, Events, ModalController, ToastController} from 'ionic-angular';
import {Searchform} from '../searchform/searchform';
import {NotificationComponent} from '../notification/notification';
import {DefaultOptionsComponent} from '../default-options/default-options';

/**
 * Generated class for the MvdTopheader component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'mvd-topheader',
  templateUrl: 'mvd-topheader.html'
})
export class MvdTopheader {

  @Input() data: any;

  @Input() title: string;
  @Input() isAppointment: boolean = false;
  @Input() isMyFiles: boolean = false;
  @Input() isMyFilesShare: boolean = false;
  @Input() isSearch: boolean = false;
  @Input() search_state: string = 'None';
  @Input() search_specialty: string = 'None';
  @Input() search_query: string = 'None';
  @Input() tabTitle: string = '';
  @Input() profileNested: boolean = true;
  @Input() healthNested: boolean = true;
  @Input() medicationNested: boolean = true;
  @Input() allergiesNested: boolean = true;
  @Input() surgeriesNested: boolean = true;
  @Input() messageNested: boolean = true;
  @Input() isSettings: boolean = true;

  messageCount: number;
  notificationCount: number;
  notification: Array<any>;
  states: Array<{ id: number, code: string, name: string }>;
  specialties: Array<{ id: number, name: string }>;
  profileImage: string;
  profileCompletion: {
    percentage: string,
    text: string,
    type: string
  } = {
    percentage: '0%',
    text: '',
    type: ''
  };

  popover: any;

  profileCompletionLinks: any = {
    'Profile': 'this.openProfile()',
    'Health': 'this.openMyHealthHealth()',
    'Medication': 'this.openMyHealthMedication()',
    'Allergies': 'this.openMyHealthAllergies()',
    'Surgeries': 'this.openMyHealthSurgeries()',
    'Complete': 'this.openProfile()'
  };

  appointmentTime: string = '';

  appointmentID: any;

  isTooltipActive: boolean = false;
  localIsTootTipShow: boolean = false;

  isSearchBarAnimate: boolean = false;

  constructor(private navCtrl: NavController,
              private popoverCtrl: PopoverController,
              private events: Events,
              private modalCtrl: ModalController,
              private toastCtrl: ToastController) {

    events.subscribe('notification:cleared:all', () => {
      this.data.notifications.unread.count = 0;
    });
    events.subscribe('notification:clear:one', () => {
      this.data.notifications.unread.count -= 1;
    });
    events.subscribe('appointment:upcoming:show:time', data => {
      this.appointmentTime = data.time;
      this.appointmentID = data.appointmentID;
    });
    events.subscribe('counter:toolTip:show', () => {
      // this.isTooltipActive = true;
      setTimeout(() => {
        this.isTooltipActive = true;
        setTimeout(() => {
          this.isTooltipActive = false;
        }, 5000);
      }, 3000);
    });
    events.subscribe('appointment:upcoming:hide:time', () => {
      this.appointmentTime = '';
      this.isTooltipActive = false;
    });

  }

  presentPopover(myEvent) {
    this.popover = this.popoverCtrl.create(Searchform, {
      states: this.data.states,
      specialties: this.data.specialties
    });
    this.popover.present({
      ev: myEvent
    });
    this.popover.onDidDismiss(data => {
      if (data) {
        data.instances.loading.dismiss();
        delete data.instances;
        if (data.doctors.length > 0) {
          this.navCtrl.push('SearchResult', data);
        } else {
          let toast = this.toastCtrl.create({
            message: 'No Doctor Found.',
            duration: 3000,
            position: 'top',
            cssClass: 'error'
          });
          toast.present();
        }
      }
    });
  }

  openNotification(event) {
    this.popover = this.popoverCtrl.create(NotificationComponent, {
      // notifications: this.notification
      notifications: this.data.notifications.unread.data
    });
    this.popover.present({
      ev: event
    });
  }

  openOptions(event) {
    let popover = this.popoverCtrl.create(DefaultOptionsComponent, {
      isSettings: this.isSettings
    });
    popover.present({
      ev: event
    });
    popover.onDidDismiss(data => {
      if(data) {
        switch (data.type) {
          case 'settings':
            this.openSettings();
            break;
          case 'appointment':
            this.openScheduleAppointment();
            break;
        }
      }
    });
  }

  openSettings() {
    this.navCtrl.setRoot('SettingsPage');
  }

  openScheduleAppointment(){
    let modal = this.modalCtrl.create('ScheduleAppointment', {
      doctor: null
    });
    modal.onDidDismiss(() => {

    });
    modal.present();
  }

  openWaitingRoom() {
    this.events.publish('waiting:room:open', true);
  }

  openProfile() {
    if (this.profileNested) {
      this.navCtrl.push('Myprofile');
    }
  }

  openMessage() {
    if (this.messageNested) {
      this.navCtrl.push('Message');
    }
  }

  openMyHealthHealth() {
    if (this.healthNested) {
      this.navCtrl.push('Myhealth', {
        index: 0
      });
    }
  }

  openMyHealthMedication() {
    if (this.medicationNested) {
      this.navCtrl.push('Myhealth', {
        index: 1
      });
    }
  }

  openMyHealthAllergies() {
    if (this.allergiesNested) {
      this.navCtrl.push('Myhealth', {
        index: 2
      });
    }
  }

  openMyHealthSurgeries() {
    if (this.surgeriesNested) {
      this.navCtrl.push('Myhealth', {
        index: 3
      });
    }
  }

  openProfileCompletionLinks() {
    eval(this.profileCompletionLinks[this.data.profileCompletionData.type]);
  }

}
