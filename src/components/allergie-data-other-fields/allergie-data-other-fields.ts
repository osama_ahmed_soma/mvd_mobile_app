import {Component} from '@angular/core';
import {IonicPage, NavParams, ViewController, ToastController} from 'ionic-angular';

/**
 * Generated class for the AllergieDataOtherFieldsComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'allergie-data-other-fields',
  templateUrl: 'allergie-data-other-fields.html'
})
export class AllergieDataOtherFieldsComponent {

  allergy: {
    name?: string,
    symptom: string,
    reaction: string
  } = {
    name: '',
    symptom: 'Severity',
    reaction: ''
  };
  isCancelled: boolean = true;
  isOther: boolean = false;

  constructor(private navParams: NavParams,
              private viewCtrl: ViewController,
              private toastCtrl: ToastController,) {
    this.allergy = this.navParams.get('allergy');
    this.isOther = this.navParams.get('isOther');
  }

  addData() {
    if (this.isOther) {
      return this.addDataForOther();
    }
    if (this.allergy.symptom == '' || this.allergy.reaction == '') {
      let toast = this.toastCtrl.create({
        message: 'All fields are required.',
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
      return;
    }
    this.isCancelled = false;
    this.dismiss();
  }

  private addDataForOther() {
    if (this.allergy.name == '' || this.allergy.symptom == '' || this.allergy.reaction == '') {
      let toast = this.toastCtrl.create({
        message: 'All fields are required.',
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
      return;
    }
    this.isCancelled = false;
    this.dismiss();
  }

  dismiss() {
    this.viewCtrl.dismiss({
      allergiesData: this.allergy,
      isCancelled: this.isCancelled
    });
  }

}
