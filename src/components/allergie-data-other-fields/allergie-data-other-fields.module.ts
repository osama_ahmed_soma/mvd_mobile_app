import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AllergieDataOtherFieldsComponent } from './allergie-data-other-fields';

@NgModule({
  declarations: [
    AllergieDataOtherFieldsComponent,
  ],
  imports: [
    IonicPageModule.forChild(AllergieDataOtherFieldsComponent),
  ],
  exports: [
    AllergieDataOtherFieldsComponent
  ]
})
export class AllergieDataOtherFieldsComponentModule {}
