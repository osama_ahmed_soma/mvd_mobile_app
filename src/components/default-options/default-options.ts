import { Component } from '@angular/core';
import {ViewController, NavParams} from 'ionic-angular';

/**
 * Generated class for the DefaultOptionsComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'default-options',
  templateUrl: 'default-options.html'
})
export class DefaultOptionsComponent {

  isSettings: boolean;

  constructor(private viewCtrl: ViewController, private navParams: NavParams) {
    this.isSettings = this.navParams.get('isSettings');
  }

  close(type: string = '') {
    this.viewCtrl.dismiss({
      type: type
    });
  }

}
