import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FaqDetail } from './faq-detail';

@NgModule({
  declarations: [
    FaqDetail,
  ],
  imports: [
    IonicPageModule.forChild(FaqDetail),
  ],
  exports: [
    FaqDetail
  ]
})
export class FaqDetailModule {}
