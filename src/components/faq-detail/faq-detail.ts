import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

/**
 * Generated class for the FaqDetail component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'faq-detail',
  templateUrl: 'faq-detail.html'
})
export class FaqDetail {

  text: string;

  constructor() {
    console.log('Hello FaqDetail Component');
    this.text = 'Hello World';
  }

}
