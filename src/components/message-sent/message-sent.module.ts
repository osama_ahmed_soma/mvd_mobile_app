import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MessageSentComponent } from './message-sent';

// custom component
import {ComponentsModule} from '../components.module';

@NgModule({
  declarations: [
    MessageSentComponent,
  ],
  imports: [
    IonicPageModule.forChild(MessageSentComponent),
    ComponentsModule
  ],
  exports: [
    MessageSentComponent
  ]
})
export class MessageSentComponentModule {}
