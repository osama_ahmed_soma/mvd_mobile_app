import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MessageTrashComponent } from './message-trash';

// custom component
import {ComponentsModule} from '../components.module';

@NgModule({
  declarations: [
    MessageTrashComponent,
  ],
  imports: [
    IonicPageModule.forChild(MessageTrashComponent),
    ComponentsModule
  ],
  exports: [
    MessageTrashComponent
  ]
})
export class MessageTrashComponentModule {}
