import {Component} from '@angular/core';
import {IonicPage, NavParams, ViewController, ToastController} from 'ionic-angular';

/**
 * Generated class for the SurgeriesDataOtherFieldsComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'surgeries-data-other-fields',
  templateUrl: 'surgeries-data-other-fields.html'
})
export class SurgeriesDataOtherFieldsComponent {

  surgery: {
    name?: string,
    year?: number
  };
  isCancelled: boolean = true;
  isOther: boolean = false;

  years: Array<number> = [];

  constructor(private navParams: NavParams,
              private viewCtrl: ViewController,
              private toastCtrl: ToastController,) {
    this.generateYears();
    this.surgery = this.navParams.get('surgery');
    if (!this.surgery.hasOwnProperty('year')) {
      this.surgery.year = new Date().getFullYear();
    }
    this.isOther = this.navParams.get('isOther');
  }

  private generateYears() {
    const currentYear: number = new Date().getFullYear();
    let firstYear: number = currentYear - 100;
    let lastYear: number = firstYear + 100;
    for (let i = firstYear; i <= lastYear; i++) {
      this.years.push(i);
    }
  }

  addData() {
    if (this.isOther) {
      return this.addDataForOther();
    }
    if (!this.surgery.year) {
      let toast = this.toastCtrl.create({
        message: 'All fields are required.',
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
      return;
    }
    this.isCancelled = false;
    this.dismiss();
  }

  private addDataForOther() {
    if (this.surgery.name == '' || !this.surgery.year) {
      let toast = this.toastCtrl.create({
        message: 'All fields are required.',
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
      return;
    }
    this.isCancelled = false;
    this.dismiss();
  }

  dismiss() {
    this.viewCtrl.dismiss({
      surgeryData: this.surgery,
      isCancelled: this.isCancelled
    });
  }

}
