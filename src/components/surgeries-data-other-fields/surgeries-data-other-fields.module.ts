import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SurgeriesDataOtherFieldsComponent } from './surgeries-data-other-fields';

@NgModule({
  declarations: [
    SurgeriesDataOtherFieldsComponent,
  ],
  imports: [
    IonicPageModule.forChild(SurgeriesDataOtherFieldsComponent),
  ],
  exports: [
    SurgeriesDataOtherFieldsComponent
  ]
})
export class SurgeriesDataOtherFieldsComponentModule {}
