import {Component} from '@angular/core';
import {
  IonicPage,
  NavParams,
  ToastController,
  AlertController,
  ModalController,
  LoadingController
} from 'ionic-angular';
import {isUndefined} from "util";

// Providers
import {PatientProvider} from '../../providers/patient/patient';
import {MedicationProvider} from '../../providers/medication/medication';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the MedicationsComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'medications',
  templateUrl: 'medications.html'
})
export class MedicationsComponent {

  tokenAndUserID: any = '';

  token: string;
  is_medication: boolean = false;
  loading: any;
  medicationData: any = [];
  editedIndex: number;

  constructor(private navParams: NavParams,
              private patientService: PatientProvider,
              private medicationService: MedicationProvider,
              private toastCtrl: ToastController,
              private alertCtrl: AlertController,
              private modalCtrl: ModalController,
              private loadingCtrl: LoadingController,
              private authService: AuthProvider) {
  }

  ionViewCanEnter() {
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if (typeof tokenAndUserID == 'object') {
        this.tokenAndUserID = tokenAndUserID;

        this.loading = this.navParams.get('loading');
        let isMedication = this.navParams.get('is_medication');
        this.token = this.tokenAndUserID.token;
        if (isUndefined(this.loading) && isUndefined(isMedication)) {
          if (isUndefined(this.loading)) {
            this.loading = this.loadingCtrl.create({
              spinner: 'crescent'
            });
            this.loading.present();
          }
          this.patientService.getMyHealthData(this.token).subscribe(response => {
            const res: any = response;
            isMedication = res.message.patient.is_medication;
            this.initialize(isMedication);
            resolve();
          });
        } else {
          this.initialize(isMedication);
          resolve();
        }
      }
    });
  }

  private initialize(isMedication) {
    // this.is_medication = (isMedication == 2) ? true : false;
    if (isMedication == 2) {
      this.is_medication = true;
    } else if (isMedication == 1) {
      this.is_medication = false;
    } else {
      this.is_medication = null;
    }
    this.medicationService.get(this.token).subscribe(response => {
      const res: any = response;
      if (!res.bool) {
        let toast = this.toastCtrl.create({
          message: res.message,
          duration: 3000,
          position: 'top',
          cssClass: 'error'
        });
        toast.present();
      } else {
        this.medicationData = res.message;
      }
      this.loading.dismiss();
    });
  }

  changeIsMedication(isMedication) {
    this.is_medication = isMedication;
    this.patientService.updateIsMedication(this.token, this.is_medication).subscribe();
  }

  openAdd() {
    let modal = this.modalCtrl.create('AddMedicationComponent');
    modal.onDidDismiss(data => {
      if (typeof data !== 'undefined') {
        this.medicationData.push(data);
      }
    });
    modal.present();
  }

  openEdit(index) {
    this.editedIndex = index;
    const medication = this.medicationData[this.editedIndex];
    let modal = this.modalCtrl.create('EditMedicationComponent', {
      medication: medication,
      index: this.editedIndex
    });
    modal.onDidDismiss(data => {
      this.medicationData[data.index] = data.medication;
    });
    modal.present();
  }

  removeDialog(index) {
    let alert = this.alertCtrl.create({
      title: this.medicationData[index].condition_term,
      message: 'Are you sure that you want to delete this medication?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.remove(index);
          }
        }
      ],
      enableBackdropDismiss: false
    });
    alert.present();
  }

  private remove(index) {
    this.medicationService.remove(this.token, this.medicationData[index].id).subscribe(response => {
      const res: any = response;
      let cssClass = 'error';
      if (!res.bool) {
        cssClass = 'error';
      } else {
        cssClass = 'success';
        this.medicationData.splice(index, 1);
      }
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: cssClass
      });
      toast.present();
    });
  }

}
