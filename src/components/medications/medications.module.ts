import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MedicationsComponent } from './medications';
import { ComponentsModule } from '../components.module';

@NgModule({
  declarations: [
    MedicationsComponent,
  ],
  imports: [
    IonicPageModule.forChild(MedicationsComponent),
    ComponentsModule
  ],
  exports: [
    MedicationsComponent
  ]
})
export class MedicationsComponentModule {}
