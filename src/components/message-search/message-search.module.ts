import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {MessageSearchComponent} from './message-search';

// custom component
import {ComponentsModule} from '../components.module';

@NgModule({
  declarations: [
    MessageSearchComponent,
  ],
  imports: [
    IonicPageModule.forChild(MessageSearchComponent),
    ComponentsModule
  ],
  exports: [
    MessageSearchComponent
  ]
})
export class MessageSearchComponentModule {
}
