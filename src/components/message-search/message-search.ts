import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController, ToastController, AlertController} from 'ionic-angular';

// Providers
import {MessageProvider} from '../../providers/message/message';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the MessageSearchComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'message-search',
  templateUrl: 'message-search.html'
})
export class MessageSearchComponent {

  tokenAndUserID: any = '';

  messages: Array<any> = [];

  search: string;
  type: string;

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private loadingCtrl: LoadingController,
              private messageService: MessageProvider,
              private toastCtrl: ToastController,
              private alertCtrl: AlertController,
              private authService: AuthProvider) {
    // this.search = navParams.get('search');
    // this.type = navParams.get('type');
    // let loading = loadingCtrl.create({
    //   spinner: 'crescent'
    // });
    // loading.present();
    // this.doRefresh({
    //   complete: () => {
    //     loading.dismiss();
    //   }
    // });
  }

  ionViewCanEnter(){
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if(typeof tokenAndUserID == 'object'){
        this.tokenAndUserID = tokenAndUserID;

        this.search = this.navParams.get('search');
        this.type = this.navParams.get('type');
        let loading = this.loadingCtrl.create({
          spinner: 'crescent'
        });
        loading.present();
        this.doRefresh({
          complete: () => {
            loading.dismiss();
            resolve();
          }
        });
      }
    });
  }

  openMessage(index) {
    this.navCtrl.push('MessageView', {
      messageID: this.messages[index].id,
      type: this.type
    });
  }

  openCompose() {
    this.navCtrl.push('Compose');
  }

  openSearch() {
    let alert = this.alertCtrl.create({
      title: 'Search',
      inputs: [
        {
          type: 'text',
          name: 'search',
          placeholder: 'Search',
          value: this.search
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {

          }
        },
        {
          text: 'Search',
          handler: data => {
            if (data.search != '') {
              this.search = data.search;
              let loading = this.loadingCtrl.create({
                spinner: 'crescent'
              });
              loading.present();
              this.doRefresh({
                complete: () => {
                  loading.dismiss();
                }
              });
            }
          }
        }
      ],
      enableBackdropDismiss: false
    });
    alert.present();
  }

  removeDialog(index) {
    let alert = this.alertCtrl.create({
      title: this.messages[index].firstname + ' ' + this.messages[index].lastname,
      message: 'Are you sure that you want to DELETE ' + this.messages[index].subject,
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.remove(index);
          }
        }
      ],
      enableBackdropDismiss: false
    });
    alert.present();
  }

  async doRefresh(refresher) {
    await this.getMessages(this.tokenAndUserID.token);
    refresher.complete();
  }

  private getMessages(token) {
    return new Promise(resolve => {
      this.messageService.search(token, {
        search: this.search,
        type: this.type
      }).subscribe(response => {
        const res: any = response;
        if (!res.bool) {
          let toast = this.toastCtrl.create({
            message: res.message,
            duration: 3000,
            position: 'top',
            cssClass: 'error'
          });
          toast.present();
          this.navCtrl.pop();
          return;
        }
        this.messages = res.message.messages;
        resolve();
      });
    });
  }

  private async remove(index) {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent'
    });
    loading.present();
    this.messageService.remove(this.tokenAndUserID.token, this.messages[index].id).subscribe(response => {
      const res: any = response;
      loading.dismiss();
      let cssClass = 'error';
      if (res.bool) {
        cssClass = 'success';
        this.messages.splice(index, 1);
      }
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: cssClass
      });
      toast.present();
    });
  }

}
