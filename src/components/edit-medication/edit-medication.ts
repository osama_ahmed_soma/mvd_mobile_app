import {Component} from '@angular/core';
import {IonicPage, NavParams, ViewController, ToastController} from 'ionic-angular';

// Providers
import {MedicationProvider} from '../../providers/medication/medication';
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the EditMedicationComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'edit-medication',
  templateUrl: 'edit-medication.html'
})
export class EditMedicationComponent {

  tokenAndUserID: any = '';

  medication: any;
  condition_term: string;
  dosage: string;
  frequency: string;
  time: string;
  index: number;

  constructor(private navParams: NavParams,
              private viewCtrl: ViewController,
              private medicationService: MedicationProvider,
              private toastCtrl: ToastController,
              private authService: AuthProvider) {
  }

  ionViewCanEnter(){
    return new Promise(async (resolve) => {
      const tokenAndUserID = await this.authService.authenticate();
      if(typeof tokenAndUserID == 'object'){
        this.tokenAndUserID = tokenAndUserID;

        this.index = this.navParams.get('index');
        this.medication = this.navParams.get('medication');
        this.condition_term = this.medication.condition_term;
        this.dosage = this.medication.dosage;
        this.frequency = this.medication.frequency;
        this.time = this.medication.time;

        resolve();
      }
    });
  }

  async changeMedication() {
    if (this.condition_term.trim() == '' || this.dosage.trim() == '' || this.frequency.trim() == '' || this.time.trim() == '') {
      let toast = this.toastCtrl.create({
        message: 'You have to fill all fields',
        duration: 3000,
        position: 'top',
        cssClass: 'error'
      });
      toast.present();
      return;
    }
    this.medication.condition_term = this.condition_term;
    this.medication.dosage = this.dosage;
    this.medication.frequency = this.frequency;
    this.medication.time = this.time;
    this.medicationService.update(this.tokenAndUserID.token, this.medication).subscribe(response => {
      const res: any = response;
      let cssClass = 'error';
      if (!res.bool) {
        cssClass = 'error';
      } else {
        cssClass = 'success';
      }
      let toast = this.toastCtrl.create({
        message: res.message,
        duration: 3000,
        position: 'top',
        cssClass: cssClass
      });
      toast.present();
      this.dismiss();
    });
  }

  dismiss() {
    this.viewCtrl.dismiss({
      medication: this.medication,
      index: this.index
    });
  }

}
