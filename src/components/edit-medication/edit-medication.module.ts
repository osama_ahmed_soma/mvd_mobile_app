import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditMedicationComponent } from './edit-medication';

@NgModule({
  declarations: [
    EditMedicationComponent,
  ],
  imports: [
    IonicPageModule.forChild(EditMedicationComponent),
  ],
  exports: [
    EditMedicationComponent
  ]
})
export class EditMedicationComponentModule {}
