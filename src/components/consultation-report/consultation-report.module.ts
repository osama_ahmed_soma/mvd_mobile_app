import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConsultationReportComponent } from './consultation-report';

@NgModule({
  declarations: [
    ConsultationReportComponent,
  ],
  imports: [
    IonicPageModule.forChild(ConsultationReportComponent),
  ],
  exports: [
    ConsultationReportComponent
  ]
})
export class ConsultationReportComponentModule {}
