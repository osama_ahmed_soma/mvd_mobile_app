import { Component } from '@angular/core';
import {IonicPage, NavParams, ViewController} from 'ionic-angular';

/**
 * Generated class for the ConsultationReportComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@IonicPage()
@Component({
  selector: 'consultation-report',
  templateUrl: 'consultation-report.html'
})
export class ConsultationReportComponent {

  appointment: any;
  reason: string = '';

  constructor(private viewCtrl: ViewController, private navParams: NavParams) {
    this.appointment = this.navParams.get('appointment');
    if(this.appointment.reason_id == 13){
      this.reason = this.appointment.reason;
    } else {
      this.reason = this.appointment.reasonObject.name;
    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
