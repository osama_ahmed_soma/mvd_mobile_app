import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ErrorHandler, NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {IonicStorageModule} from '@ionic/storage';
import {SocketIoModule, SocketIoConfig} from 'ng-socket-io';

import {MyApp} from './app.component';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Stripe} from '@ionic-native/stripe';
import {FileTransfer} from '@ionic-native/file-transfer';
import {File} from '@ionic-native/file';
import {Camera} from '@ionic-native/camera';
import {Diagnostic} from '@ionic-native/diagnostic';
import {PhotoViewer} from '@ionic-native/photo-viewer';
import {FileOpener} from '@ionic-native/file-opener';
import {FileChooser} from '@ionic-native/file-chooser';
import {Network} from '@ionic-native/network';
import {Push} from '@ionic-native/push';

import { AutoCompleteModule } from 'ionic2-auto-complete';

import {Searchform} from '../components/searchform/searchform';
import {UserProvider} from '../providers/user/user';
import {MessageProvider} from '../providers/message/message';
import {DashboardProvider} from '../providers/dashboard/dashboard';
import {SearchProvider} from '../providers/search/search';
import {AppointmentProvider} from '../providers/appointment/appointment';
import {DoctorProvider} from '../providers/doctor/doctor';
import {FavoriteProvider} from '../providers/favorite/favorite';
import {ReasonProvider} from '../providers/reason/reason';
import {WaiveFeeProvider} from '../providers/waive-fee/waive-fee';
import {DoctorConsultationTypesProvider} from '../providers/doctor-consultation-types/doctor-consultation-types';
import {PatientProvider} from '../providers/patient/patient';
import {HealthProvider} from '../providers/health/health';
import {MedicationProvider} from '../providers/medication/medication';
import {AllergyProvider} from '../providers/allergy/allergy';
import {SurgeryProvider} from '../providers/surgery/surgery';
import {FilesUploadProvider} from '../providers/files-upload/files-upload';
import {SharedFilesProvider} from '../providers/shared-files/shared-files';

import * as MvdConfig from '../config/main.config';
import {NotificationComponent} from '../components/notification/notification';
import {AuthProvider} from '../providers/auth/auth';
import {NotificationProvider} from '../providers/notification/notification';
import {RegisterDevicesProvider} from '../providers/register-devices/register-devices';
import {DefaultOptionsComponent} from '../components/default-options/default-options';
import {HealthConditionDataProvider} from '../providers/health-condition-data/health-condition-data';
import {MedicationDataProvider} from '../providers/medication-data/medication-data';
import {AllergiesDataProvider} from '../providers/allergies-data/allergies-data';
import {SurgeriesDataProvider} from '../providers/surgeries-data/surgeries-data';

const config: SocketIoConfig = {
  url: MvdConfig.config.socketURL,
  options: {
    path: '/websockets/socket.io'
  }
};

// TODO: 02132417887

// 8063

// const config: SocketIoConfig = {
//   url: MvdConfig.config.socketURL,
//   options: {}
// };

@NgModule({
  declarations: [
    MyApp,
    Searchform,
    NotificationComponent,
    DefaultOptionsComponent
  ],
  imports: [
    BrowserModule,
    SocketIoModule.forRoot(config),
    HttpClientModule,
    IonicModule.forRoot(MyApp, {
      menuType: 'overlay',
      pageTransition: 'ios-transition'
    }),
    IonicStorageModule.forRoot(),
    BrowserAnimationsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Searchform,
    NotificationComponent,
    DefaultOptionsComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Stripe,
    FileTransfer,
    File,
    Camera,
    Diagnostic,
    PhotoViewer,
    FileOpener,
    FileChooser,
    Network,
    AutoCompleteModule,
    Push,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserProvider, MessageProvider, DashboardProvider, SearchProvider, AppointmentProvider, DoctorProvider, FavoriteProvider, ReasonProvider, WaiveFeeProvider, DoctorConsultationTypesProvider, PatientProvider, HealthProvider, MedicationProvider, AllergyProvider, SurgeryProvider, FilesUploadProvider, SharedFilesProvider, AuthProvider, NotificationProvider, RegisterDevicesProvider, HealthConditionDataProvider, MedicationDataProvider, AllergiesDataProvider, SurgeriesDataProvider,
  ]
})
export class AppModule {
}
