import {Component, ViewChild} from '@angular/core';
import {
  Nav,
  Platform,
  MenuController,
  ToastController,
  Events,
  ModalController,
  LoadingController,
  AlertController
} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Network} from '@ionic-native/network';
import {Storage} from '@ionic/storage';
import {Push, PushObject, PushOptions} from '@ionic-native/push';
import {Socket} from 'ng-socket-io';

// Provider
import {AuthProvider} from '../providers/auth/auth';
import {UserProvider} from '../providers/user/user';

// Custom
import {AppointmentClass} from '../custom/classes/appointment.class';

import * as MvdConfig from '../config/main.config';

declare const window: any;

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  pages: Array<{ title: string, component: any }>;

  tokenAndUserID: any;

  userID: string = '';

  isWaitingRoomOpen: boolean = false;
  isStatusRecieved: boolean = false;
  isDelayed: boolean = false;

  loading: any;
  isLoadingPresent: boolean = false;

  appointmentClass: any;

  isVideoPageOpen: boolean = false;

  constructor(public platform: Platform,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen,
              public storage: Storage,
              public menuCtrl: MenuController,
              private toastCtrl: ToastController,
              private socket: Socket,
              private events: Events,
              private modalCtrl: ModalController,
              private network: Network,
              private loadingCtrl: LoadingController,
              private authService: AuthProvider,
              private push: Push,
              private alertCtrl: AlertController,
              private userService: UserProvider) {

    // used for an example of ngFor and navigation
    this.pages = [
      {title: 'Dashboard', component: 'Dashboard'},
      {title: 'My Appointments', component: 'Appointment'},
      {title: 'Message', component: 'Message'},
      {title: 'My Health', component: 'Myhealth'},
      {title: 'My Physicians ', component: 'MyPhysiciansPage'},
      {title: 'My Files', component: 'MyFilesPage'},
      {title: 'My Profile', component: 'Myprofile'},
      // {title: 'My Settings', component: 'ChangePasswordPage'},
      {title: 'My Settings', component: 'SettingsPage'},
      // {title: 'Support Center', component: 'Faq'},
      {title: 'Contact Support', component: ''},
      {title: 'View Tutorial', component: 'IntroSlidesPage'},
      {title: 'Welcome', component: 'WelcomePage'},
      {title: 'Basic Information', component: 'BasicInformationPage'},
      {title: 'Health Condition', component: 'HealthConditionPage'},
      {title: 'Medication', component: 'MedicationPage'},
      {title: 'Allergies', component: 'AllergiesPage'},
      {title: 'Surgeries', component: 'SurgeriesPage'},
      {title: 'All Set', component: 'AllSetPage'},
      {title: 'Logout', component: ''}
    ];

    // bind appointment main event
    events.subscribe('setTokenAndUserID', (tokenAndUserID) => {
      this.tokenAndUserID = tokenAndUserID;
    });
    events.subscribe('appointment:main:start', () => {
      this.appointmentClass = new AppointmentClass(this);
    });
    events.subscribe('videoPageIsClose', () => {
      this.isVideoPageOpen = false;
    });
    events.subscribe('startVideoEvent', appointmentID => {
      this.appointmentClass.eventHandler.startVideo();
      if (!this.isVideoPageOpen) {
        this.isVideoPageOpen = true;
        this.nav.setRoot('VideoPage', {
          appointmentID: appointmentID
        });
      }
    });

    events.subscribe('logout', () => {
      this.logout();
    });

    this.initializeApp();
    this.initializePushNotification();

  }

  private initializePushNotification() {
    this.platform.ready().then(() => {
      this.push.hasPermission()
        .then((res: any) => {

          if (res.isEnabled) {
            console.log('We have permission to send push notifications');
          } else {
            console.log('We do not have permission to send push notifications');
          }

        });
      const options: PushOptions = MvdConfig.config.PushOptions;

      const pushObject: PushObject = this.push.init(options);

      pushObject.on('notification').subscribe((notification: any) => {
        // alert(JSON.stringify(notification));
        if (notification.additionalData.foreground) {
          const alertObjOption = {
            title: notification.title,
            message: notification.message,
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel',
                handler: () => {

                }
              },
              {
                text: 'View',
                handler: () => {

                }
              }
            ]
          };
          if (notification.additionalData.type === 'message') {
            alertObjOption.buttons.splice(1, 1);
            alertObjOption.buttons.push({
              text: 'View',
              handler: () => {
                this.openMessageView(notification.additionalData.messageData.id);
              }
            });
            // alertObjOption.buttons[1].handler = this.openMessageView(notification.additionalData.messageData.id);
          } else if (notification.additionalData.type === 'notification') {
            // remove view button
            alertObjOption.buttons.splice(1, 1);
          } else if (notification.additionalData.type === 'appointment:upcoming') {
            return;
          } else if (notification.additionalData.type === 'appointment:cancelled') {
            alertObjOption.buttons.splice(1, 1);
            alertObjOption.buttons.push({
              text: 'View',
              handler: () => {
                this.openCancelledAppointment();
              }
            });
            this.events.publish('appointment:unsetAllWorking');
            this.events.publish('appointment:upcoming:hide:time');
            this.events.publish('appointment:upcoming:hide:waiting:room');
            this.appointmentClass = new AppointmentClass(this);

          } else if (notification.additionalData.type === 'appointment:notes') {
            this.events.publish('appointment:upcoming:hide:waiting:room');
            alertObjOption.buttons.splice(1, 1);
            alertObjOption.buttons.push({
              text: 'View',
              handler: () => {
                this.openNotesAppointment(notification.additionalData.appointment.notes);
              }
            });
          }
          const alertObj = this.alertCtrl.create(alertObjOption);
          alertObj.present();
        } else {
          // alert(123);
          const alertObjOption = {
            title: notification.title,
            message: notification.message,
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel',
                handler: () => {

                }
              },
              {
                text: 'View',
                handler: () => {

                }
              }
            ]
          };
          if (notification.additionalData.type === 'message') {
            alertObjOption.buttons.splice(1, 1);
            alertObjOption.buttons.push({
              text: 'View',
              handler: () => {
                this.openMessageView(notification.additionalData.messageData.id);
              }
            });
            // alertObjOption.buttons[1].handler = this.openMessageView(notification.additionalData.messageData.id);
          } else if (notification.additionalData.type === 'notification') {
            // remove view button
            alertObjOption.buttons.splice(1, 1);
          } else if (notification.additionalData.type === 'appointment:upcoming') {
            return;
          } else if (notification.additionalData.type === 'appointment:cancelled') {
            alertObjOption.buttons.splice(1, 1);
            alertObjOption.buttons.push({
              text: 'View',
              handler: () => {
                this.openCancelledAppointment();
              }
            });
            this.events.publish('appointment:unsetAllWorking');
            this.events.publish('appointment:upcoming:hide:time');
            this.events.publish('appointment:upcoming:hide:waiting:room');
            this.appointmentClass = new AppointmentClass(this);

          } else if (notification.additionalData.type === 'appointment:notes') {
            this.events.publish('appointment:upcoming:hide:waiting:room');
            alertObjOption.buttons.splice(1, 1);
            alertObjOption.buttons.push({
              text: 'View',
              handler: () => {
                this.openNotesAppointment(notification.additionalData.appointment.notes);
              }
            });
          }
          const alertObj = this.alertCtrl.create(alertObjOption);
          alertObj.present();
        }
      });

      pushObject.on('registration').subscribe((registration: any) => {
        this.authService.deviceID = registration.registrationId;
        this.storage.set('deviceID', this.authService.deviceID);
      });

      pushObject.on('error').subscribe(error => {
        alert(JSON.stringify(error));
      });
    });
  }

  private openMessageView(id) {
    this.nav.push('MessageView', {
      messageID: id,
      type: 'inbox'
    });
  }

  private openCancelledAppointment() {
    this.nav.push('Appointment', {
      index: 1
    });
  }

  private openNotesAppointment(appointment) {
    let modal = this.modalCtrl.create('ConsultationReportComponent', {
      appointment: appointment
    });
    modal.onDidDismiss(() => {
      // this.events.publish('waiting:room:closed');
    });
    modal.present();
  }

  authenticate() {
    return new Promise(async resolve => {
      await this.storage.ready();
      const token = await this.storage.get('jwt-token');
      if (!token) {
        this.rootPage = 'Login';
        return resolve();
      }
      this.socket.emit('checkAuth', token);
      this.socket.on('checkAuth', async (data) => {
        data = JSON.parse(data);
        if (!data.bool) {
          this.logout();
          return resolve();
        } else {
          this.tokenAndUserID = await this.authService.getTokenAndUserID();
          // this.appointmentClass = new AppointmentClass(this);
          this.events.publish('appointment:main:start');
          this.rootPage = 'Dashboard';
          resolve();
        }
      });
      // return resolve();
    });
  }

  async initializeApp() {
    this.platform.ready().then(async () => {
      this.statusBar.hide();
      await this.authenticate();


      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // this.statusBar.styleDefault();
      // this.statusBar.overlaysWebView(true);
      this.network.onDisconnect().subscribe(() => {
        if (this.isLoadingPresent) {
          this.loading.dismiss();
          this.isLoadingPresent = false;
        }
        this.loading = this.loadingCtrl.create({
          spinner: 'crescent',
          content: 'Network Disconnected.'
        });
        this.loading.present();
        this.isLoadingPresent = true;
      });
      this.network.onConnect().subscribe(() => {
        // We just got a connection but we need to wait briefly
        // before we determine the connection type. Might need to wait.
        // prior to doing any api requests as well.
        setTimeout(() => {
          // if (this.network.type === 'wifi') {
          //   console.log('we got a wifi connection, woohoo!');
          // }
          let toast = this.toastCtrl.create({
            message: 'Network Connected.',
            duration: 3000,
            position: 'top',
            cssClass: 'success'
          });
          toast.present();
          this.loading.dismiss();
          this.isLoadingPresent = false;
        }, 3000);
      });
      this.splashScreen.hide();
    });
  }

  clickPage(page) {
    if (page.title == 'Logout') {
      this.logout();
      return;
    }
    if (page.title == 'Support Center') {
      this.mailTo('support@myvirtualdoctor.com');
      return;
    }
    this.nav.setRoot(page.component);
  }

  mailTo(email: string) {
    this.platform.ready().then(async () => {
      window.open('mailto:' + email);
    });
  }

  async logout() {
    await this.storage.ready();
    this.storage.remove('jwt-token');
    this.storage.remove('userID');
    this.storage.remove('tutorialWatched');
    this.userService.logout(this.authService.tokenUserIDAndData.token, this.authService.deviceID).subscribe(res => {

    });
    let toast = this.toastCtrl.create({
      message: 'You have successfully logged out of My Virtual Doctor',
      duration: 3000,
      position: 'top',
      cssClass: 'success'
    });
    toast.present();
    this.events.publish('appointment:unsetAllWorking');
    this.events.publish('videoPageIsClose');
    this.rootPage = 'Login';
    this.nav.setRoot('Login');
  }

}
