export let config = {
  "api_url": "https://www.myvirtualdoctor.com/api",
  // "api_url" : "http://192.168.8.249:3000/api",
  // "api_url": "http://localhost:3000/api",
  socketURL: "https://www.myvirtualdoctor.com",
  // socketURL: "http://192.168.8.249:3000",
  // socketURL: "http://localhost:3000",
  PushOptions: {
    android: {
      senderID: '643150348936',
      sound: true,
      vibrate: true,
      clearBadge: true
    },
    ios: {
      alert: true,
      sound: true,
      clearBadge: true
    }
  }
};
