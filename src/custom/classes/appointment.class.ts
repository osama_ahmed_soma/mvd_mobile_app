import {EventHandlerClass} from './event.handler.class';
import 'rxjs/add/operator/map';

export class AppointmentClass {

  tokenAndUserID: any;
  interval: any;

  app: any;
  eventHandler: any;

  isRunning: boolean = false;
  isStatusCheck: boolean = false;

  constructor(app) {
    this.app = app;
    this.eventHandler = new EventHandlerClass(this.app);
    this.checkUpcomingAppointment();

    this.app.events.subscribe('appointment:unsetAllWorking', () => {
      this.stopInterval();
      clearInterval(this.eventHandler.interval);
      // this.app.events.publish('appointment:upcoming:hide:time');
    });

    this.app.events.subscribe('appointment:upcoming:check:doctor', (appointmentID) => {
      this.checkIsDoctorReady(appointmentID);
    });

  }

  private startInterval() {
    if (!this.isRunning) {
      this.isRunning = true;
      this.interval = setInterval(() => {
        this.app.socket.emit('appointment:upcoming:check', this.app.tokenAndUserID.userID);
      }, 3000);
    }
  }

  checkUpcomingAppointment() {
    // request sockets every 1 seconds and handle event too in this method.
    // if there is any upcoming appointment after 5 minutes then trigger a local event for start timer and stop this interval for now.
    this.startInterval();
    this.app.socket.fromEvent('appointment:upcoming:checked').map(data => data.toString()).subscribe(data => {
      data = JSON.parse(data);
      console.log(data);
      if (data.actionType !== 'cancelled') {
        this.stopInterval();
        if (!data.isAction) {
          // doctor does not action on this appointment yet so recheck this appointment.
          if (!this.isStatusCheck) {
            // this.app.events.publish('appointment:upcoming:startTimer', data.remainingTime);
            this.eventHandler.proceedAppointmentTime(data.secondsLeft, data.appointmentID);
          }
          this.isStatusCheck = true;
          setTimeout(() => {
            this.startInterval();
          }, 3000);
        } else {
          // doctor actioned
          this.isStatusCheck = false;
          this.eventHandler.proceedAppointmentTime(data.secondsLeft, data.appointmentID);

        }
      }
    });
    this.app.socket.fromEvent('appointment:upcoming:knocking:doctor:available').map(data => data.toString()).subscribe(data => {
      data = JSON.parse(data);
      console.log(data);
      this.app.events.publish('appointment:upcoming:knocking:doctor:available', data);
      // if (!data) {
      //   // show message and button
      //   console.log('not available');
      //   this.app.event.publish('appointment:upcoming:knocking:doctor:available', data);
      // } else {
      //   // hide message and button
      //   console.log('available');
      // }
      // console.log(data);
    });
  }

  checkIsDoctorReady(appointmentID) {
    this.interval = setInterval(() => {
      this.app.socket.emit('appointment:upcoming:isDoctorReady', JSON.stringify({
        userID: this.app.tokenAndUserID.userID,
        appointmentID: appointmentID
      }));
    }, 1000);
    this.app.socket.fromEvent('appointment:upcoming:doctorIsReady').map(data => data => toString()).subscribe(() => {
      clearInterval(this.interval);
      this.app.events.publish('startVideoEvent', appointmentID);
      // this.eventHandler.startVideo(appointmentID);
    });
  }


  stopInterval() {
    this.isRunning = false;
    clearInterval(this.interval);
  }

}
