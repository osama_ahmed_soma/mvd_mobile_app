export class EventHandlerClass {
  app: any;
  interval: any;

  isVideoPageOpen: boolean = false;

  constructor(app) {
    this.app = app;

    this.app.events.subscribe('waiting:room:open', (canClose, appointment: any = '') => {
      if (!this.app.isWaitingRoomOpen) {
        this.openWaitingRoom(canClose, appointment);
      }
    });

    this.app.events.subscribe('appointment:upcoming:check:start', () => {
      this.app.appointmentClass.checkUpcomingAppointment();
    });

  }

  proceedAppointmentTime(seconds, appointmentID) {
    let time = seconds;
    clearInterval(this.interval);
    // emit event here
    this.app.events.publish('counter:toolTip:show');
    this.interval = setInterval(() => {
      const minutes = Math.floor(time / 60);
      const seconds = time - minutes * 60;
      this.app.events.publish('appointment:upcoming:show:time', {
        appointmentID: appointmentID,
        time: this.pad(minutes) + ':' + this.pad(seconds)
      });
      // check doctor is in video page if yes show message with knocking button
      this.app.socket.emit('appointment:upcoming:knocking:doctor:available', appointmentID);


      if (time <= 30) {
        this.app.events.publish('waiting:room:open', false);
      }
      if (time <= 0) {
        clearInterval(this.interval);
        this.app.appointmentClass.checkIsDoctorReady(appointmentID);
      }
      time--;
    }, 1000);
  }

  startVideo() {
    clearInterval(this.interval);
    clearInterval(this.app.appointmentClass.interval);
    this.app.events.publish('appointment:upcoming:hide:time');
    this.app.events.publish('appointment:upcoming:hide:waiting:room');
    // this.app.nav.setRoot('VideoPage', {
    //   appointmentID: appointmentID
    // });
    // this.app.events.publish('get:isVideoPageOpen');
    // this.app.events.subscribe('isVideoPageOpen', (isVideoPageOpen) => {
    //   if(!isVideoPageOpen){
    //     this.app.events.publish('videoPageIsOpen');
    //     this.app.nav.setRoot('VideoPage', {
    //       appointmentID: appointmentID
    //     });
    //   }
    // });
  }

  private pad(n) {
    return (n < 10) ? ("0" + n) : n;
  }

  private openWaitingRoom(canClose, appointment: any = '') {
    let modal = this.app.modalCtrl.create('WaitingRoomComponent', {
      canClose: canClose,
      appointment: appointment
    });
    modal.onDidDismiss(() => {
      this.app.isWaitingRoomOpen = false;
    });
    modal.present();
    this.app.isWaitingRoomOpen = true;
  }


}
