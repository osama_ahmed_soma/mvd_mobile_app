// import {Injectable} from '@angular/core';
// import {Http, Headers} from '@angular/http';
// import 'rxjs/add/operator/map';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';

// Config
import * as MvdConfig from '../../config/main.config';

/*
 Generated class for the MedicationDataProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class MedicationDataProvider {

  endpoint: string = MvdConfig.config.api_url + '/medication/medication_data';

  constructor(public http: HttpClient) {
    console.log('Hello MedicationDataProvider Provider');
  }

  getAll(token) {
    return this.http.get(this.endpoint, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

}
