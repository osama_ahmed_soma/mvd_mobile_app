// import {Injectable} from '@angular/core';
// import {Http, Headers} from '@angular/http';
// import 'rxjs/add/operator/map';
// import {LoadingController, ToastController} from 'ionic-angular';
// import {Storage} from '@ionic/storage';
// import {AutoCompleteService} from 'ionic2-auto-complete-ng5';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {LoadingController, ToastController} from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {AutoCompleteService} from 'ionic2-auto-complete';
import 'rxjs/add/operator/map';

import * as MvdConfig from '../../config/main.config';

/*
 Generated class for the MedicationProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class MedicationProvider implements AutoCompleteService {

  endpoint: string = MvdConfig.config.api_url + '/medication';

  labelAttribute = "name";

  token: string;

  constructor(public http: HttpClient, private storage: Storage, private loadingCtrl: LoadingController, private toastCtrl: ToastController,) {
    this.storage.ready().then(() => {
      this.storage.get('jwt-token').then(token => {
        this.token = token;
      });
    });
  }

  getResults(keyword: string) {
    if (keyword.trim().length >= 3) {
      // let loading = this.loadingCtrl.create({
      //   spinner: 'crescent'
      // });
      // loading.present();
      return this.http.get(this.endpoint + '/getMedicines/' + keyword, {
        headers: new HttpHeaders().set('Authorization', this.token)
      }).map(result => {
        // loading.dismiss();
        // const data = result.json();
        const data: any = result;
        if (data.bool) {
          // return result.json().message.filter(item => item.name.toLowerCase().startsWith(keyword.toLowerCase()));
          return data.message.filter(item => item.name.toLowerCase().startsWith(keyword.toLowerCase()));
        } else {
          let toast = this.toastCtrl.create({
            message: data.message,
            duration: 3000,
            position: 'top',
            cssClass: 'error'
          });
          toast.present();
          return {};
        }
      });
    } else {
      return {};
    }
  }

  get(token) {
    return this.http.get(this.endpoint, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  insert(token, body) {
    return this.http.post(this.endpoint, body, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  insertBunch(token, body) {
    return this.http.post(this.endpoint + '/bunch', body, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  update(token, medication) {
    return this.http.put(this.endpoint + '/' + medication.id, {
      condition_term: medication.condition_term,
      dosage: medication.dosage,
      frequency: medication.frequency,
      time: medication.time
    }, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  remove(token, id) {
    return this.http.delete(this.endpoint + '/' + id, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

}
