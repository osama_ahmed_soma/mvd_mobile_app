// import {Injectable} from '@angular/core';
// import {Http, Headers} from '@angular/http';
// import 'rxjs/add/operator/map';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import * as MvdConfig from '../../config/main.config';

/*
 Generated class for the HealthProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class HealthProvider {

  endpoint: string = MvdConfig.config.api_url + '/health';

  constructor(public http: HttpClient) {
  }

  get(token) {
    return this.http.get(this.endpoint, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  insert(token, body) {
    return this.http.post(this.endpoint, body, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  insertBunch(token, body) {
    return this.http.post(this.endpoint + '/bunch', body, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  update(token, condition) {
    return this.http.put(this.endpoint + '/' + condition.id, {
      condition: condition.condition_term
    }, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  remove(token, id) {
    return this.http.delete(this.endpoint + '/' + id, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

}
