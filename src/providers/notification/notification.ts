import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';

// Config
import * as MvdConfig from '../../config/main.config';

/*
  Generated class for the NotificationProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class NotificationProvider {

  endpoint: string = MvdConfig.config.api_url + '/notification';

  constructor(public http: HttpClient) {
    console.log('Hello NotificationProvider Provider');
  }

  clearAll(token) {
    return this.http.get(this.endpoint + '/readAll', {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  clear(token, notificationID) {
    return this.http.put(this.endpoint + '/read', {
      notificationID: notificationID
    }, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

}
