// import {Injectable} from '@angular/core';
// import {Http, Response, Headers} from '@angular/http';
// import {Observable} from 'rxjs/Observable';
// import 'rxjs/add/operator/map';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import * as MvdConfig from '../../config/main.config';

/*
 Generated class for the UserProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class UserProvider {

  endpoint: string = MvdConfig.config.api_url + '/user';

  constructor(public http: HttpClient) {
  }

  login(userData) {
    return this.http.post(this.endpoint + '/login', userData)
      .catch(this.handleError);
  }

  resetPassword(email) {
    return this.http.post(this.endpoint + '/forgot-password', {email})
      .catch(this.handleError);
  }

  logout(token, deviceID) {
    return this.http.post(this.endpoint + '/logout', {
      deviceID: deviceID
    }, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  register(userData) {
    delete userData.agreed;
    return this.http.post(this.endpoint + '/register', userData)
      .catch(this.handleError);
  }

  changePassword(token, passwordData) {
    return this.http.put(this.endpoint + '/changePassword', passwordData, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  private handleError(error: Response | any) {
// In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      // const body = error.json() || '';
      const body: any = error || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }

}
