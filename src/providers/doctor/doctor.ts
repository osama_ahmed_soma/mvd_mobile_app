// import {Injectable} from '@angular/core';
// import {Http, Headers} from '@angular/http';
// import 'rxjs/add/operator/map';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import * as MvdConfig from '../../config/main.config';

/*
 Generated class for the DoctorProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class DoctorProvider {

  endpoint: string = MvdConfig.config.api_url + '/doctor';

  constructor(public http: HttpClient) {
  }

  getByUserID(token, userID) {
    return this.http.get(this.endpoint + '/' + userID, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  getAvailableTime(token: string, data: {
    doctorID: string,
    date: {
      year: number,
      month: number,
      day: number
    }
  }) {
    return this.http.get(this.endpoint + '/available-time/' + data.doctorID + '/' + data.date.year + '/' + data.date.month + '/' + data.date.day, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

}
