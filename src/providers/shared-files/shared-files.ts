// import {Injectable} from '@angular/core';
// import {Http, Headers} from '@angular/http';
// import 'rxjs/add/operator/map';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';

import * as MvdConfig from '../../config/main.config';

/*
 Generated class for the SharedFilesProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class SharedFilesProvider {

  endpoint: string = MvdConfig.config.api_url + '/patient/myFiles/sharedFiles';

  constructor(public http: HttpClient) {
  }

  getAll(token, fileID) {
    return this.http.get(this.endpoint + '/' + fileID, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  share(token, fileID, doctorID) {
    return this.http.post(this.endpoint + '/' + fileID, {
      doctorID: doctorID
    }, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

}
