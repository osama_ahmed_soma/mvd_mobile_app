import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Socket} from 'ng-socket-io';
import 'rxjs/add/operator/map';

import * as MvdConfig from '../../config/main.config';

/*
 Generated class for the DashboardProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class DashboardProvider {

  endpoint: string = MvdConfig.config.api_url + '/dashboard';

  constructor(public http: HttpClient, private socket: Socket) {
  }

  getAllData(token) {
    return this.http.get(this.endpoint, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  getAllDataSocket(userID: any = '') {
    this.socket.emit('getDashboard', userID);
    return this.socket.fromEvent('dashboard').map(data => data.toString());
  }

}
