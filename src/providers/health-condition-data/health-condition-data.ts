// import {Injectable} from '@angular/core';
// import {Http, Headers} from '@angular/http';
// import 'rxjs/add/operator/map';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import * as MvdConfig from '../../config/main.config';

/*
 Generated class for the HealthConditionDataProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class HealthConditionDataProvider {

  endpoint: string = MvdConfig.config.api_url + '/health/health_condition_data';

  constructor(public http: HttpClient) {
  }

  getAll(token) {
    return this.http.get(this.endpoint, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

}
