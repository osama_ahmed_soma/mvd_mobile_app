// import {Injectable} from '@angular/core';
// import {Http, Headers} from '@angular/http';
// import 'rxjs/add/operator/map';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';

// Config
import * as MvdConfig from '../../config/main.config';

// Providers
import {AuthProvider} from '../auth/auth';

/*
  Generated class for the RegisterDevicesProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class RegisterDevicesProvider {

  endpoint: string = MvdConfig.config.api_url + '/registered_devices';

  constructor(public http: HttpClient,
              private authService: AuthProvider) {
    console.log('Hello RegisterDevicesProvider Provider');
  }

  get() {
    const token = this.authService.getData().token;
    return this.http.get(this.endpoint + '/' + this.authService.getDeviceID(), {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  changeNotification(messageFlag, type) {
    return this.http.put(this.endpoint + '/change', {
      deviceID: this.authService.getDeviceID(),
      messageFlag: messageFlag,
      type: type
    }, {
      headers: new HttpHeaders().set('Authorization', this.authService.getData().token)
    });
  }

}
