// import {Injectable} from '@angular/core';
// import {Http, Headers} from '@angular/http';
// import 'rxjs/add/operator/map';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';

import * as MvdConfig from '../../config/main.config';

/*
 Generated class for the SearchProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class SearchProvider {

  endpoint: string = MvdConfig.config.api_url + '/doctor';

  constructor(public http: HttpClient) {
  }

  search(token, body: {
    query: any,
    phone_number: any,
    state: number,
    specialty: string,
    online_now: boolean
  }) {
    return this.http.post(this.endpoint + '/search', body, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

}
