// import {Injectable} from '@angular/core';
// import {Http, Headers} from '@angular/http';
// import 'rxjs/add/operator/map';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';

import * as MvdConfig from '../../config/main.config';

/*
 Generated class for the PatientProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class PatientProvider {

  endpoint: string = MvdConfig.config.api_url + '/patient';

  constructor(public http: HttpClient) {
  }

  getProfile(token) {
    return this.http.get(this.endpoint, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  update(token, body) {
    return this.http.put(this.endpoint, body, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  updateImage(token, body) {
    return this.http.put(this.endpoint + '/image', body, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  getMyHealthData(token) {
    return this.http.get(this.endpoint + '/myHealth', {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  getMyPhysicians(token) {
    return this.http.get(this.endpoint + '/my-doctors', {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  getPrimaryPhysician(token) {
    return this.http.get(this.endpoint + '/getPrimaryPhysician', {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  updatePrimaryPhysician(token, body) {
    return this.http.put(this.endpoint + '/updatePrimaryPhysician', body, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  updateIsHealth(token, isHealth) {
    return this.http.put(this.endpoint + '/myHealth/changeIsHealth', {
      isHealth: isHealth
    }, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  updateIsMedication(token, isMedication) {
    return this.http.put(this.endpoint + '/myHealth/changeIsMedication', {
      isMedication: isMedication
    }, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  updateIsAllergies(token, isAllergies) {
    return this.http.put(this.endpoint + '/myHealth/changeIsAllergies', {
      isAllergies: isAllergies
    }, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  updateIsSurgeries(token, isSurgeries) {
    return this.http.put(this.endpoint + '/myHealth/changeIsSurgeries', {
      isSurgeries: isSurgeries
    }, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

}
