import {Injectable} from '@angular/core';

import {Events} from 'ionic-angular';
import {Storage} from '@ionic/storage';

import {DashboardProvider} from '../dashboard/dashboard';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AuthProvider {

  deviceID: any;
  tokenUserIDAndData: any;

  constructor(private dashBoardService: DashboardProvider, private storage: Storage, private events: Events) {

  }

  authenticate() {
    return new Promise(async (resolve) => {
      const token = await this.authenticateToken();
      const userID = await this.authenticateUserID();
      if (token && userID) {
        this.dashBoardService.getAllDataSocket(userID).subscribe(res => {
          const data: any = JSON.parse(res);
          if (data.bool && data.appointment.upcoming > 0) {
            this.events.publish('appointment:upcoming:check', data.appointment.upcomingAppointmentData[0], userID);
          }
          this.tokenUserIDAndData = {
            token: token,
            userID: userID,
            data: data
          };
          resolve(this.tokenUserIDAndData);
        });
      } else {
        this.events.publish('logout');
        resolve(false);
      }
    });
  }

  getTokenAndUserID() {
    return new Promise(async (resolve) => {
      const token = await this.authenticateToken();
      const userID = await this.authenticateUserID();
      if (token && userID) {
        resolve({
          token: token,
          userID: userID
        });
      } else {
        this.events.publish('logout');
        resolve(false);
      }
    });
  }

  private authenticateToken() {
    return new Promise(async (resolve) => {
      await this.storage.ready();
      const token = await this.storage.get('jwt-token');
      if (!token) {
        this.events.publish('logout');
        resolve(false);
      } else {
        resolve(token);
      }
    });
  }

  private authenticateUserID() {
    return new Promise(async (resolve) => {
      await this.storage.ready();
      const userID = await this.storage.get('userID');
      if (!userID) {
        this.events.publish('logout');
        resolve(false);
      } else {
        resolve(userID);
      }
    });
  }

  getData() {
    return this.tokenUserIDAndData;
  }

  getDeviceID() {
    return this.deviceID;
  }

}
