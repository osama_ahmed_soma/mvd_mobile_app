// import {Injectable} from '@angular/core';
// import {Http, Headers} from '@angular/http';
// import 'rxjs/add/operator/map';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import * as MvdConfig from '../../config/main.config';

/*
 Generated class for the AppointmentProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class AppointmentProvider {

  endpoint: string = MvdConfig.config.api_url + '/appointment';

  constructor(public http: HttpClient) {
  }

  getPastData(token, page?: number) {
    if (page === undefined) {
      page = 1;
    }
    return this.http.get(this.endpoint + '/past/' + page, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  getCancelData(token, page?: number) {
    if (page === undefined) {
      page = 1;
    }
    return this.http.get(this.endpoint + '/cancelled/' + page, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  getUpcomingData(token, page?: number) {
    if (page === undefined) {
      page = 1;
    }
    return this.http.get(this.endpoint + '/upcoming/' + page, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  schedule(token, body: any) {
    return this.http.post(this.endpoint + '/schedule/', body, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  patientKnock(token, appointmentID) {
    return this.http.get(this.endpoint + '/patientKnocked/' + appointmentID, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  startVideo(token, appointmentID: number) {
    return this.http.get(this.endpoint + '/startVideo/' + appointmentID, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

}
