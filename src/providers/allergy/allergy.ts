// import {Injectable} from '@angular/core';
// import {Http, Headers} from '@angular/http';
// import 'rxjs/add/operator/map';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import * as MvdConfig from '../../config/main.config';

/*
 Generated class for the AllergyProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class AllergyProvider {

  endpoint: string = MvdConfig.config.api_url + '/allergies';

  constructor(public http: HttpClient) {
  }

  get(token) {
    const header = new HttpHeaders();
    header.set('Authorization', token);
    return this.http.get(this.endpoint, {
      headers: header
    });
  }

  getAllPreDefined(token) {
    const header = new HttpHeaders();
    header.set('Authorization', token);
    return this.http.get(this.endpoint + '/pre_defined', {
      headers: header
    });
  }

  insert(token, body) {
    const header = new HttpHeaders();
    header.set('Authorization', token);
    return this.http.post(this.endpoint, body, {
      headers: header
    });
  }

  insertBunch(token, body) {
    const header = new HttpHeaders();
    header.set('Authorization', token);
    return this.http.post(this.endpoint + '/bunch', body, {
      headers: header
    });
  }

  update(token, allery) {
    const header = new HttpHeaders();
    header.set('Authorization', token);
    return this.http.put(this.endpoint + '/' + allery.id, {
      condition_term: allery.condition_term,
      severity: allery.severity,
      reaction: allery.reaction
    }, {
      headers: header
    });
  }

  remove(token, id) {
    const header = new HttpHeaders();
    header.set('Authorization', token);
    return this.http.delete(this.endpoint + '/' + id, {
      headers: header
    });
  }

}
