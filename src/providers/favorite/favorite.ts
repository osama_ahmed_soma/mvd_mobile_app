// import {Injectable} from '@angular/core';
// import {Http, Headers} from '@angular/http';
// import 'rxjs/add/operator/map';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import * as MvdConfig from '../../config/main.config';

/*
 Generated class for the FavoriteProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class FavoriteProvider {

  endpoint: string = MvdConfig.config.api_url + '/patient';

  constructor(public http: HttpClient) {
  }

  add(token, doctorID) {
    return this.http.post(this.endpoint + '/addDoctor', {
      doctorID: doctorID
    }, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  remove(token, doctorID) {
    return this.http.delete(this.endpoint + '/removeDoctor', {
      headers: new HttpHeaders().set('Authorization', token),
      params: {
        doctorID: doctorID
      }
    });
  }

}
