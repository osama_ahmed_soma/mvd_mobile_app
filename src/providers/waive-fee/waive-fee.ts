// import {Injectable} from '@angular/core';
// import {Http, Headers} from '@angular/http';
// import 'rxjs/add/operator/map';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';

import * as MvdConfig from '../../config/main.config';

/*
 Generated class for the WaiveFeeProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class WaiveFeeProvider {

  endpoint: string = MvdConfig.config.api_url + '/waive_fee';

  constructor(public http: HttpClient) {
  }

  isWaived(token: string, doctorID: number) {
    return this.http.get(this.endpoint + '/is-patient-waived/' + doctorID, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

}
