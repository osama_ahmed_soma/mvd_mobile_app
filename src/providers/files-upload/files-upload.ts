// import {Injectable} from '@angular/core';
// import {Http, Headers} from '@angular/http';
// import 'rxjs/add/operator/map';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import {Platform} from 'ionic-angular';
import {FileTransfer, FileUploadOptions, FileTransferObject} from '@ionic-native/file-transfer';
import {File} from '@ionic-native/file';
import {Diagnostic} from '@ionic-native/diagnostic';


import * as MvdConfig from '../../config/main.config';

/*
 Generated class for the FilesUploadProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class FilesUploadProvider {

  endpoint: string = MvdConfig.config.api_url + '/patient/myFiles';

  constructor(private platform: Platform, public http: HttpClient, private transfer: FileTransfer, private file: File, private diagnostic: Diagnostic) {
  }

  getAll(token) {
    return this.http.get(this.endpoint, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  update(token, fileID, title) {
    return this.http.post(this.endpoint + '/' + fileID, {
      title: title
    }, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  remove(token, fileID) {
    return this.http.delete(this.endpoint + '/' + fileID, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  private async executeDownloadPlugin(token, fileID, fileName) {
    try {
      const fileTransfer: FileTransferObject = this.transfer.create();
      let directory: string = this.file.externalDataDirectory;
      if (this.platform.is('ios')) {
        directory = this.file.syncedDataDirectory;
      }
      const entry = await fileTransfer.download(this.endpoint + '/download/' + fileID, directory + fileName, true, {
        headers: {
          "Authorization": token
        }
      });
      return entry.toURL();
    } catch (e) {
      return e.toString();
    }
  }

  download(token, fileID, fileName): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.platform.ready();
        const authorized = await this.diagnostic.isExternalStorageAuthorized();
        if (authorized) {
          // use plugin
          resolve(this.executeDownloadPlugin(token, fileID, fileName));
        } else {
          // request
          await this.diagnostic.requestExternalStorageAuthorization();
          resolve(this.executeDownloadPlugin(token, fileID, fileName));
        }
      } catch (e) {
        reject(e.toString());
      }
    });
  }

  upload(token, filePath, mimeType, fileName): Promise<string> {
    return new Promise(async (resolve, reject) => {
      try {
        await this.platform.ready();
        let options: FileUploadOptions = {
          fileName: fileName,
          mimeType: mimeType,
          httpMethod: 'POST',
          headers: {
            "Authorization": token
          }
        };
        const fileTransfer: FileTransferObject = this.transfer.create();
        await fileTransfer.upload(filePath, this.endpoint + '/upload', options, true);
        resolve();
      } catch (e) {
        reject(e.toString());
      }
    });
  }


}
