// import {Injectable} from '@angular/core';
// import {Http, Headers} from '@angular/http';
// import 'rxjs/add/operator/map';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';

import * as MvdConfig from '../../config/main.config';

/*
 Generated class for the MessageProvider provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class MessageProvider {

  endpoint: string = MvdConfig.config.api_url + '/message';

  constructor(public http: HttpClient) {
  }

  create(token, body) {
    return this.http.post(this.endpoint, body, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  viewMessages(token, messageID, type) {
    return this.http.get(this.endpoint + '/view/' + messageID + '/' + type, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  getInbox(token) {
    return this.http.get(this.endpoint + '/inbox', {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  getSent(token) {
    return this.http.get(this.endpoint + '/sent', {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  getDrafts(token) {
    return this.http.get(this.endpoint + '/drafts', {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  getTrash(token) {
    return this.http.get(this.endpoint + '/trash', {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  getUnreadCount(token) {
    return this.http.get(this.endpoint + '/unread-count', {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  search(token, body) {
    return this.http.post(this.endpoint + '/search', body, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  remove(token, messageID) {
    return this.http.delete(this.endpoint + '/' + messageID, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

  removePermanent(token, messageID) {
    return this.http.delete(this.endpoint + '/permanent/' + messageID, {
      headers: new HttpHeaders().set('Authorization', token)
    });
  }

}
